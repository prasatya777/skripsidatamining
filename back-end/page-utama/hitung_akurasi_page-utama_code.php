<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}
	include '../../page-admin/authentication/authenc_code.php';

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT id_dp, nama_dp, A1Cresult_dp FROM tbl_data_pasien
							ORDER BY id_dp ASC
						');

	$pdo->execute();
	$i=0;
	while($row= $pdo->fetch(PDO::FETCH_OBJ))
	{
		$i++;
		$datapasien[$i]=array('id'=>$row->id_dp, 
						'nama'=>$row->nama_dp, 'a1c'=>$row->A1Cresult_dp);
	}
	// echo json_encode($datapasien);
	// echo count($datapasien);

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT id_hsl_p, nama_hsl_p, perkiraan_hsl_p 
							FROM tbl_data_pasien_prediksi
							ORDER BY id_hsl_p ASC
						');

	$pdo->execute();
	$benar=0;
	$salah=0;
	while($row= $pdo->fetch(PDO::FETCH_OBJ))
	{
		for($i=1;$i<=count($datapasien);$i++)
		{
			if($datapasien[$i]['id']==$row->id_hsl_p AND $datapasien[$i]['nama']==$row->nama_hsl_p)
			{
				if($datapasien[$i]['a1c']==$row->perkiraan_hsl_p){$benar++;}
				else{$salah++;}
			}
		}
	}
	$akurasi = (intval($benar)/intval(count($datapasien)))*100;
	$total = count($datapasien);
	$persen = $akurasi."%";
	// echo "Benar : ".$benar."<br>";
	// echo "Salah : ".$salah."<br><br>";
	// echo "Akurasi : ".$akurasi."%<br><br>";

	$data[] = array(
                'nilai_benar'	=> $benar,
                'nilai_salah'	=> $salah,
                'nilai_total'   => $total,
                'nilai_akurasi' => $persen
          	);
    echo json_encode($data);  
?>