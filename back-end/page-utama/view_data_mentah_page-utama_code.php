<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}
	include '../../page-admin/authentication/authenc_code.php';

	try {
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo = $conn->prepare('SELECT DISTINCT A1Cresult_dm FROM tbl_data_mentah ORDER BY A1Cresult_dm ASC');
		$pdo->execute();
		$count = $pdo->rowCount();
		while($row= $pdo->fetch(PDO::FETCH_OBJ))
		{
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo2 = $conn->prepare('SELECT COUNT(case when A1Cresult_dm = "Norm" then 1 end) AS TotalNorm,
									 COUNT(case when A1Cresult_dm = ">7" then 1 end) AS Total7,
									 COUNT(case when A1Cresult_dm = ">8" then 1 end) AS Total8
									 FROM tbl_data_mentah');
			$pdo2->execute();
			$row2= $pdo2->fetch(PDO::FETCH_OBJ);

			if($row->A1Cresult_dm=="Norm"){$jumlah=$row2->TotalNorm;}
				if($row->A1Cresult_dm==">7"){$jumlah=$row2->Total7;}
					if($row->A1Cresult_dm==">8"){$jumlah=$row2->Total8;}

			$data[] = array(
				'labelname'	 	=> $row->A1Cresult_dm,
				'labelnilai'	=> $jumlah	
			);
		}

		if($count==0)
			{
				$data[] = array(
				'labelname'	 	=> ">7",
				'labelnilai'	=> 0
				);
				$data[] = array(
				'labelname'	 	=> ">8",
				'labelnilai'	=> 0
				);
				$data[] = array(
				'labelname'	 	=> "Norm",
				'labelnilai'	=> 0
				);
			}
	} catch (PDOexception $e) {
	  die();
	}
	$data2['nilai']=$data;
	echo json_encode($data2);
?>