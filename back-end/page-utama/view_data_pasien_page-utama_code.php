<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}
	include '../../page-admin/authentication/authenc_code.php';

	try {
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo = $conn->prepare('SELECT DISTINCT perkiraan_hsl_p FROM tbl_data_pasien_prediksi ORDER BY 
								perkiraan_hsl_p ASC');
		$pdo->execute();
		$count = $pdo->rowCount();
		while($row= $pdo->fetch(PDO::FETCH_OBJ))
		{
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo2 = $conn->prepare('SELECT COUNT(case when perkiraan_hsl_p = "Norm" then 1 end) AS TotalNorm,
									 COUNT(case when perkiraan_hsl_p = ">7" then 1 end) AS Total7,
									 COUNT(case when perkiraan_hsl_p = ">8" then 1 end) AS Total8,
									 COUNT(case when perkiraan_hsl_p = "None" then 1 end) AS TotalNone
									 FROM tbl_data_pasien_prediksi');
			$pdo2->execute();
			$row2= $pdo2->fetch(PDO::FETCH_OBJ);

			if($row->perkiraan_hsl_p=="Norm"){$jumlah=$row2->TotalNorm;}
				if($row->perkiraan_hsl_p==">7"){$jumlah=$row2->Total7;}
					if($row->perkiraan_hsl_p==">8"){$jumlah=$row2->Total8;}
						if($row->perkiraan_hsl_p=="None"){$jumlah=$row2->TotalNone;}

			$data[] = array(
				'labelname'	 	=> $row->perkiraan_hsl_p,
				'labelnilai'	=> $jumlah
			);
		}
		
		if($count==0)
			{
				$data[] = array(
				'labelname'	 	=> ">7",
				'labelnilai'	=> 0
				);
				$data[] = array(
				'labelname'	 	=> ">8",
				'labelnilai'	=> 0
				);
				$data[] = array(
				'labelname'	 	=> "Norm",
				'labelnilai'	=> 0
				);
			}
	} catch (PDOexception $e) {
	  die();
	}
	$data2['nilai']=$data;
	echo json_encode($data2);
?>