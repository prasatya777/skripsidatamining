<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}
	include '../../page-admin/authentication/authenc_code.php';

	$result = $conn->prepare('
								INSERT INTO tbl_data_pasien_joinmentah 
								(nama_dm, gender_dm, age_dm, admission_type_id_dm, discharge_disposition_id_dm, admission_source_id_dm, time_in_hospital_dm, num_lab_procedures_dm, num_procedures_dm, diag_1_dm, diag_2_dm, diag_3_dm, number_diagnoses_dm, change_dm, diabetesMed_dm, readmitted_dm)
								SELECT nama_dp, gender_dp, age_dp, admission_type_id_dp, discharge_disposition_id_dp, admission_source_id_dp, time_in_hospital_dp, num_lab_procedures_dp, num_procedures_dp, diag_1_dp, diag_2_dp, diag_3_dp, number_diagnoses_dp, change_dp, diabetesMed_dp, readmitted_dp 
								FROM tbl_data_pasien
								');
	$result->execute();

	$result = $conn->prepare('
								INSERT INTO tbl_data_pasien_joinmentah 
								(gender_dm, age_dm, admission_type_id_dm, discharge_disposition_id_dm, admission_source_id_dm, time_in_hospital_dm, num_lab_procedures_dm, num_procedures_dm, diag_1_dm, diag_2_dm, diag_3_dm, number_diagnoses_dm, change_dm, diabetesMed_dm, readmitted_dm)
								SELECT gender_dm, age_dm, admission_type_id_dm, discharge_disposition_id_dm, admission_source_id_dm, time_in_hospital_dm, num_lab_procedures_dm, num_procedures_dm, diag_1_dm, diag_2_dm, diag_3_dm, number_diagnoses_dm, change_dm, diabetesMed_dm, readmitted_dm 
								FROM tbl_data_mentah
								');
	$result->execute();

	// Reset nilai autoincrement dan mengurutkan nilai ID
	$result = $conn->prepare('
								SET @count:=0;
								UPDATE tbl_data_pasien_joinmentah SET id_dm=@count:=@count+1;
								ALTER TABLE tbl_data_pasien_joinmentah AUTO_INCREMENT=1;
								');
	$result->execute();
?>