<?php
	include '../../koneksi/koneksi.php';

	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';

	$nilai_cek=0;

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$result = $conn->prepare('SELECT id_dp FROM tbl_data_pasien 
								ORDER BY id_dp DESC LIMIT 1
							');
	$result->execute();
	$count = $result->rowCount();
			
	if($count>0)
	{
		$nilai_cek+=0;
	}
	else
	{
		$nilai_cek+=1;
	}

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$result = $conn->prepare('SELECT id_hsl_k FROM tbl_hsl_kmean 
								ORDER BY id_hsl_k DESC LIMIT 1
							');
	$result->execute();
	$count = $result->rowCount();
			
	if($count>0)
	{
		$nilai_cek+=0;
	}
	else
	{
		$nilai_cek+=1;
	}

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$result = $conn->prepare('SELECT id_hc45 FROM tbl_hsl_c45 
								ORDER BY id_hc45 DESC LIMIT 1
							');
	$result->execute();
	$count = $result->rowCount();
			
	if($count>0)
	{
		$nilai_cek+=0;
	}
	else
	{
		$nilai_cek+=1;
	}

	echo $nilai_cek;
?>