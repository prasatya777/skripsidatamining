<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}
	include '../../page-admin/authentication/authenc_code.php';

	//Bagian Data Pasien
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT * FROM tbl_data_pasien_hsl_kmean');
	$pdo->execute();
	$i=0;
	while($row= $pdo->fetch(PDO::FETCH_OBJ))
	{
		$array_datapasien[$i] = array(
		    'id_hsl_k'					 => $row->id_hsl_k,
		    'nama_hsl_k'				 => $row->nama_hsl_k,
		    'gender_hsl_k'				 => $row->gender_hsl_k,
		    'age_hsl_k'					 => $row->age_hsl_k,
		    'admission_type_id_hsl_k'	 => $row->admission_type_id_hsl_k,
		    'admission_source_id_hsl_k'	 => $row->admission_source_id_hsl_k,
		    'time_in_hospital_hsl_k'	 => $row->time_in_hospital_hsl_k,
		    'num_lab_procedures_hsl_k'	 => $row->num_lab_procedures_hsl_k,
		    'num_procedures_hsl_k'		 => $row->num_procedures_hsl_k,
		    'diag_1_hsl_k'				 => $row->diag_1_hsl_k,
		    'diag_2_hsl_k'				 => $row->diag_2_hsl_k,
		    'diag_3_hsl_k'				 => $row->diag_3_hsl_k,
		    'number_diagnoses_hsl_k'	 => $row->number_diagnoses_hsl_k,
		    'change_hsl_k'				 => $row->change_hsl_k,
		    'diabetesMed_hsl_k'			 => $row->diabetesMed_hsl_k,
		    'readmitted_hsl_k'			 => $row->readmitted_hsl_k,
		    'prediksi'					 => '',
		    'next_leaf' 				 => '',
		    'nm_root' 				 	 => '',
		    'vl_root' 				 	 => '',
		    'nm_lf1' 				 	 => '',
		    'vl_lf1' 				 	 => '',
		    'nm_lf2' 				 	 => '',
		    'vl_lf2' 				 	 => ''
		);
		$i++;
	}

	//Bagian Root
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT * FROM tbl_hsl_c45 WHERE noleaftree_hc45 = :noleaf');
	$cek = array(':noleaf' => '1');
	$pdo->execute($cek);
	while($row= $pdo->fetch(PDO::FETCH_OBJ))
	{
		for($i=0;$i<count($array_datapasien);$i++)
		{
			foreach ( $array_datapasien[$i] as $key => $value )
			{
			    if (strval($key) == strval($row->nmvariabel_hc45)) 
			    {
			    	if(strval($value) == strval($row->value_hc45) 
			    		&& $array_datapasien[$i]['prediksi']=="")
			    	{
			    		$n_perkiraan = convertnilai($row->perkiraan_hc45);
			    		$array_datapasien[$i]['prediksi'] = $n_perkiraan;
			    		if($array_datapasien[$i]['prediksi'] == "" 
			    			|| $array_datapasien[$i]['prediksi'] == null)
			    		{
			    			$array_datapasien[$i]['next_leaf'] = 1;
			    			$array_datapasien[$i]['nm_root'] = $row->nmvariabel_hc45;
			    			$array_datapasien[$i]['vl_root'] = $row->value_hc45;
			    		}
			    	}
			    }
			}
		}
		//Bagian Akar 1 ------------------------------------------------------------->
		if(
			strval($row->perkiraan_hc45)!="Norm" &&
			strval($row->perkiraan_hc45)!=">8" &&
			strval($row->perkiraan_hc45)!=">7" &&
			strval($row->perkiraan_hc45)!="None"
		  )
		{
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo1 = $conn->prepare('SELECT * FROM tbl_hsl_c45 
									WHERE noleaftree_hc45 = :noleaf
									AND id_hc45 = :nilaiakar1');
			$cek1 = array(':noleaf' => '2', ':nilaiakar1' => $row->perkiraan_hc45);
			$pdo1->execute($cek1);
			while($row1= $pdo1->fetch(PDO::FETCH_OBJ))
			{
				for($i=0;$i<count($array_datapasien);$i++)
				{
					foreach ( $array_datapasien[$i] as $key => $value )
					{
					    if (strval($key) == strval($row1->nmvariabel_hc45) &&
					    		$array_datapasien[$i]['next_leaf'] == 1 &&
					    		strval($array_datapasien[$i]['nm_root']) == strval($row->nmvariabel_hc45) &&
					    		strval($array_datapasien[$i]['vl_root']) == strval($row->value_hc45)
					    	) 
					    {
					    	if(strval($value) == strval($row1->value_hc45) 
					    		&& $array_datapasien[$i]['prediksi']=="")
					    	{
					    		$n_perkiraan = convertnilai($row1->perkiraan_hc45);
					    		$array_datapasien[$i]['prediksi'] = $n_perkiraan;
					    		if($array_datapasien[$i]['prediksi'] == ""
					    			|| $array_datapasien[$i]['prediksi'] == null)
					    		{
					    			$array_datapasien[$i]['next_leaf'] = 2;
					    			$array_datapasien[$i]['nm_lf1'] = $row1->nmvariabel_hc45;
					    			$array_datapasien[$i]['vl_lf1'] = $row1->value_hc45;
					    		}
					    	}
					    }
					}
				}

				//Bagian Akar 2 ----------------------------------------------------------->
				if(
					strval($row1->perkiraan_hc45)!="Norm" &&
					strval($row1->perkiraan_hc45)!=">8" &&
					strval($row1->perkiraan_hc45)!=">7" &&
					strval($row1->perkiraan_hc45)!="None"
				  )
				{
					$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$pdo2 = $conn->prepare('SELECT * FROM tbl_hsl_c45 
											WHERE noleaftree_hc45 = :noleaf
											AND id_hc45 = :nilaiakar1');
					$cek2 = array(':noleaf' => '3', ':nilaiakar1' => $row1->perkiraan_hc45);
					$pdo2->execute($cek2);
					while($row2= $pdo2->fetch(PDO::FETCH_OBJ))
					{
						for($i=0;$i<count($array_datapasien);$i++)
						{
							foreach ( $array_datapasien[$i] as $key => $value )
							{
							    if (strval($key) == strval($row2->nmvariabel_hc45) &&
							    	$array_datapasien[$i]['next_leaf'] == 2 &&
						    		strval($array_datapasien[$i]['nm_root']) == strval($row->nmvariabel_hc45) &&
						    		strval($array_datapasien[$i]['vl_root']) == strval($row->value_hc45) &&
						    		strval($array_datapasien[$i]['nm_lf1']) == strval($row1->nmvariabel_hc45) &&
						    		strval($array_datapasien[$i]['vl_lf1']) == strval($row1->value_hc45)
							    	) 
							    {
							    	if(strval($value) == strval($row2->value_hc45) 
							    		&& $array_datapasien[$i]['prediksi']=="")
							    	{
							    		$n_perkiraan = convertnilai($row2->perkiraan_hc45);
							    		$array_datapasien[$i]['prediksi'] = $n_perkiraan;
							    		if($array_datapasien[$i]['prediksi'] == ""
							    			|| $array_datapasien[$i]['prediksi'] == null)
							    		{
							    			$array_datapasien[$i]['next_leaf'] = 3;
							    			$array_datapasien[$i]['nm_lf2'] = $row2->nmvariabel_hc45;
							    			$array_datapasien[$i]['vl_lf2'] = $row2->value_hc45;
							    		}
							    	}
							    }
							}
						}
						//Bagian Akar 3 --------------------------------------------------->
						if(
							strval($row2->perkiraan_hc45)!="Norm" &&
							strval($row2->perkiraan_hc45)!=">8" &&
							strval($row2->perkiraan_hc45)!=">7" &&
							strval($row2->perkiraan_hc45)!="None"
						  )
						{
							$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
							$pdo3 = $conn->prepare('SELECT * FROM tbl_hsl_c45 
													WHERE noleaftree_hc45 = :noleaf
													AND id_hc45 = :nilaiakar1');
							$cek3 = array(':noleaf' => '4', ':nilaiakar1' => $row2->perkiraan_hc45);
							$pdo3->execute($cek3);
							while($row3= $pdo3->fetch(PDO::FETCH_OBJ))
							{
								for($i=0;$i<count($array_datapasien);$i++)
								{
									foreach ( $array_datapasien[$i] as $key => $value )
									{
									    if (strval($key) == strval($row3->nmvariabel_hc45) &&
									    	$array_datapasien[$i]['next_leaf'] == 3 &&
								    		strval($array_datapasien[$i]['nm_root']) == strval($row->nmvariabel_hc45) &&
								    		strval($array_datapasien[$i]['vl_root']) == strval($row->value_hc45) &&
								    		strval($array_datapasien[$i]['nm_lf1']) == strval($row1->nmvariabel_hc45) &&
								    		strval($array_datapasien[$i]['vl_lf1']) == strval($row1->value_hc45) &&
								    		strval($array_datapasien[$i]['nm_lf2']) == strval($row2->nmvariabel_hc45) &&
								    		strval($array_datapasien[$i]['vl_lf2']) == strval($row2->value_hc45)
									    	) 
									    {
									    	if(strval($value) == strval($row3->value_hc45) 
									    		&& $array_datapasien[$i]['prediksi']=="")
									    	{
									    		$n_perkiraan = convertnilai($row3->perkiraan_hc45);
									    		$array_datapasien[$i]['prediksi'] = $n_perkiraan;
									    		if($array_datapasien[$i]['prediksi'] == ""
									    			|| $array_datapasien[$i]['prediksi'] == null)
									    		{
									    			$array_datapasien[$i]['next_leaf'] = "";
									    		}
									    	}
									    }
									}
								}
							}	
						}
					}	
				}
			}	
		}
	}

// Simpan Hasil Prediksi------------------------------------------------------------------
for($i=0;$i<count($array_datapasien);$i++)
{
	foreach ( $array_datapasien[$i] as $key => $value )
	{
		if (strval($key) == "id_hsl_k")
			{$id = strval($value);}
			if (strval($key) == "nama_hsl_k")
				{$nama = strval($value);}
					if (strval($key) == "gender_hsl_k")
						{$gender = strval($value);}
						if (strval($key) == "age_hsl_k")
							{$age = strval($value);}
							if (strval($key) == "prediksi")
								{$hslperkiraan = strval($value);}
	}
	if($hslperkiraan=="" || $hslperkiraan== null ){$hslperkiraan="None";}
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('INSERT INTO tbl_data_pasien_prediksi(
								id_hsl_p,
								nama_hsl_p,
								gender_hsl_p,
								age_hsl_p,
								perkiraan_hsl_p
								) 
								VALUES 
								(
								:idp, :nm, :ge, :ag, :pr
						)');
	$insertdata = array(
							':idp' => $id, 
							':nm' => $nama, 
							':ge' => $gender, 
							':ag' => $age, 
							':pr' => $hslperkiraan
						);
	$pdo->execute($insertdata);
	reset($array_datapasien[$i]);
}

function convertnilai($nilai_p)
{
	if(strval($nilai_p)!="Norm" &&
		strval($nilai_p)!=">8" &&
		strval($nilai_p)!=">7" &&
		strval($nilai_p)!="None"
	   )
	{
		return null;
	}
	else
	{
		return $nilai_p;
	}
}

echo json_encode($array_datapasien);
?>