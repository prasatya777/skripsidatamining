<?php
	//Untuk mengosongkan isi tabel Centroid dan Hasil K-means
	deleteTabel();

	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';
	
	include '../../vendor/PHPExcel-1.8/Classes/PHPExcel.php';

		$inputFile = $_FILES["ImportExcelFile"]["tmp_name"];
        $inputFileType = PHPExcel_IOFactory::identify($inputFile);
        $excelReader = PHPExcel_IOFactory::createReader($inputFileType);
        $excelObj = $excelReader->load($inputFile);

		$worksheet = $excelObj->getSheet(0);
		$lastRow = $worksheet->getHighestRow();

		//Mengambil Nilai ID Terakhir
		include '../../koneksi/koneksi.php';
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$result = $conn->prepare('SELECT id_dp
									FROM tbl_data_pasien
									ORDER BY id_dp DESC LIMIT 1
									');
		$result->execute();
		$count = $result->rowCount();
		
		if($count>0)
		{
			$row = $result->fetch(PDO::FETCH_OBJ);
			$no=$row->id_dp;	
		}
		else
		{
			$no=0;
		}
		
		
		for ($row = 2; $row <= $lastRow; $row++) {

			$no++;
			$gender = $worksheet->getCell('A'.$row)->getValue();
			$age = $worksheet->getCell('B'.$row)->getValue();
			$admission_type_id	= $worksheet->getCell('C'.$row)->getValue();
			$discharge_disposition_id	= $worksheet->getCell('D'.$row)->getValue();
			$admission_source_id	= $worksheet->getCell('E'.$row)->getValue();
			$time_in_hospital	= $worksheet->getCell('F'.$row)->getValue();
			$num_lab_procedures = $worksheet->getCell('G'.$row)->getValue();
			$num_procedures = $worksheet->getCell('H'.$row)->getValue();
			$diag_1 = $worksheet->getCell('I'.$row)->getValue();
			$diag_2 = $worksheet->getCell('J'.$row)->getValue();
			$diag_3 = $worksheet->getCell('K'.$row)->getValue();
			$number_diagnoses = $worksheet->getCell('L'.$row)->getValue();
			$A1Cresult = $worksheet->getCell('M'.$row)->getValue();
			$change = $worksheet->getCell('N'.$row)->getValue();
			$diabetesMed = $worksheet->getCell('O'.$row)->getValue();
			$readmitted = $worksheet->getCell('P'.$row)->getValue();

			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('INSERT INTO tbl_data_pasien (
										id_dp,
										nama_dp,
										gender_dp, 
										age_dp, 
										admission_type_id_dp, 
										discharge_disposition_id_dp, 
										admission_source_id_dp, 
										time_in_hospital_dp, 
										num_lab_procedures_dp, 
										num_procedures_dp,
										diag_1_dp, 
										diag_2_dp, 
										diag_3_dp,
										number_diagnoses_dp,
										change_dp,
										diabetesMed_dp,
										readmitted_dp,
										A1Cresult_dp
										) 
										VALUES 
										(
										:id,
										:nm,
										:gndr, 
										:ag, 
										:ati,
										:ddi, 
										:asi, 
										:tih, 
										:nlp, 
										:np, 
										:dg1, 
										:dg2, 
										:dg3,
										:nd,
										:chng,
										:dm,
										:read,
										:a1c

									)');
			$insertdata = array(
									':id' => $no, ':nm' => $no,
									':gndr' => $gender, ':ag' => $age, 
									':ati' => $admission_type_id, 
									':ddi' => $discharge_disposition_id, 
									':asi' => $admission_source_id, 
									':tih' => $time_in_hospital,
									':nlp' => $num_lab_procedures, 
									':np' => $num_procedures,
									':dg1' => $diag_1, ':dg2' => $diag_2, 
									':dg3' => $diag_3, ':nd' => $number_diagnoses,
									':chng' => $change, ':dm' => $diabetesMed, 
									':read' => $readmitted, ':a1c' => $A1Cresult
								);
			$pdo->execute($insertdata);
		}

		//Untuk mengosongkan isi tabel Pasien
		function deleteTabel()
		{
			include '../../koneksi/koneksi.php';
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('TRUNCATE tbl_data_pasien');
			$pdo->execute();
		}

?>