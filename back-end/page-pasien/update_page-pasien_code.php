<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';

	extract($_POST);
	$id 						= $_POST['id'];
	$nama 						= $_POST['txt_nama'];
	$gender 					= $_POST['txt_gender'];
	$age 						= $_POST['txt_age'];
	$admission_type_id			= $_POST['txt_admission_type_id'];
	$discharge_disposition_id	= $_POST['txt_discharge_disposition_id'];
	$admission_source_id		= $_POST['txt_admission_source_id'];
	$time_in_hospital 			= $_POST['txt_time_in_hospital'];
	$num_lab_procedures 		= $_POST['txt_num_lab_procedures'];
	$num_procedures 			= $_POST['txt_num_procedures'];
	$diag_1 					= $_POST['txt_diag_1'];
	$diag_2 					= $_POST['txt_diag_2'];
	$diag_3 					= $_POST['txt_diag_3'];
	$number_diagnoses 			= $_POST['txt_number_diagnoses'];
	$change 					= $_POST['txt_change'];
	$diabetesMed 				= $_POST['txt_diabetesMed'];
	$readmitted 				= $_POST['txt_readmitted'];

	try {
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('UPDATE tbl_data_pasien
										set
										nama_dp	=:nm,
										gender_dp =:gndr, 
										age_dp =:ag,
										admission_type_id_dp =:ati, 
										discharge_disposition_id_dp =:ddi, 	
										admission_source_id_dp =:asi, 
										time_in_hospital_dp =:tih,
										num_lab_procedures_dp =:nlp, 
										num_procedures_dp =:np,
										diag_1_dp =:dg1, 
										diag_2_dp =:dg2,
										diag_3_dp =:dg3,
										number_diagnoses_dp =:nd,
										change_dp =:chng, 
										diabetesMed_dp =:dm,
										readmitted_dp =:read
										WHERE id_dp =:idp');
			$updatedata = array(
									':nm' => $nama,
									':gndr' => $gender, ':ag' => $age, 
									':ati' => $admission_type_id, 
									':ddi' => $discharge_disposition_id, 
									':asi' => $admission_source_id, 
									':tih' => $time_in_hospital,
									':nlp' => $num_lab_procedures, 
									':np' => $num_procedures,
									':dg1' => $diag_1, ':dg2' => $diag_2, 
									':dg3' => $diag_3, ':nd' => $number_diagnoses,
									':chng' => $change, ':dm' => $diabetesMed, 
									':read' => $readmitted, ':idp' => $id,
								);
			$pdo->execute($updatedata);
		} catch (PDOexception $e) {
		  die();
		}
?>