<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

//Hitung Jumlah Data
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT COUNT(id_dm) AS TotalData FROM tbl_data_mentah');

	$pdo->execute(); 
	$row= $pdo->fetch(PDO::FETCH_OBJ);

	$totaldata = $row->TotalData;

//Penetuan Centroid Awal
	$centroidawal_1 = round(($totaldata/3)/2);
	$centroidawal_2 = round(($totaldata/3) + $centroidawal_1);
	$centroidawal_3 = round(($totaldata/3) + $centroidawal_2);

//Mengambil Nilai Centroid Awal

	//Centroid Awal = 0
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT time_in_hospital_dm, num_lab_procedures_dm, num_procedures_dm, number_diagnoses_dm FROM tbl_data_mentah WHERE 
		id_dm = :id');

	$pdo->execute(array(':id' => $centroidawal_1));
	$row= $pdo->fetch(PDO::FETCH_OBJ);

	$centroid_a=array(
		array($row->time_in_hospital_dm, $row->num_lab_procedures_dm, 
			$row->num_procedures_dm, $row->number_diagnoses_dm)
	);

	//Save Insert Database Centroid Awal Centroid=0
	$gol = 0;
	insertTblCentroid($gol, $row->time_in_hospital_dm, $row->num_lab_procedures_dm, $row->num_procedures_dm, $row->number_diagnoses_dm);

	//Centroid Awal = 1
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT time_in_hospital_dm, num_lab_procedures_dm, num_procedures_dm, number_diagnoses_dm FROM tbl_data_mentah WHERE 
		id_dm = :id');

	$pdo->execute(array(':id' => $centroidawal_2));
	$row= $pdo->fetch(PDO::FETCH_OBJ);

	$centroid_b=array(
		array($row->time_in_hospital_dm, $row->num_lab_procedures_dm, 
			$row->num_procedures_dm, $row->number_diagnoses_dm)
	);

	//Save Insert Database Centroid Awal Centroid=1
	$gol = 1;
	insertTblCentroid($gol, $row->time_in_hospital_dm, $row->num_lab_procedures_dm, $row->num_procedures_dm, $row->number_diagnoses_dm);

	//Centroid Awal = 2
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT time_in_hospital_dm, num_lab_procedures_dm, num_procedures_dm, number_diagnoses_dm FROM tbl_data_mentah WHERE 
		id_dm = :id');

	$pdo->execute(array(':id' => $centroidawal_3));
	$row= $pdo->fetch(PDO::FETCH_OBJ);

	$centroid_c=array(
		array($row->time_in_hospital_dm, $row->num_lab_procedures_dm, 
			$row->num_procedures_dm, $row->number_diagnoses_dm)
	);

	//Save Insert Database Centroid Awal Centroid=2
	$gol = 2;
	insertTblCentroid($gol, $row->time_in_hospital_dm, $row->num_lab_procedures_dm, $row->num_procedures_dm, $row->number_diagnoses_dm);

//Start Looping
do
{
//Menghitung Nilai Centroid Selanjutnya
	//Tabel Hitung Nilai Kolom X dan Kolom Y
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT id_dm, time_in_hospital_dm, num_lab_procedures_dm, num_procedures_dm, number_diagnoses_dm,
		SQRT(
			(POW((time_in_hospital_dm-:tih_a),2))+ 
			(POW((num_lab_procedures_dm-:nlp_a),2))+
			(POW((num_procedures_dm-:np_a),2))+
			(POW((number_diagnoses_dm-:nd_a),2)) 
			) AS NilaiX,
		SQRT(
			(POW((time_in_hospital_dm-:tih_b),2))+ 
			(POW((num_lab_procedures_dm-:nlp_b),2))+
			(POW((num_procedures_dm-:np_b),2))+
			(POW((number_diagnoses_dm-:nd_b),2))  
			) AS NilaiY,
		SQRT(
			(POW((time_in_hospital_dm-:tih_c),2))+ 
			(POW((num_lab_procedures_dm-:nlp_c),2))+
			(POW((num_procedures_dm-:np_c),2))+
			(POW((number_diagnoses_dm-:nd_c),2))  
			) AS NilaiZ
		FROM tbl_data_mentah');

	$view = array(
					':tih_a' => $centroid_a[0][0], 
					':nlp_a' => $centroid_a[0][1], 
					':np_a' => $centroid_a[0][2], 
					':nd_a' => $centroid_a[0][3],

					':tih_b' => $centroid_b[0][0], 
					':nlp_b' => $centroid_b[0][1], 
					':np_b' => $centroid_b[0][2], 
					':nd_b' => $centroid_b[0][3],

					':tih_c' => $centroid_c[0][0], 
					':nlp_c' => $centroid_c[0][1], 
					':np_c' => $centroid_c[0][2], 
					':nd_c' => $centroid_c[0][3]
					);

	$pdo->execute($view);

	$total_centroid_0 = 0;

	$total_centroid_1 = 0;

	$total_centroid_2 = 0;

	while($row= $pdo->fetch(PDO::FETCH_OBJ))
	{
		//Penentuan Mana Yang Termasuk Kelompok Centroid 0
		if($row->NilaiX < $row->NilaiY && $row->NilaiX < $row->NilaiZ)
		{
			$hasil_centroid_0[] = array(
									$row->id_dm,
									$row->time_in_hospital_dm,
									$row->num_lab_procedures_dm,
									$row->num_procedures_dm,
									$row->number_diagnoses_dm
			);
			$total_centroid_0++;
		}

		//Penentuan Mana Yang Termasuk Kelompok Centroid 1
		if($row->NilaiY < $row->NilaiX && $row->NilaiY < $row->NilaiZ)
		{
			$hasil_centroid_1[] = array(
									$row->id_dm,
									$row->time_in_hospital_dm,
									$row->num_lab_procedures_dm,
									$row->num_procedures_dm,
									$row->number_diagnoses_dm
			);
			$total_centroid_1++;
		}
		
		//Penentuan Mana Yang Termasuk Kelompok Centroid 2
		if($row->NilaiZ < $row->NilaiX && $row->NilaiZ < $row->NilaiY)
		{
			$hasil_centroid_2[] = array(
									$row->id_dm,
									$row->time_in_hospital_dm,
									$row->num_lab_procedures_dm,
									$row->num_procedures_dm,
									$row->number_diagnoses_dm
			);
			$total_centroid_2++;
		}

	}

	//Perhitungan Untuk mementukan Centroid Baru Centroid=0
	$total_tih_0 = 0;
	$total_nlp_0 = 0;
	$total_np_0 = 0;
	$total_nd_0 = 0;

	for($i=0; $i<$total_centroid_0; $i++)
	{
		$total_tih_0 += $hasil_centroid_0[$i][1];
		$total_nlp_0 += $hasil_centroid_0[$i][2];
		$total_np_0 += $hasil_centroid_0[$i][3];
		$total_nd_0 += $hasil_centroid_0[$i][4];
	}
		$total_tih_0 = $total_tih_0/$total_centroid_0;
		$total_nlp_0 = $total_nlp_0/$total_centroid_0;
		$total_np_0 = $total_np_0/$total_centroid_0;
		$total_nd_0 = $total_nd_0/$total_centroid_0;

		unset($centroid_a);
		$centroid_a=array(
			array($total_tih_0, $total_nlp_0, $total_np_0, $total_nd_0)
		);

	
	//Perhitungan Untuk mementukan Centroid Baru Centroid=1
	$total_tih_1 = 0;
	$total_nlp_1 = 0;
	$total_np_1 = 0;
	$total_nd_1 = 0;

	for($i=0; $i<$total_centroid_1; $i++)
	{
		$total_tih_1 += $hasil_centroid_1[$i][1];
		$total_nlp_1 += $hasil_centroid_1[$i][2];
		$total_np_1 += $hasil_centroid_1[$i][3];
		$total_nd_1 += $hasil_centroid_1[$i][4];
	}

		$total_tih_1 = $total_tih_1/$total_centroid_1;
		$total_nlp_1 = $total_nlp_1/$total_centroid_1;
		$total_np_1 = $total_np_1/$total_centroid_1;
		$total_nd_1 = $total_nd_1/$total_centroid_1;

		unset($centroid_b);
		$centroid_b=array(
			array($total_tih_1, $total_nlp_1, $total_np_1, $total_nd_1)
		);


	//Perhitungan Untuk mementukan Centroid Baru Centroid=2
	$total_tih_2 = 0;
	$total_nlp_2 = 0;
	$total_np_2 = 0;
	$total_nd_2 = 0;

	for($i=0; $i<$total_centroid_2; $i++)
	{
		$total_tih_2 += $hasil_centroid_2[$i][1];
		$total_nlp_2 += $hasil_centroid_2[$i][2];
		$total_np_2 += $hasil_centroid_2[$i][3];
		$total_nd_2 += $hasil_centroid_2[$i][4];
	}

		$total_tih_2 = $total_tih_2/$total_centroid_2;
		$total_nlp_2 = $total_nlp_2/$total_centroid_2;
		$total_np_2 = $total_np_2/$total_centroid_2;
		$total_nd_2 = $total_nd_2/$total_centroid_2;

		unset($centroid_c);
		$centroid_c=array(
			array($total_tih_2, $total_nlp_2, $total_np_2, $total_nd_2)
		);

//Menggambil Nilai Centroid Dari Database
	//Mengambil Nilai Centroid 0
	$cek_centroid_0 = getnilaiCentroid("0", $centroid_a[0][0], $centroid_a[0][1], 
					$centroid_a[0][2], $centroid_a[0][3]);

	//Mengambil Nilai Centroid 1
	$cek_centroid_1 = getnilaiCentroid("1", $centroid_b[0][0], $centroid_b[0][1], 
					$centroid_b[0][2], $centroid_b[0][3]);

	//Mengambil Nilai Centroid 2
	$cek_centroid_2 = getnilaiCentroid("2", $centroid_c[0][0], $centroid_c[0][1], 
					$centroid_c[0][2], $centroid_c[0][3]);

	if($cek_centroid_0==0 || $cek_centroid_1==0 || $cek_centroid_2==0)
	{
	unset($hasil_centroid_0);
	unset($hasil_centroid_1);
	unset($hasil_centroid_2);
	}

	//Save Insert Database Centroid=0
	$gol = 0;
	insertTblCentroid($gol, $centroid_a[0][0], $centroid_a[0][1], $centroid_a[0][2], $centroid_a[0][3]);

	//Save Insert Database Centroid=0
	$gol = 1;
	insertTblCentroid($gol, $centroid_b[0][0], $centroid_b[0][1], $centroid_b[0][2], $centroid_b[0][3]);

	//Save Insert Database Centroid=2
	$gol = 2;
	insertTblCentroid($gol, $centroid_c[0][0], $centroid_c[0][1], $centroid_c[0][2], $centroid_c[0][3]);

	
} while($cek_centroid_0==0 || $cek_centroid_1==0 || $cek_centroid_2==0);
//End Looping

//Insert To Tabel Hasil K-Means (Hasil Akhir K-Means Dan telah dikelompokkan)
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT 
							id_dm, 
							gender_dm, 
							age_dm, 
							admission_type_id_dm,
							discharge_disposition_id_dm,
							admission_source_id_dm,
							diag_1_dm,
							diag_2_dm,
							diag_3_dm,
							A1Cresult_dm,
							change_dm,
							diabetesMed_dm,
							readmitted_dm
							FROM tbl_data_mentah
						');

	$pdo->execute();
	while($row= $pdo->fetch(PDO::FETCH_OBJ))
	{
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$insert = $conn->prepare('INSERT INTO tbl_hsl_kmean (
									id_hsl_k,
									gender_hsl_k, 
									age_hsl_k, 
									admission_type_id_hsl_k, 
									discharge_disposition_id_hsl_k, 
									admission_source_id_hsl_k, 
									time_in_hospital_hsl_k, 
									num_lab_procedures_hsl_k, 
									num_procedures_hsl_k,
									diag_1_hsl_k, 
									diag_2_hsl_k, 
									diag_3_hsl_k,
									number_diagnoses_hsl_k,
									A1Cresult_hsl_k,
									change_hsl_k,
									diabetesMed_hsl_k,
									readmitted_hsl_k
									) 
									VALUES 
									(
									:id,
									:gndr, 
									:ag, 
									:ati,
									:ddi, 
									:asi, 
									:tih, 
									:nlp, 
									:np, 
									:dg1, 
									:dg2, 
									:dg3,
									:nd,
									:a1c,
									:chng,
									:dm,
									:read
								)');

		for($i=0; $i<$total_centroid_0; $i++)
		{
			if($row->id_dm == $hasil_centroid_0[$i][0])
			{
				$hsl_centroid=0;
			}
		}

		for($i=0; $i<$total_centroid_1; $i++)
		{
			if($row->id_dm == $hasil_centroid_1[$i][0])
			{
				$hsl_centroid=1;
			}
		}

		for($i=0; $i<$total_centroid_2; $i++)
		{
			if($row->id_dm == $hasil_centroid_2[$i][0])
			{
				$hsl_centroid=2;
			}
		}

		$insertdata = array(
								':id' => $row->id_dm,
								':gndr' => $row->gender_dm, 
								':ag' => $row->age_dm, 
								':ati' => $row->admission_type_id_dm, 
								':ddi' => $row->discharge_disposition_id_dm, 
								':asi' => $row->admission_source_id_dm, 
								':tih' => $hsl_centroid,
								':nlp' => $hsl_centroid, 
								':np' => $hsl_centroid,
								':dg1' => $row->diag_1_dm, 
								':dg2' => $row->diag_2_dm, 
								':dg3' => $row->diag_3_dm, 
								':nd' => $hsl_centroid,
								':a1c' => $row->A1Cresult_dm, 
								':chng' => $row->change_dm,
								':dm' => $row->diabetesMed_dm, 
								':read' => $row->readmitted_dm
							);
		$insert->execute($insertdata);
	}





//Function Insert To Tabel Centroid K-Means
	function insertTblCentroid($gol, $tih, $nlp, $np, $nd)
	{
			include '../../koneksi/koneksi.php';
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('INSERT INTO tbl_centroid_kmean (
										gol_centroid_ck,
										time_in_hospital_ck,
										num_lab_procedures_ck,
										num_procedures_ck,
										number_diagnoses_ck
										) 
										VALUES 
										(
										:gc, :tih, :nlp, :np, :nd
									)');
			$insertdata = array(
									':gc' => $gol, 
									':tih' => $tih, 
									':nlp' => $nlp, 
									':np' => $np, 
									':nd' => $nd
								);
			$pdo->execute($insertdata);
	}

	function getnilaiCentroid($gol, $tih, $nlp, $np, $nd)
	{
		include '../../koneksi/koneksi.php';
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$result = $conn->prepare('SELECT time_in_hospital_ck, 
									num_lab_procedures_ck, 
									num_procedures_ck, 
									number_diagnoses_ck 
									FROM tbl_centroid_kmean
									WHERE gol_centroid_ck = '.$gol.' 
									ORDER BY id_ck DESC LIMIT 2
									');
		$result->execute();
		$row = $result->fetch(PDO::FETCH_OBJ);

		if( strval($row->time_in_hospital_ck) == strval($tih) AND
			strval($row->num_lab_procedures_ck) == strval($nlp) AND
			strval($row->num_procedures_ck) == strval($np) AND
			strval($row->number_diagnoses_ck) == strval($nd)
		)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
?>