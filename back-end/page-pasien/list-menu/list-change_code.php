<?php
    include '../../../koneksi/koneksi.php';
    if (session_status() == PHP_SESSION_NONE) 
    {
        session_start();
        ob_start();
    }
    include '../../../page-admin/authentication/authenc_code.php';

    $result = $conn->query('SELECT DISTINCT change_dm 
                            from tbl_data_mentah ORDER BY cast(change_dm as int) ASC');
    while($row=$result->fetch(PDO::FETCH_OBJ))
    {
      $data[] = array(
                        'id' => $row->change_dm,
                        'text' => $row->change_dm
                );
    }
   echo json_encode($data);

?>   