<?php
    include '../../../koneksi/koneksi.php';
    if (session_status() == PHP_SESSION_NONE) 
    {
        session_start();
        ob_start();
    }
    include '../../../page-admin/authentication/authenc_code.php';

    $result = $conn->query('SELECT DISTINCT discharge_disposition_id_dm 
                            from tbl_data_mentah ORDER BY cast(discharge_disposition_id_dm as int) ASC');
    while($row=$result->fetch(PDO::FETCH_OBJ))
    {
      $data[] = array(
                        'id' => $row->discharge_disposition_id_dm,
                        'text' => $row->discharge_disposition_id_dm
                );
    }
   echo json_encode($data);
 
?>   