<?php
    include '../../../koneksi/koneksi.php';
    if (session_status() == PHP_SESSION_NONE) 
    {
        session_start();
        ob_start();
    }
    include '../../../page-admin/authentication/authenc_code.php';

    $result = $conn->query('SELECT DISTINCT readmitted_dm 
                            from tbl_data_mentah ORDER BY cast(readmitted_dm as int) ASC');
    while($row=$result->fetch(PDO::FETCH_OBJ))
    {
      $data[] = array(
                        'id' => $row->readmitted_dm,
                        'text' => $row->readmitted_dm
                );
    }
   echo json_encode($data);

?>   