<?php
    include '../../../koneksi/koneksi.php';
    if (session_status() == PHP_SESSION_NONE) 
    {
        session_start();
        ob_start();
    }
    include '../../../page-admin/authentication/authenc_code.php';

    $result = $conn->query('SELECT DISTINCT admission_source_id_dm 
                            from tbl_data_mentah ORDER BY cast(admission_source_id_dm as int) ASC');
    while($row=$result->fetch(PDO::FETCH_OBJ))
    {
      $data[] = array(
                        'id' => $row->admission_source_id_dm,
                        'text' => $row->admission_source_id_dm
                );
    }
   echo json_encode($data);
 
?>   