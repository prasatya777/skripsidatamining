<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT * FROM tbl_data_pasien_hsl_kmean');
	$pdo->execute();

	while($row= $pdo->fetch(PDO::FETCH_OBJ))
	{
		$data[] = array(
			'id'	 					=> $row->id_hsl_k,
			'nama'	 					=> $row->nama_hsl_k,
			'gender' 					=> $row->gender_hsl_k,
			'age' 						=> $row->age_hsl_k,
			'admission_type_id'			=> $row->admission_type_id_hsl_k,
			'discharge_disposition_id'	=> $row->discharge_disposition_id_hsl_k,
			'admission_source_id'		=> $row->admission_source_id_hsl_k,
			'time_in_hospital'			=> $row->time_in_hospital_hsl_k,
			'num_lab_procedures'		=> $row->num_lab_procedures_hsl_k,
			'num_procedures'			=> $row->num_procedures_hsl_k,
			'diag_1'					=> $row->diag_1_hsl_k,
			'diag_2'					=> $row->diag_2_hsl_k,
			'diag_3'					=> $row->diag_3_hsl_k,
			'number_diagnoses'			=> $row->number_diagnoses_hsl_k,
			'change'					=> $row->change_hsl_k,
			'diabetesMed'				=> $row->diabetesMed_hsl_k,
			'readmitted'				=> $row->readmitted_hsl_k	
		);
	}

	echo json_encode($data);
?>