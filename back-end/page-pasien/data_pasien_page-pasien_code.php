<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}
	include '../../page-admin/authentication/authenc_code.php';

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT * FROM tbl_data_pasien');
	$pdo->execute();

	$no = 0;
	while($row= $pdo->fetch(PDO::FETCH_OBJ))
	{
		$no++;
		$info = $row->id_dp.'-'.$row->nama_dp;
		$data[] = array(
			'no'						=> $no,
			'nama'						=> $row->nama_dp,
			'id'	 					=> $row->id_dp,
			'gender' 					=> $row->gender_dp,
			'age' 						=> $row->age_dp,
			'admission_type_id'			=> $row->admission_type_id_dp,
			'discharge_disposition_id'	=> $row->discharge_disposition_id_dp,
			'admission_source_id'		=> $row->admission_source_id_dp,
			'time_in_hospital'			=> $row->time_in_hospital_dp,
			'num_lab_procedures'		=> $row->num_lab_procedures_dp,
			'num_procedures'			=> $row->num_procedures_dp,
			'diag_1'					=> $row->diag_1_dp,
			'diag_2'					=> $row->diag_2_dp,
			'diag_3'					=> $row->diag_3_dp,
			'number_diagnoses'			=> $row->number_diagnoses_dp,
			'change'					=> $row->change_dp,
			'diabetesMed'				=> $row->diabetesMed_dp,
			'readmitted'				=> $row->readmitted_dp,
			'info'						=> $info
		);
	}

	echo json_encode($data);
?>