<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT * FROM tbl_data_pasien_prediksi');
	$pdo->execute();

	while($row= $pdo->fetch(PDO::FETCH_OBJ))
	{
		$data[] = array(
			'id'	 					=> $row->id_hsl_p,
			'nama'	 					=> $row->nama_hsl_p,
			'gender' 					=> $row->gender_hsl_p,
			'age' 						=> $row->age_hsl_p,
			'prediksi'					=> $row->perkiraan_hsl_p
		);
	}

	echo json_encode($data);
?>