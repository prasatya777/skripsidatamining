<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';

	$id_view = $_POST['id'];

	$response = array();
	try {
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('SELECT * FROM tbl_data_pasien WHERE id_dp = :idv');
			$pdo->bindparam(':idv', $id_view);
			$pdo->execute();
			$row = $pdo->fetch(PDO::FETCH_OBJ);
			$response = $row;

			echo json_encode($response);
		} catch (PDOexception $e) {
		  die();
		}
?>