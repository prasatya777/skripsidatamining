<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}
	include '../../page-admin/authentication/authenc_code.php';


	$nm_array_min_max=array("tih","nlp","np","nd");
	$var_array_min_max=array("Time In Hospital","Num Lab Procedures",
							"Num Procedures","Num Diagnoses");
	$i=0;
	foreach ($nm_array_min_max as $min_max ) 
	{
		for($a=0;$a<3;$a++)
		{
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('SELECT * FROM tbl_range_kmean 
									WHERE var_rk="'.$var_array_min_max[$i].'" 
									ORDER BY CAST(max_rk AS INT) DESC LIMIT '.$a.',1');
			$pdo->execute();
			$row= $pdo->fetch(PDO::FETCH_OBJ);

				${$min_max."_min_max"}[] = array(
					'var_range' 	=> $row->var_rk,
					'cluster_range' => $row->cluster_rk,
					'min_range'		=> $row->min_rk,
					'max_range'		=> $row->max_rk
				);
		}
		$i++;
	}

// -----------------------------------------------------------------------------------------

//Insert To Tabel Hasil K-Means (Hasil Akhir K-Means Dan telah dikelompokkan) dan hanya menyimpan data pasien yang difilter
	$nm_var_dm=array("time_in_hospital_dp","num_lab_procedures_dp",
						"num_procedures_dp","number_diagnoses_dp");
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT * FROM tbl_data_pasien ORDER BY id_dp ASC');
	$pdo->execute();
	while($row= $pdo->fetch(PDO::FETCH_OBJ))
	{
		$i=0;
		foreach ($nm_array_min_max as $if_min_max ) 
			{
				// Penampung variabel dari array $nm_var_dm
				$t_nm=strval($nm_var_dm[$i]);
				// Untuk Tidak Ada Nilai None
				if(
					${$if_min_max."_min_max"}[0]['min_range']!="None" AND
					${$if_min_max."_min_max"}[0]['max_range']!="None" AND
					${$if_min_max."_min_max"}[1]['min_range']!="None" AND
					${$if_min_max."_min_max"}[1]['max_range']!="None" AND
					${$if_min_max."_min_max"}[2]['min_range']!="None" AND
					${$if_min_max."_min_max"}[2]['max_range']!="None"
				  )
				{
					if(intval($row->$t_nm) >= 
						intval(${$if_min_max."_min_max"}[0]['min_range'])
					  )
					{	
						${$if_min_max."_hsl_gol"} = ${$if_min_max."_min_max"}[0]
							['cluster_range'];
					}
					else if(intval($row->$t_nm) >= 
								intval(${$if_min_max."_min_max"}[1]['min_range']) AND
							intval($row->$t_nm) <= 
								intval(${$if_min_max."_min_max"}[1]['max_range'])
							)
					{
						${$if_min_max."_hsl_gol"} = ${$if_min_max."_min_max"}[1]
							['cluster_range'];
					}
					else if(intval($row->$t_nm) <= 
								intval(${$if_min_max."_min_max"}[2]['max_range'])
							)
					{
						${$if_min_max."_hsl_gol"} = ${$if_min_max."_min_max"}[2]
							['cluster_range'];
					}
					else
					{
						${$if_min_max."_hsl_gol"} = 4;
					}
				}
				// Untuk None pada Middle dan MIN
				else if(
						${$if_min_max."_min_max"}[0]['min_range']!="None" AND
						${$if_min_max."_min_max"}[0]['max_range']!="None" AND
						${$if_min_max."_min_max"}[1]['min_range']=="None" AND
						${$if_min_max."_min_max"}[1]['max_range']=="None" AND
						${$if_min_max."_min_max"}[2]['min_range']=="None" AND
						${$if_min_max."_min_max"}[2]['max_range']=="None"
				  		)
				{
					if(intval($row->$t_nm) >= 0)
					{
						${$if_min_max."_hsl_gol"} = ${$if_min_max."_min_max"}[0]
							['cluster_range'];
					}
				}
				// Untuk None pada MIN
				else if(
						${$if_min_max."_min_max"}[0]['min_range']!="None" AND
						${$if_min_max."_min_max"}[0]['max_range']!="None" AND
						${$if_min_max."_min_max"}[1]['min_range']!=="None" AND
						${$if_min_max."_min_max"}[1]['max_range']!=="None" AND
						${$if_min_max."_min_max"}[2]['min_range']=="None" AND
						${$if_min_max."_min_max"}[2]['max_range']=="None"
				  		)
				{
					if(intval($row->$t_nm) >= 
						intval(${$if_min_max."_min_max"}[0]['min_range'])
					  )
					{
						${$if_min_max."_hsl_gol"} = ${$if_min_max."_min_max"}[0]
							['cluster_range'];
					}
					else if(intval($row->$t_nm) >= 0 AND
							intval($row->$t_nm) <= 
								intval(${$if_min_max."_min_max"}[1]['max_range'])
							)
					{
						${$if_min_max."_hsl_gol"} = ${$if_min_max."_min_max"}[1]
							['cluster_range'];
					}
					else
					{
						${$if_min_max."_hsl_gol"} = 4;
					}
				}
				// Untuk All None
				else if(
						${$if_min_max."_min_max"}[0]['min_range']=="None" AND
						${$if_min_max."_min_max"}[0]['max_range']=="None" AND
						${$if_min_max."_min_max"}[1]['min_range']=="None" AND
						${$if_min_max."_min_max"}[1]['max_range']=="None" AND
						${$if_min_max."_min_max"}[2]['min_range']=="None" AND
						${$if_min_max."_min_max"}[2]['max_range']=="None"
				  		)
				{
					${$if_min_max."_hsl_gol"} = 4;
				}

				$i++;
			}

		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$insert = $conn->prepare('INSERT INTO tbl_data_pasien_hsl_kmean (
									id_hsl_k,
									nama_hsl_k,
									gender_hsl_k, 
									age_hsl_k, 
									admission_type_id_hsl_k, 
									discharge_disposition_id_hsl_k, 
									admission_source_id_hsl_k, 
									time_in_hospital_hsl_k, 
									num_lab_procedures_hsl_k, 
									num_procedures_hsl_k,
									diag_1_hsl_k, 
									diag_2_hsl_k, 
									diag_3_hsl_k,
									number_diagnoses_hsl_k,
									change_hsl_k,
									diabetesMed_hsl_k,
									readmitted_hsl_k
									) 
									VALUES 
									(
									:id, :nm, :gndr, :ag, :ati, :ddi, :asi, :tih, :nlp, 
									:np, :dg1, :dg2, :dg3, :nd, :chng, :dm,	:read
								)');

		$insertdata = array(
								':id' => $row->id_dp,
								':nm' => $row->nama_dp,
								':gndr' => $row->gender_dp, 
								':ag' => $row->age_dp, 
								':ati' => $row->admission_type_id_dp, 
								':ddi' => $row->discharge_disposition_id_dp, 
								':asi' => $row->admission_source_id_dp, 
								':tih' => $tih_hsl_gol,
								':nlp' => $nlp_hsl_gol, 
								':np' => $np_hsl_gol,
								':dg1' => $row->diag_1_dp, 
								':dg2' => $row->diag_2_dp, 
								':dg3' => $row->diag_3_dp, 
								':nd' => $nd_hsl_gol,
								':chng' => $row->change_dp,
								':dm' => $row->diabetesMed_dp, 
								':read' => $row->readmitted_dp
							);
		$insert->execute($insertdata);
	}
?>