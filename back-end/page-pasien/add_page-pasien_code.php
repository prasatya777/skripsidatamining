<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';
	//Mengambil Nilai ID Terakhir
	include '../../koneksi/koneksi.php';
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$result = $conn->prepare('SELECT id_dp
								FROM tbl_data_pasien
								ORDER BY id_dp DESC LIMIT 1
								');
	$result->execute();
	$count = $result->rowCount();
		
	if($count>0)
	{
		$row = $result->fetch(PDO::FETCH_OBJ);
		$no=intval($row->id_dp) + 1;	
	}
	else
	{
		$no=1;
	}

	extract($_POST);
	$nama = $_POST['txt_nama'];
	$gender = $_POST['txt_gender'];
	$age = $_POST['txt_age'];
	$admission_type_id	= $_POST['txt_admission_type_id'];
	$discharge_disposition_id	= $_POST['txt_discharge_disposition_id'];
	$admission_source_id	= $_POST['txt_admission_source_id'];
	$time_in_hospital = $_POST['txt_time_in_hospital'];
	$num_lab_procedures = $_POST['txt_num_lab_procedures'];
	$num_procedures = $_POST['txt_num_procedures'];
	$diag_1 = $_POST['txt_diag_1'];
	$diag_2 = $_POST['txt_diag_2'];
	$diag_3 = $_POST['txt_diag_3'];
	$number_diagnoses = $_POST['txt_number_diagnoses'];
	$change = $_POST['txt_change'];
	$diabetesMed = $_POST['txt_diabetesMed'];
	$readmitted = $_POST['txt_readmitted'];

	try {
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('INSERT INTO tbl_data_pasien (
										nama_dp,
										id_dp,
										gender_dp, 
										age_dp, 
										admission_type_id_dp, 
										discharge_disposition_id_dp, 
										admission_source_id_dp, 
										time_in_hospital_dp, 
										num_lab_procedures_dp, 
										num_procedures_dp,
										diag_1_dp, 
										diag_2_dp, 
										diag_3_dp,
										number_diagnoses_dp,
										change_dp,
										diabetesMed_dp,
										readmitted_dp
										) 
										VALUES 
										(
										:nm, :id, :gndr, :ag, :ati, :ddi, :asi, :tih, :nlp, :np, :dg1, 
										:dg2, :dg3, :nd, :chng, :dm, :read
									)');
			$insertdata = array(
									':nm' => $nama,
									':id' => $no,
									':gndr' => $gender, ':ag' => $age, 
									':ati' => $admission_type_id, 
									':ddi' => $discharge_disposition_id, 
									':asi' => $admission_source_id, 
									':tih' => $time_in_hospital,
									':nlp' => $num_lab_procedures, 
									':np' => $num_procedures,
									':dg1' => $diag_1, ':dg2' => $diag_2, 
									':dg3' => $diag_3, ':nd' => $number_diagnoses,
									':chng' => $change, ':dm' => $diabetesMed, ':read' => $readmitted
								);
			$pdo->execute($insertdata);
		} 
			catch (PDOexception $e) {
			die();
		}
?>