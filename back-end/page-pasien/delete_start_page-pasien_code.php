<?php
	include '../../koneksi/koneksi.php';

	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('TRUNCATE tbl_data_pasien_hsl_kmean');
	$pdo->execute();

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('TRUNCATE tbl_data_pasien_prediksi');
	$pdo->execute();
?>