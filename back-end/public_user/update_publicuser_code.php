<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';

	extract($_POST);
			$nm	 	  = $_POST['txt_nama'];
			$em  	  = $_POST['txt_email'];
			$psw 	  = $_POST['txt_password'];
			$username = $_SESSION['username'];

			try {

				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

				$pdo = $conn->prepare('UPDATE tbl_admin_login
												set
												email_al =:mail,
												nama_al =:nam, 
												password_al =:pwd
												WHERE email_al =:user');
				$updatedata = array(
									':mail' => $em,
									':nam' => $nm, 
									':pwd' => $psw,
									':user' => $username
								);
				$pdo->execute($updatedata);

			$_SESSION['username'] = $em;
			$_SESSION['nama'] = $nm;
			} catch (PDOexception $e) {
			   die();
			}
?>