<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}
	include '../../page-admin/authentication/authenc_code.php';

	// Select Nilai Max Dan Min Awal
	$i=0;$no_var=0;

	$nm_array_search=array("time_in_hospital_gk","num_lab_procedures_gk",
							"num_procedures_gk","number_diagnoses_gk");
	$var_array_search=array("Time In Hospital","Num Lab Procedures",
							"Num Procedures","Num Diagnoses");

	foreach ($nm_array_search as $search ) 
	{
		for($a=1;$a<4;$a++)
		{
			$i++;
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('SELECT MAX(CAST('.$search.' AS INT)) AS N_MAX,  
									MIN(CAST('.$search.' AS INT)) AS N_MIN
									FROM tbl_group_kmean WHERE gol_'.$search.' = '.$a.'');
			$pdo->execute();
			$row= $pdo->fetch(PDO::FETCH_OBJ);

				$data[] = array(
					'no_range'		=> $i,
					'var_range' 	=> $var_array_search[$no_var],
					'cluster_range' => $a,
					'min_range'		=> $row->N_MIN,
					'max_range'		=> $row->N_MAX
				);
		}
		$no_var++;
	}

	// Kosongkan tabel range kmean - 1
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('TRUNCATE tbl_range_kmean');
	$pdo->execute();

	// echo json_encode($data);

	// Insert Tabel Range Kmean - 2
	foreach ($data as $key => $value) 
	{
		$min = "";$max = "";
		$min = $value['min_range'];$max = $value['max_range'];

		if(empty($min) AND strval($min)!="0" ){$min="None";}
			if(empty($max) AND strval($max)!="0" ){$max="None";}

		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo = $conn->prepare('INSERT INTO tbl_range_kmean (
									id_rk, var_rk, cluster_rk, min_rk, max_rk
									) 
									VALUES 
									(
									:id, :vr, :cl, :min, :max
								)');
		$insertdata = array(
								':id' => $value['no_range'], 
								':vr' => $value['var_range'],
								':cl' => $value['cluster_range'],
								':min' => $min,
								':max' => $max
							);
		$pdo->execute($insertdata);
	}

	// Mengatur Nilai Maxsimal dan minimal------------------------------------------------------
	$nm_array_min_max=array("tih","nlp","np","nd");
	$var_array_min_max=array("Time In Hospital","Num Lab Procedures",
							"Num Procedures","Num Diagnoses");
	$i=0;
	foreach ($nm_array_min_max as $min_max ) 
	{
		for($a=0;$a<3;$a++)
		{
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('SELECT * FROM tbl_range_kmean 
									WHERE var_rk="'.$var_array_min_max[$i].'" 
									ORDER BY CAST(max_rk AS INT) DESC LIMIT '.$a.',1');
			$pdo->execute();
			$row= $pdo->fetch(PDO::FETCH_OBJ);
				${$min_max."_min_max"}[] = array(
					'no_range'		=> $row->id_rk,
					'var_range' 	=> $row->var_rk,
					'cluster_range' => $row->cluster_rk,
					'min_range'		=> $row->min_rk,
					'max_range'		=> $row->max_rk
				);
		}

		// Max-Middle
		if(${$min_max."_min_max"}[0]['min_range']!="None" &&
			${$min_max."_min_max"}[1]['max_range']!="None")
		{
			$nilaiselisih=0;
			$nilaiselisih=intval(${$min_max."_min_max"}[0]['min_range'])-
							intval(${$min_max."_min_max"}[1]['max_range']);
			if(intval($nilaiselisih)>1)
			{
				$nilaiselisih-=1;
				$nilaiselisih=floor(intval($nilaiselisih)/2);
				${$min_max."_min_max"}[0]['min_range'] = intval(${$min_max."_min_max"}[0]
															['min_range']) - 
															intval($nilaiselisih);
				${$min_max."_min_max"}[1]['max_range'] = intval(${$min_max."_min_max"}[1]
															['max_range']) + 
															intval($nilaiselisih);
			}
		}

		// Middle-Min
		if(${$min_max."_min_max"}[1]['min_range']!="None" &&
			${$min_max."_min_max"}[2]['max_range']!="None")
		{
			$nilaiselisih=0;
			$nilaiselisih=intval(${$min_max."_min_max"}[1]['min_range'])-
							intval(${$min_max."_min_max"}[2]['max_range']);
			if(intval($nilaiselisih)>1)
			{
				$nilaiselisih-=1;
				$nilaiselisih=floor(intval($nilaiselisih)/2);
				${$min_max."_min_max"}[1]['min_range'] = intval(${$min_max."_min_max"}[1]
															['min_range']) - 
															intval($nilaiselisih);
				${$min_max."_min_max"}[2]['max_range'] = intval(${$min_max."_min_max"}[2]
															['max_range']) + 
															intval($nilaiselisih);
			}
		}
		$i++;
	}
	// -----------------------------------------------------------------------------------------
	// echo json_encode($np_min_max);
	// Kosongkan tabel range kmean-2
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('TRUNCATE tbl_range_kmean');
	$pdo->execute();

	// Insert Tabel Range Kmean-2
	$i=0;
	foreach ($nm_array_min_max as $nm_min_max ) {
		foreach (${$nm_min_max."_min_max"} as $key => $value) 
		{
			$i++;
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('INSERT INTO tbl_range_kmean (
										id_rk, var_rk, cluster_rk, min_rk, max_rk
										) 
										VALUES 
										(
										:id, :vr, :cl, :min, :max
									)');
			$insertdata = array(
									':id' => $i, 
									':vr' => $value['var_range'],
									':cl' => $value['cluster_range'],
									':min' => $value['min_range'],
									':max' => $value['max_range']
								);
			$pdo->execute($insertdata);
		}
	}

	// Tampil hasil range
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT * FROM tbl_range_kmean');
	$pdo->execute();

	while($row= $pdo->fetch(PDO::FETCH_OBJ))
	{
		$data2[] = array(
				'no_range'		=> $row->id_rk,
				'var_range' 	=> $row->var_rk,
				'cluster_range' => $row->cluster_rk,
				'min_range'		=> $row->min_rk,
				'max_range'		=> $row->max_rk
		);
	}

	echo json_encode($data2);
?>