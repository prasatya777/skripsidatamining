<?php
  include '../../koneksi/koneksi.php';
  if (session_status() == PHP_SESSION_NONE) 
  {
      session_start();
      ob_start();
  }

  include '../../page-admin/authentication/authenc_code.php';
  
  include '../../vendor/PHPExcel-1.8/Classes/PHPExcel.php';
  include '../../back-end/page-kmean/style_excel_download_excel_code.php';

  $tanggal_export=date('d-m-Y');
  $objPHPExcel = new PHPExcel();

  $objPHPExcel->getActiveSheet()->setShowGridlines(false);

  $objPHPExcel->getActiveSheet()->getStyle("A1:P1")->applyFromArray($styleOri);

  $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
  $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
  $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
  $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
  $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
  $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
  $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
  $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
  $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
  $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
  $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
  $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
  $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
  $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
  $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
  $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
  
  $result = $conn->query('select * from tbl_hsl_kmean');

  $objPHPExcel->getActiveSheet()->setCellValue('A1','gender');
  $objPHPExcel->getActiveSheet()->setCellValue('B1','age');
  $objPHPExcel->getActiveSheet()->setCellValue('C1','admission_type_id');
  $objPHPExcel->getActiveSheet()->setCellValue('D1','discharge_disposition_id');
  $objPHPExcel->getActiveSheet()->setCellValue('E1','admission_source_id');
  $objPHPExcel->getActiveSheet()->setCellValue('F1','time_in_hospital');
  $objPHPExcel->getActiveSheet()->setCellValue('G1','num_lab_procedures');
  $objPHPExcel->getActiveSheet()->setCellValue('H1','num_procedures');
  $objPHPExcel->getActiveSheet()->setCellValue('I1','diag_1');
  $objPHPExcel->getActiveSheet()->setCellValue('J1','diag_2');
  $objPHPExcel->getActiveSheet()->setCellValue('K1','diag_3');
  $objPHPExcel->getActiveSheet()->setCellValue('L1','number_diagnoses');
  $objPHPExcel->getActiveSheet()->setCellValue('M1','A1Cresult');
  $objPHPExcel->getActiveSheet()->setCellValue('N1','change');
  $objPHPExcel->getActiveSheet()->setCellValue('O1','diabetesMed');
  $objPHPExcel->getActiveSheet()->setCellValue('P1','readmitted');

  $i=1;

  while($row=$result->fetch(PDO::FETCH_OBJ)){
    $i++;

    $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $row->gender_hsl_k);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->age_hsl_k);
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->admission_type_id_hsl_k);
    $objPHPExcel->getActiveSheet()
    ->setCellValue('D'.$i, $row->discharge_disposition_id_hsl_k);
    $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->admission_source_id_hsl_k);
    $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->time_in_hospital_hsl_k);
    $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->num_lab_procedures_hsl_k);
    $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row->num_procedures_hsl_k);
    $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row->diag_1_hsl_k);
    $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $row->diag_2_hsl_k);
    $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $row->diag_3_hsl_k);
    $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $row->number_diagnoses_hsl_k);
    $objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $row->A1Cresult_hsl_k);
    $objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $row->change_hsl_k);
    $objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $row->diabetesMed_hsl_k);
    $objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $row->readmitted_hsl_k);

    $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleOri);
  }

  $objPHPExcel->getActiveSheet()->setTitle('HASIL_K-MEANS');

  header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  header('Content-Disposition: attachment;filename="File-Export-Hasil_K-MEANS('.$tanggal_export.').xlsx"');
  header('Cache-Control: max-age=0');
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
  $objWriter->save('php://output');
  exit;
?>