<?php
	include '../../koneksi/koneksi.php';

	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}
	include '../../page-admin/authentication/authenc_code.php';

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('TRUNCATE tbl_data_mentah');
	$pdo->execute();

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('TRUNCATE tbl_centroid_kmean_tih');
	$pdo->execute();

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('TRUNCATE tbl_centroid_kmean_nlp');
	$pdo->execute();

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('TRUNCATE tbl_centroid_kmean_np');
	$pdo->execute();

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('TRUNCATE tbl_centroid_kmean_nd');
	$pdo->execute();

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('TRUNCATE tbl_group_kmean');
	$pdo->execute();

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('TRUNCATE tbl_hsl_kmean');
	$pdo->execute();

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('TRUNCATE tbl_range_kmean');
	$pdo->execute();

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('TRUNCATE tbl_hsl_c45');
	$pdo->execute();
?>