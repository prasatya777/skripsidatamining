<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}
	include '../../page-admin/authentication/authenc_code.php';

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT * FROM tbl_centroid_kmean');
	$pdo->execute();

	while($row= $pdo->fetch(PDO::FETCH_OBJ))
	{
		$data[] = array(
			'id'	 					=> $row->id_ck,
			'gol_centroid' 				=> "Centroid-".$row->gol_centroid_ck,
			'time_in_hospital'			=> $row->time_in_hospital_ck,
			'num_lab_procedures'		=> $row->num_lab_procedures_ck,
			'num_procedures'			=> $row->num_procedures_ck,
			'number_diagnoses'			=> $row->number_diagnoses_ck
		);
	}

	echo json_encode($data);
?>