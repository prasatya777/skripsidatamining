<?php
	//Untuk mengosongkan isi tabel Centroid dan Hasil K-means
	deleteTabel();

	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';
	
	include '../../vendor/PHPExcel-1.8/Classes/PHPExcel.php';

		$inputFile = $_FILES["ImportExcelFile"]["tmp_name"];
        $inputFileType = PHPExcel_IOFactory::identify($inputFile);
        $excelReader = PHPExcel_IOFactory::createReader($inputFileType);
        $excelObj = $excelReader->load($inputFile);

		$worksheet = $excelObj->getSheet(0);
		$lastRow = $worksheet->getHighestRow();

		//Mengambil Nilai ID Terakhir
		include '../../koneksi/koneksi.php';
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$result = $conn->prepare('SELECT id_dm
									FROM tbl_data_mentah
									ORDER BY id_dm DESC LIMIT 1
									');
		$result->execute();
		$count = $result->rowCount();
		
		if($count>0)
		{
			$row = $result->fetch(PDO::FETCH_OBJ);
			$no=$row->id_dm;	
		}
		else
		{
			$no=0;
		}
		
		
		for ($row = 2; $row <= $lastRow; $row++) {

			$no++;
			$gender = $worksheet->getCell('A'.$row)->getValue();
			$age = $worksheet->getCell('B'.$row)->getValue();
			$admission_type_id	= $worksheet->getCell('C'.$row)->getValue();
			$discharge_disposition_id	= $worksheet->getCell('D'.$row)->getValue();
			$admission_source_id	= $worksheet->getCell('E'.$row)->getValue();
			$time_in_hospital	= $worksheet->getCell('F'.$row)->getValue();
			$num_lab_procedures = $worksheet->getCell('G'.$row)->getValue();
			$num_procedures = $worksheet->getCell('H'.$row)->getValue();
			$diag_1 = $worksheet->getCell('I'.$row)->getValue();
			$diag_2 = $worksheet->getCell('J'.$row)->getValue();
			$diag_3 = $worksheet->getCell('K'.$row)->getValue();
			$number_diagnoses = $worksheet->getCell('L'.$row)->getValue();
			$A1Cresult = $worksheet->getCell('M'.$row)->getValue();
			$change = $worksheet->getCell('N'.$row)->getValue();
			$diabetesMed = $worksheet->getCell('O'.$row)->getValue();
			$readmitted = $worksheet->getCell('P'.$row)->getValue();

			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('INSERT INTO tbl_data_mentah (
										id_dm,
										gender_dm, 
										age_dm, 
										admission_type_id_dm, 
										discharge_disposition_id_dm, 
										admission_source_id_dm, 
										time_in_hospital_dm, 
										num_lab_procedures_dm, 
										num_procedures_dm,
										diag_1_dm, 
										diag_2_dm, 
										diag_3_dm,
										number_diagnoses_dm,
										A1Cresult_dm,
										change_dm,
										diabetesMed_dm,
										readmitted_dm
										) 
										VALUES 
										(
										:id,
										:gndr, 
										:ag, 
										:ati,
										:ddi, 
										:asi, 
										:tih, 
										:nlp, 
										:np, 
										:dg1, 
										:dg2, 
										:dg3,
										:nd,
										:a1c,
										:chng,
										:dm,
										:read

									)');
			$insertdata = array(
									':id' => $no,
									':gndr' => $gender, ':ag' => $age, 
									':ati' => $admission_type_id, 
									':ddi' => $discharge_disposition_id, 
									':asi' => $admission_source_id, 
									':tih' => $time_in_hospital,
									':nlp' => $num_lab_procedures, 
									':np' => $num_procedures,
									':dg1' => $diag_1, ':dg2' => $diag_2, 
									':dg3' => $diag_3, ':nd' => $number_diagnoses,
									':a1c' => $A1Cresult, ':chng' => $change,
									':dm' => $diabetesMed, ':read' => $readmitted
								);
			$pdo->execute($insertdata);
		}

		//Untuk mengosongkan isi tabel Centroid dan Hasil K-means
		function deleteTabel()
		{
			include '../../koneksi/koneksi.php';
			
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('TRUNCATE tbl_centroid_kmean_tih');
			$pdo->execute();

			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('TRUNCATE tbl_centroid_kmean_nlp');
			$pdo->execute();

			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('TRUNCATE tbl_centroid_kmean_np');
			$pdo->execute();

			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('TRUNCATE tbl_centroid_kmean_nd');
			$pdo->execute();

			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('TRUNCATE tbl_group_kmean');
			$pdo->execute();
	
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('TRUNCATE tbl_hsl_kmean');
			$pdo->execute();
		}

?>