<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}
	include '../../page-admin/authentication/authenc_code.php';

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT * FROM tbl_data_mentah');
	$pdo->execute();

	while($row= $pdo->fetch(PDO::FETCH_OBJ))
	{
		$data[] = array(
			'id'	 					=> $row->id_dm,
			'gender' 					=> $row->gender_dm,
			'age' 						=> $row->age_dm,
			'admission_type_id'			=> $row->admission_type_id_dm,
			'discharge_disposition_id'	=> $row->discharge_disposition_id_dm,
			'admission_source_id'		=> $row->admission_source_id_dm,
			'time_in_hospital'			=> $row->time_in_hospital_dm,
			'num_lab_procedures'		=> $row->num_lab_procedures_dm,
			'num_procedures'			=> $row->num_procedures_dm,
			'diag_1'					=> $row->diag_1_dm,
			'diag_2'					=> $row->diag_2_dm,
			'diag_3'					=> $row->diag_3_dm,
			'number_diagnoses'			=> $row->number_diagnoses_dm,
			'A1Cresult'					=> $row->A1Cresult_dm,
			'change'					=> $row->change_dm,
			'diabetesMed'				=> $row->diabetesMed_dm,
			'readmitted'				=> $row->readmitted_dm	
		);
	}

	echo json_encode($data);
?>