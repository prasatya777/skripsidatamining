<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}
	include '../../page-admin/authentication/authenc_code.php';

//Insert To Tabel Hasil K-Means (Hasil Akhir K-Means Dan telah dikelompokkan)
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT 
							id_dm, 
							gender_dm, 
							age_dm, 
							admission_type_id_dm,
							discharge_disposition_id_dm,
							admission_source_id_dm,
							diag_1_dm,
							diag_2_dm,
							diag_3_dm,
							A1Cresult_dm,
							change_dm,
							diabetesMed_dm,
							readmitted_dm
							FROM tbl_data_mentah
						');

	$pdo->execute();
	while($row= $pdo->fetch(PDO::FETCH_OBJ))
	{
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$insert = $conn->prepare('INSERT INTO tbl_hsl_kmean (
									id_hsl_k,
									gender_hsl_k, 
									age_hsl_k, 
									admission_type_id_hsl_k, 
									discharge_disposition_id_hsl_k, 
									admission_source_id_hsl_k,  
									diag_1_hsl_k, 
									diag_2_hsl_k, 
									diag_3_hsl_k,
									A1Cresult_hsl_k,
									change_hsl_k,
									diabetesMed_hsl_k,
									readmitted_hsl_k
									) 
									VALUES 
									(
									:id,
									:gndr, 
									:ag, 
									:ati,
									:ddi, 
									:asi, 
									:dg1, 
									:dg2, 
									:dg3,
									:a1c,
									:chng,
									:dm,
									:read
								)');

		$insertdata = array(
								':id' => $row->id_dm,
								':gndr' => $row->gender_dm, 
								':ag' => $row->age_dm, 
								':ati' => $row->admission_type_id_dm, 
								':ddi' => $row->discharge_disposition_id_dm, 
								':asi' => $row->admission_source_id_dm, 
								':dg1' => $row->diag_1_dm, 
								':dg2' => $row->diag_2_dm, 
								':dg3' => $row->diag_3_dm, 
								':a1c' => $row->A1Cresult_dm, 
								':chng' => $row->change_dm,
								':dm' => $row->diabetesMed_dm, 
								':read' => $row->readmitted_dm
							);
		$insert->execute($insertdata);
	}

	//Update KMeans Hasil - group_kmean
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT id_gk,
							gol_time_in_hospital_gk,
							gol_num_lab_procedures_gk,
							gol_num_procedures_gk,
							gol_number_diagnoses_gk
							 FROM tbl_group_kmean');
	$pdo->execute();
	while($row= $pdo->fetch(PDO::FETCH_OBJ))
	{
		try {
				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$pdo2 = $conn->prepare('UPDATE tbl_hsl_kmean
												set
												time_in_hospital_hsl_k =:tih,
												num_lab_procedures_hsl_k =:nlp,
												num_procedures_hsl_k =:np,
												number_diagnoses_hsl_k =:nd
												WHERE id_hsl_k =:id');
				$updatedata2 = array(
									':id' => $row->id_gk,
									':tih' => $row->gol_time_in_hospital_gk,
									':nlp' => $row->gol_num_lab_procedures_gk,
									':np' => $row->gol_num_procedures_gk,
									':nd' => $row->gol_number_diagnoses_gk
									);
				$pdo2->execute($updatedata2);

			} catch (PDOexception $e) {
				die();
			}
	}
?>