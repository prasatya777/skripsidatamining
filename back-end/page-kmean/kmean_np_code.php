<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}
	include '../../page-admin/authentication/authenc_code.php';

do
{
	//Hitung Jumlah Data
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo = $conn->prepare('SELECT COUNT(id_dm) AS TotalData FROM tbl_data_mentah');

		$pdo->execute(); 
		$row= $pdo->fetch(PDO::FETCH_OBJ);

		$totaldata = $row->TotalData;

	//Penetuan Centroid Awal )(Random)
		$centroidawal_1 = rand(1,intval($totaldata));

		do
		{
			$centroidawal_2 = rand(1,intval($totaldata));
		}while($centroidawal_2==$centroidawal_1);

		do
		{
			$centroidawal_3 = rand(1,intval($totaldata));
		}while($centroidawal_3==$centroidawal_1 || $centroidawal_3==$centroidawal_2);
		// $centroidawal_1 = 1;
		// $centroidawal_2 = 5;
		// $centroidawal_3 = 9;
		
	//Mengambil Nilai Centroid Awal

		//Centroid Awal
		$huruf="a";
		for($i=0;$i<3;$i++)
		{
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('SELECT num_procedures_dm FROM tbl_data_mentah WHERE 
				id_dm = :id');

			$pdo->execute(array(':id' => ${"centroidawal_".($i+1)}));
			$row= $pdo->fetch(PDO::FETCH_OBJ);

			${"centroid_".$huruf}=array(
				array($row->num_procedures_dm)
			);

			//Save Insert Database Centroid Awal Centroid=0
			$gol = $i;
			insertTblCentroid($gol, $row->num_procedures_dm);

			$huruf++;
		}

	//Start Looping
	do
	{
	//Menghitung Nilai Centroid Selanjutnya
		//Tabel Hitung Nilai Kolom X dan Kolom Y
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo = $conn->prepare('SELECT id_dm, num_procedures_dm,
			SQRT(
				(POW((num_procedures_dm-:tih_a),2))
				) AS NilaiX,
			SQRT(
				(POW((num_procedures_dm-:tih_b),2)) 
				) AS NilaiY,
			SQRT(
				(POW((num_procedures_dm-:tih_c),2))
				) AS NilaiZ
			FROM tbl_data_mentah');

		$view = array(
						':tih_a' => $centroid_a[0][0], 

						':tih_b' => $centroid_b[0][0], 

						':tih_c' => $centroid_c[0][0], 
						);

		$pdo->execute($view);

		$total_centroid_0 = 0;
		$total_centroid_1 = 0;
		$total_centroid_2 = 0;

		while($row= $pdo->fetch(PDO::FETCH_OBJ))
		{
			//Penentuan Mana Yang Termasuk Kelompok Centroid 0
			if(strval($row->NilaiX) < strval($row->NilaiY) && strval($row->NilaiX) < strval($row->NilaiZ))
			{
				$hasil_centroid_0[] = array(
										$row->id_dm,
										$row->num_procedures_dm
				);
				$total_centroid_0++;
			}

			//Penentuan Mana Yang Termasuk Kelompok Centroid 1
			if(strval($row->NilaiY) < strval($row->NilaiX) && strval($row->NilaiY) < strval($row->NilaiZ))
			{
				$hasil_centroid_1[] = array(
										$row->id_dm,
										$row->num_procedures_dm
				);
				$total_centroid_1++;
			}
			
			//Penentuan Mana Yang Termasuk Kelompok Centroid 2
			if(strval($row->NilaiZ) < strval($row->NilaiX) && strval($row->NilaiZ) < strval($row->NilaiY))
			{
				$hasil_centroid_2[] = array(
										$row->id_dm,
										$row->num_procedures_dm
				);
				$total_centroid_2++;
			}

		}
		
		//Perhitungan Untuk mementukan Centroid Baru Centroid
		$huruf="a";
		for($j=0;$j<3;$j++)
		{
			${"total_tih_".$j} = 0;
			settype(${"total_tih_".$j}, 'float');

			for($i=0; $i<${"total_centroid_".$j}; $i++)
			{
				${"total_tih_".$j} += floatval(${"hasil_centroid_".$j}[$i][1]);
			}

				${"total_tih_".$j} = (floatval(${"total_centroid_".$j})!=0)?
									floatval(${"total_tih_".$j})/
									floatval(${"total_centroid_".$j}):0;


				unset(${"centroid_".$huruf});
				if(is_nan(${"total_tih_".$j}))
				{
					${"total_tih_".$j}=0;
				}

				${"centroid_".$huruf}=array(
					array(${"total_tih_".$j})
				);
				$huruf++;
		}



	//Menggambil Nilai Centroid Dari Database
		$huruf="a";
		for($i=0;$i<3;$i++)
		{
			//Mengambil Nilai Centroid
			${"cek_centroid_".$i} = getnilaiCentroid($i, ${"centroid_".$huruf}[0][0]);

			//Save Insert Database Centroid
			insertTblCentroid($i, ${"centroid_".$huruf}[0][0]);

			$huruf++;	
		}


		if($cek_centroid_0==0 || $cek_centroid_1==0 || $cek_centroid_2==0)
		{
		unset($hasil_centroid_0); unset($hasil_centroid_1); unset($hasil_centroid_2);
		}

		
	} while($cek_centroid_0==0 || $cek_centroid_1==0 || $cek_centroid_2==0);
	//End Looping

	//Insert To Tabel Hasil K-Means (Hasil Akhir K-Means Dan telah dikelompokkan)
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo = $conn->prepare('SELECT * FROM tbl_data_mentah');
		$pdo->execute();
		while($row= $pdo->fetch(PDO::FETCH_OBJ))
		{
			$hsl_centroid="";
			for($j=0;$j<3;$j++)
			{
				for($i=0; $i<${"total_centroid_".$j}; $i++)
				{
					if($row->id_dm == ${"hasil_centroid_".$j}[$i][0])
					{
						$hsl_centroid=$j+1;
					}
				}
			}

			if($hsl_centroid=="")
			{
				$hsl_centroid=4;
			}

			//Update ke group k-means
			try {
					$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$pdo2 = $conn->prepare('UPDATE tbl_group_kmean
													set
													num_procedures_gk =:np,
													gol_num_procedures_gk =:gol
													WHERE id_gk =:id');
					$updatedata2 = array(
										':id' => $row->id_dm,
										':np' => $row->num_procedures_dm,
										':gol' => $hsl_centroid
										);
					$pdo2->execute($updatedata2);

				} catch (PDOexception $e) {
					die();
				}
		}

	// Cek apakah nilai golongan masih kosong atau tidak
	$nilai_gol=0;

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT gol_num_procedures_gk FROM 
							tbl_group_kmean ORDER BY gol_num_procedures_gk ASC LIMIT 1');
	$pdo->execute(); 
	$row= $pdo->fetch(PDO::FETCH_OBJ);

	if(empty($row->gol_num_procedures_gk) AND strval($row->gol_num_procedures_gk)!="0")
	{
		$nilai_gol+=1;
	}
	else
	{
		$nilai_gol+=0;
	}

	// Delete tbl_centroid
	if($nilai_gol>0)
	{
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo = $conn->prepare('TRUNCATE tbl_centroid_kmean_np');
		$pdo->execute();
	}

}while($nilai_gol>0);
// ---------------------------------------------------------------------------------------------

//Function Insert To Tabel Centroid K-Means
	function insertTblCentroid($gol, $tih)
	{
			include '../../koneksi/koneksi.php';
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('INSERT INTO tbl_centroid_kmean_np (
										gol_centroid_ck,
										num_procedures_ck
										) 
										VALUES 
										(
										:gc, :tih
									)');
			$insertdata = array(
									':gc' => $gol, 
									':tih' => $tih
								);
			$pdo->execute($insertdata);
	}

	function getnilaiCentroid($gol, $tih)
	{
		include '../../koneksi/koneksi.php';
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$result = $conn->prepare('SELECT num_procedures_ck
									FROM tbl_centroid_kmean_np
									WHERE gol_centroid_ck = '.$gol.' 
									ORDER BY id_ck DESC LIMIT 2
									');
		$result->execute();
		$row = $result->fetch(PDO::FETCH_OBJ);

		if( strval($row->num_procedures_ck) == strval($tih) )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
?>