<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}
	//Hitung Total Data Pada Tabel Hasil K-Means
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$result = $conn->prepare('SELECT COUNT(id_hsl_k) AS TotalD FROM tbl_hsl_kmean');
	$result->execute();
	$row = $result->fetch(PDO::FETCH_OBJ);
	$TotalData = $row->TotalD;

	//Mengambil Nama Field Database
	$namafield=array();

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$result = $conn->prepare('SELECT * FROM tbl_hsl_kmean LIMIT 0');
	$result->execute();
	$count_column = $result->columnCount();

	for ($i = 1; $i < $count_column ; $i++) 
	{
	    $col = $result->getColumnMeta($i);
	    array_push($namafield, $col['name']);
	}

	//Proses Distinct
	$GLOBALS['nilai_variabel'][]= array();
	for($i=0; $i < ($count_column-1);$i++)
	{
		searchDistinct($i, $namafield[$i], $namafield[$i]);
	}

	//Menghitung Total Entropy HBA1C
	$TotalEntropy_A1CResult = 0;
	settype($TotalEntropy_A1CResult, 'float');

	for($i=0; $i < count($nilai_variabel[12])-1; $i++)
	{
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$result = $conn->prepare('SELECT COUNT(id_hsl_k) AS Total FROM tbl_hsl_kmean  WHERE '.$nilai_variabel[12][0].' = :nilai');
		$kunci = array(
						':nilai' => $nilai_variabel[12][$i+1]
				);
		$result->execute($kunci);
		$row = $result->fetch(PDO::FETCH_OBJ);
		$jumlah = $row->Total;

		$entropy=(($jumlah/$TotalData)*-1)*(LOG(($jumlah/$TotalData),2));

		$TotalEntropy_A1CResult+=floatval($entropy);
	}

	//Menghitung Total Entropy
	$variabel_entropy[]= array();
	$no_urut=0;
	//-Looping Nama Field
	for($j=0;$j<count($nilai_variabel);$j++)
	{
		//Kondisi HBA1C Tidak Boleh Diproses
		if($nilai_variabel[$j][0] != $nilai_variabel[12][0])
		{
			//Looping Berdasakran Jumlah Jenis Data Pada Tiap Field
			for($k=0;$k<count($nilai_variabel[$j])-1;$k++)
			{
				$totalentropy_variabel_other = 0;
				settype($totalentropy_variabel_other, 'float');

				$variabel_entropy[$no_urut][0] = $nilai_variabel[$j][0];
				$variabel_entropy[$no_urut][1] = $nilai_variabel[$j][$k+1];

				//Looping Berdasarkan Jumlah Jenis Dari HBA1C
				for($i=0; $i<count($nilai_variabel[12])-1; $i++)
				{
					$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$result = $conn->prepare('SELECT COUNT(id_hsl_k) AS Total_Jns_Var FROM tbl_hsl_kmean WHERE '.$nilai_variabel[$j][0].' = :nilai');
					$kunci = array(
									':nilai' => $nilai_variabel[$j][$k+1]
							);
					$result->execute($kunci);
					$row = $result->fetch(PDO::FETCH_OBJ);
					$total_jns_var = $row->Total_Jns_Var;

					$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$result = $conn->prepare('SELECT COUNT(id_hsl_k) AS Total FROM tbl_hsl_kmean  WHERE '.$nilai_variabel[12][0].' = :nilai AND 
						'.$nilai_variabel[$j][0].' = :nilai2'
						);
					$kunci = array(
									':nilai' => $nilai_variabel[12][$i+1],
									':nilai2' => $nilai_variabel[$j][$k+1]
							);
					$result->execute($kunci);
					$row = $result->fetch(PDO::FETCH_OBJ);
					$jumlah = $row->Total;

					$entropy=(($jumlah/$total_jns_var)*-1)*(LOG(($jumlah/$total_jns_var),2));

					if(is_nan($entropy))
					{
						$entropy=0;
					}

					$totalentropy_variabel_other+=floatval($entropy);

					$variabel_entropy[$no_urut][2] = $total_jns_var;
					$variabel_entropy[$no_urut][$i+6] = $jumlah;

					// echo "Entropy ".$nilai_variabel[$j][0]." -> ".$nilai_variabel[12][$i+1]." -> ".$nilai_variabel[$j][$k+1]." :".$entropy."</br>";
				}
				$variabel_entropy[$no_urut][3] = $totalentropy_variabel_other;
				$variabel_entropy[$no_urut][4] = (floatval($total_jns_var)/floatval($TotalData))*floatval($totalentropy_variabel_other);
				// echo "Total Entropy ".$nilai_variabel[$j][0]." :".$totalentropy_variabel_other."</br></br>";
				$no_urut++;
			}
		}
	}


	//Menghitung Total Gain Variabel Lain
	$variabel_gain[]= array();
	$cek_max_gain=array();
	//-Looping Nama Field
	for($i=0;$i<count($nilai_variabel);$i++)
	{
		if($nilai_variabel[$i][0] != $nilai_variabel[12][0])
		{
			$variabel_gain[$i][1] = 0;
			//Looping Pencarian Entropy Tiap NamaField
			for($j=0;$j<count($variabel_entropy);$j++)
			{
				if(strval($nilai_variabel[$i][0]) == strval($variabel_entropy[$j][0]))
				{
					$variabel_gain[$i][0] = $variabel_entropy[$j][0];
					$variabel_gain[$i][1] += $variabel_entropy[$j][4];
				}
			}
			$variabel_gain[$i][2] = floatval($TotalEntropy_A1CResult) - floatval($variabel_gain[$i][1]);
			array_push($cek_max_gain,$variabel_gain[$i][2]);
		}
	}


	//Memasukkan Nilai Gain Pada Array $variabel_entropy
	for($i=0;$i<count($nilai_variabel);$i++)
	{
		//Looping Pencarian Entropy Tiap NamaField
		for($j=0;$j<count($variabel_entropy);$j++)
		{
			if(strval($nilai_variabel[$i][0]) == strval($variabel_entropy[$j][0]))
			{
				$variabel_entropy[$j][5] = $variabel_gain[$i][2];
			}
		}
	}

	//Mencari Nilai Giant Tertinggi
	$gaint_akhir[]=array();
	$no_gaint=0;
	for($j=0;$j<count($variabel_entropy);$j++)
		{
			if(strval($variabel_entropy[$j][5]) == strval(max($cek_max_gain)))
			{
				for($k=0;$k<count($variabel_entropy[$j]);$k++)
				{
					$gaint_akhir[$no_gaint][$k] = $variabel_entropy[$j][$k];
				}
				$no_gaint++;
			}
		}

		//Mengambil Nilai ID Terakhir
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$result = $conn->prepare('SELECT id_hc45
									FROM tbl_hsl_c45
									ORDER BY id_hc45 DESC LIMIT 1
									');
		$result->execute();
		$count = $result->rowCount();
		
		if($count>0)
		{
			$row = $result->fetch(PDO::FETCH_OBJ);
			$no=$row->id_hc45;	
		}
		else
		{
			$no=1;
		}

	//Input Hasil C4.5
	for($j=0;$j<count($gaint_akhir);$j++)
		{
			$prediksi=0;
			$angka=0;
			for($k=0;$k<count($gaint_akhir[$j])-6;$k++)
			{
				$angka++;
				if(intval($gaint_akhir[$j][$k+6])>0)
				{
					$prediksi+=$angka;
				}
			}
			if($prediksi == 1)
			{
				$hsl_prediksi="Norm";
			}
			else if($prediksi == 2)
			{
				$hsl_prediksi=">8";
			}
			else if($prediksi == 3)
			{
				$hsl_prediksi=">7";
			}
			else
			{
				$hsl_prediksi="None";
			}

			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('INSERT INTO tbl_hsl_c45 (
										id_hc45,
										no_hc45,
										nmvariabel_hc45,
										value_hc45,
										entropy_hc45,
										gain_hc45,
										perkiraan_hc45
										) 
										VALUES 
										(
										:id, :no, :nm, :vl, :entropy, :gain, :prdks
									)');
			$insertdata = array(
									':id' => $no, 
									':no' => $j, 
									':nm' => $gaint_akhir[$j][0], 
									':vl' => $gaint_akhir[$j][1], 
									':entropy' => $gaint_akhir[$j][3],
									':gain' => $gaint_akhir[$j][5],
									':prdks' => $hsl_prediksi
								);
			$pdo->execute($insertdata);
		}

		//Cek Apakah Masih Terdapat NONE atau Tidak Pada perhitungan sebelumnya
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$result = $conn->prepare('SELECT perkiraan_hc45
									FROM tbl_hsl_c45
									WHERE id_hc45 = :id AND perkiraan_hc45 = :prdks
									');
		$cek = array(':id' => $no, ':prdks' => "None")
		$result->execute($cek);
		$cek_none = $result->rowCount();
		

	// print_r($gaint_akhir);

	function searchDistinct($i, $variabel, $field)
	{
		include '../../koneksi/koneksi.php';
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo = $conn->prepare('SELECT DISTINCT '.$field.' 
			FROM tbl_hsl_kmean');

		$pdo->execute();
		$GLOBALS['nilai_variabel'][$i][0] = $variabel;
		$a=0; 
		while($row= $pdo->fetch(PDO::FETCH_OBJ))
		{
			$a++;
			$GLOBALS['nilai_variabel'][$i][$a]= $row->$field;
		}
	}

?>