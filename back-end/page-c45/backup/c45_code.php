<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}
	//Hitung Total Data Pada Tabel Hasil K-Means
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$result = $conn->prepare('SELECT COUNT(id_hsl_k) AS TotalD FROM tbl_hsl_kmean');
	$result->execute();
	$row = $result->fetch(PDO::FETCH_OBJ);
	$TotalData = $row->TotalD;

	//Mengambil Nama Field Database
	$namafield=array();

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$result = $conn->prepare('SELECT * FROM tbl_hsl_kmean LIMIT 0');
	$result->execute();
	$count_column = $result->columnCount();

	for ($i = 1; $i < $count_column ; $i++) 
	{
	    $col = $result->getColumnMeta($i);
	    array_push($namafield, $col['name']);
	}

	//Proses Distinct
	$GLOBALS['nilai_variabel'][]= array();
	for($i=0; $i < ($count_column-1);$i++)
	{
		searchDistinct($i, $namafield[$i], $namafield[$i]);
	}

	$nilai_variabel_hba1c=array();

	for($i=0; $i < count($nilai_variabel[12]); $i++)
	{
		array_push($nilai_variabel_hba1c, $nilai_variabel[12][$i]);
	}

	//Menghitung Total Entropy HBA1C
	$TotalEntropy_Root = 0;
	settype($TotalEntropy_Root, 'float');

	for($i=0; $i < count($nilai_variabel[12])-1; $i++)
	{
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$result = $conn->prepare('SELECT COUNT(id_hsl_k) AS Total FROM tbl_hsl_kmean  WHERE '.$nilai_variabel[12][0].' = :nilai');
		$kunci = array(
						':nilai' => $nilai_variabel[12][$i+1]
				);
		$result->execute($kunci);
		$row = $result->fetch(PDO::FETCH_OBJ);
		$jumlah = $row->Total;

		$entropy=(($jumlah/$TotalData)*-1)*(LOG(($jumlah/$TotalData),2));

		if(is_nan($entropy))
			{
				$entropy=0;
			}

		$TotalEntropy_Root+=floatval($entropy);
	}

	//Menghitung Total Entropy Awal
	$variabel_entropy[]= array();
	$no_urut=0;
	//-Looping Nama Field
	for($j=0;$j<count($nilai_variabel);$j++)
	{
		//Kondisi HBA1C Tidak Boleh Diproses
		if($nilai_variabel[$j][0] != $nilai_variabel[12][0] || $nilai_variabel[$j][0] != 0)
		{
			//Looping Berdasakran Jumlah Jenis Data Pada Tiap Field
			for($k=0;$k<count($nilai_variabel[$j])-1;$k++)
			{
				$totalentropy_variabel_other = 0;
				settype($totalentropy_variabel_other, 'float');

				$variabel_entropy[$no_urut][0] = $nilai_variabel[$j][0];
				$variabel_entropy[$no_urut][1] = $nilai_variabel[$j][$k+1];

				//Looping Berdasarkan Jumlah Jenis Dari HBA1C
				for($i=0; $i<count($nilai_variabel[12])-1; $i++)
				{
					$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$result = $conn->prepare('SELECT COUNT(id_hsl_k) AS Total_Jns_Var FROM tbl_hsl_kmean WHERE '.$nilai_variabel[$j][0].' = :nilai');
					$kunci = array(
									':nilai' => $nilai_variabel[$j][$k+1]
							);
					$result->execute($kunci);
					$row = $result->fetch(PDO::FETCH_OBJ);
					$total_jns_var = $row->Total_Jns_Var;

					$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$result = $conn->prepare('SELECT COUNT(id_hsl_k) AS Total FROM tbl_hsl_kmean  WHERE '.$nilai_variabel[12][0].' = :nilai AND 
						'.$nilai_variabel[$j][0].' = :nilai2'
						);
					$kunci = array(
									':nilai' => $nilai_variabel[12][$i+1],
									':nilai2' => $nilai_variabel[$j][$k+1]
							);
					$result->execute($kunci);
					$row = $result->fetch(PDO::FETCH_OBJ);
					$jumlah = $row->Total;

					$entropy=(($jumlah/$total_jns_var)*-1)*(LOG(($jumlah/$total_jns_var),2));

					if(is_nan($entropy))
					{
						$entropy=0;
					}

					$totalentropy_variabel_other+=floatval($entropy);

					$variabel_entropy[$no_urut][2] = $total_jns_var;
					$variabel_entropy[$no_urut][$i+6] = $jumlah;
				}
				$variabel_entropy[$no_urut][3] = $totalentropy_variabel_other;
				$variabel_entropy[$no_urut][4] = (floatval($total_jns_var)/floatval($TotalData))*floatval($totalentropy_variabel_other);
				$no_urut++;
			}
		}
	}

	//Menghitung Total Gain Variabel Lain
	$variabel_gain[]= array();
	$cek_max_gain=array();
	//-Looping Nama Field
	for($i=0;$i<count($nilai_variabel);$i++)
	{
		if($nilai_variabel[$i][0] != $nilai_variabel[12][0] || $nilai_variabel[$i][0] != 0)
		{
			$variabel_gain[$i][1] = 0;
			//Looping Pencarian Entropy Tiap NamaField
			for($j=0;$j<count($variabel_entropy);$j++)
			{
				if(strval($nilai_variabel[$i][0]) == strval($variabel_entropy[$j][0]))
				{
					$variabel_gain[$i][0] = $variabel_entropy[$j][0];
					$variabel_gain[$i][1] += $variabel_entropy[$j][4];
				}
			}
			$variabel_gain[$i][2] = floatval($TotalEntropy_Root) - floatval($variabel_gain[$i][1]);
			array_push($cek_max_gain,$variabel_gain[$i][2]);
		}
	}


	//Memasukkan Nilai Gain Pada Array $variabel_entropy
	for($i=0;$i<count($nilai_variabel);$i++)
	{
		//Looping Pencarian Entropy Tiap NamaField
		for($j=0;$j<count($variabel_entropy);$j++)
		{
			if(strval($nilai_variabel[$i][0]) == strval($variabel_entropy[$j][0]))
			{
				$variabel_entropy[$j][5] = $variabel_gain[$i][2];
			}
		}
	}

	//Mencari Nilai Giant Tertinggi
	$gaint_akhir[]=array();
	$no_gaint=0;
	for($j=0;$j<count($variabel_entropy);$j++)
		{
			if(strval($variabel_entropy[$j][5]) == strval(max($cek_max_gain)))
			{
				for($k=0;$k<count($variabel_entropy[$j]);$k++)
				{
					$gaint_akhir[$no_gaint][$k] = $variabel_entropy[$j][$k];
				}
				$no_gaint++;
			}
		}

		//Mengambil Nilai ID Terakhir
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$result = $conn->prepare('SELECT id_hc45
									FROM tbl_hsl_c45
									ORDER BY id_hc45 DESC LIMIT 1
									');
		$result->execute();
		$count = $result->rowCount();
		
		if($count>0)
		{
			$row = $result->fetch(PDO::FETCH_OBJ);
			$no=intval($row->id_hc45)+1;	
		}
		else
		{
			$no=1;
		}

	//Input Hasil C4.5
	for($j=0;$j<count($gaint_akhir);$j++)
		{
			$prediksi=0;
			$angka=0;
			for($k=0;$k<count($gaint_akhir[$j])-6;$k++)
			{
				$angka++;
				if(intval($gaint_akhir[$j][$k+6])>0)
				{
					$prediksi+=$angka;
				}
			}
			if($prediksi == 1)
			{
				$hsl_prediksi=">7";
			}
			else if($prediksi == 2)
			{
				$hsl_prediksi=">8";
			}
			else if($prediksi == 3)
			{
				$hsl_prediksi="Norm";
			}
			else
			{
				$hsl_prediksi="None";
			}

			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('INSERT INTO tbl_hsl_c45 (
										id_hc45,
										no_hc45,
										nmvariabel_hc45,
										value_hc45,
										entropy_hc45,
										gain_hc45,
										perkiraan_hc45
										) 
										VALUES 
										(
										:id, :no, :nm, :vl, :entropy, :gain, :prdks
									)');
			$insertdata = array(
									':id' => $no, 
									':no' => $j, 
									':nm' => $gaint_akhir[$j][0], 
									':vl' => $gaint_akhir[$j][1], 
									':entropy' => $gaint_akhir[$j][3],
									':gain' => $gaint_akhir[$j][5],
									':prdks' => $hsl_prediksi
								);
			$pdo->execute($insertdata);
		}

		$nm_variabel_root = $gaint_akhir[0][0];

// ----------------------------------------------------------------------------

$tes=0;
do{
		//Hitung Jumlah ID Variabel Apa Saja Yang Telah Menjadi Root
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo = $conn->prepare('SELECT DISTINCT id_hc45	FROM tbl_hsl_c45');
		$pdo->execute();
		$jml_ID = $pdo->rowCount();

		//Cek Apakah Masih Terdapat NONE atau Tidak Pada perhitungan sebelumnya
		$no_done=0;
		do{
			$no_done++;
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$result = $conn->prepare('SELECT id_hc45, no_hc45, nmvariabel_hc45, value_hc45, 
										perkiraan_hc45 FROM tbl_hsl_c45
										WHERE id_hc45 = :id AND perkiraan_hc45 = :prdks
										');
			$cek = array(':id' => $no_done, ':prdks' => 'None');
			$result->execute($cek);
			$cek_none = $result->rowCount();
		
			if($cek_none>0)
			{
				$row = $result->fetch(PDO::FETCH_OBJ);
				$variabel_none = array(
										$row->id_hc45, $row->no_hc45, $row->nmvariabel_hc45,
										$row->value_hc45, $row->perkiraan_hc45
				);		
			}

			if($no_done > $jml_ID)
			{
				$cek_none = 1;
				$variabel_none[0]=0;
			}

		}while($cek_none == 0);

		// echo json_encode($variabel_none);

		if($variabel_none[0] > 0)
		{
			unset($nilai_variabel);unset($variabel_entropy);
			unset($variabel_gain);unset($cek_max_gain);
			unset($gaint_akhir);

			//Proses Distinct
			$nilai_variabel[]= array();
			for($i=0; $i < ($count_column-1);$i++)
			{
				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$pdo = $conn->prepare('SELECT DISTINCT '.$namafield[$i].' 
					FROM tbl_hsl_kmean WHERE '.$variabel_none[2].' = :vl
					ORDER BY '.$namafield[$i].' ASC
					');
				$cek = array(':vl' => $variabel_none[3]);
				$pdo->execute($cek);
					if($i != 12)
					{
						$nilai_variabel[$i][0] = $namafield[$i];
						$a=0; 
						$field = $namafield[$i];
						while($row= $pdo->fetch(PDO::FETCH_OBJ))
						{
							$a++;
							$nilai_variabel[$i][$a]= $row->$field;
						}	
					}
					else if($i == 12)
					{
						for($a=0;$a<count($nilai_variabel_hba1c);$a++)
						{
							$nilai_variabel[12][$a] = $nilai_variabel_hba1c[$a];
						}
					}

			}

			// echo json_encode($nilai_variabel);

			//Menghitung Total Entropy Root
			$TotalEntropy_Root = 0;
			settype($TotalEntropy_Root, 'float');

			for($i=0; $i < count($nilai_variabel[12])-1; $i++)
			{
				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$result = $conn->prepare('
										SELECT COUNT(id_hsl_k) AS Total FROM tbl_hsl_kmean  
										WHERE '.$nilai_variabel[12][0].' = :nilai
										AND '.$variabel_none[2].' = :nilai2
										');
				$kunci = array(
								':nilai' => $nilai_variabel[12][$i+1],
								':nilai2' => $variabel_none[3]
						);
				$result->execute($kunci);
				$hitung_data = $result->rowCount();

				if($hitung_data>0)
				{
					$row = $result->fetch(PDO::FETCH_OBJ);
					$jumlah = $row->Total;
				}
				else
				{
					$jumlah = 0;
				}
				
				$entropy=(($jumlah/$TotalData)*-1)*(LOG(($jumlah/$TotalData),2));

				if(is_nan($entropy))
					{
						$entropy=0;
					}

				$TotalEntropy_Root+=floatval($entropy);
			}
			// echo json_encode($TotalEntropy_Root);

			//Menetukan Variabel Yang Tidak Boleh DiProses
			$not_variabel_allow = array($nm_variabel_root, $variabel_none[2]);

			//Merubah Varibel Nama Field Yang Sudah Menjadi Root Dan Tidak Terdapat None Di Set Nilai 0
			for($i=0;$i<count($nilai_variabel);$i++)
			{
				for($j=0;$j<count($not_variabel_allow);$j++)
				{
					if($nilai_variabel[$i][0] == $not_variabel_allow[$j])
					{
						$nilai_variabel[$i][0] = 0;
					}
				}
			}

			unset($not_variabel_allow);
			// echo json_encode($nilai_variabel);
// -------------------------------------------------

		//Menghitung Total Entropy
		$variabel_entropy[]= array();
		$no_urut=0;
		//-Looping Nama Field
		for($j=0;$j<count($nilai_variabel);$j++)
		{
			//Kondisi HBA1C Tidak Boleh Diproses
			if($nilai_variabel[$j][0] != $nilai_variabel[12][0] || 
				$nilai_variabel[$j][0] != 0)
			{
				//Looping Berdasakran Jumlah Jenis Data Pada Tiap Field
				for($k=0;$k<count($nilai_variabel[$j])-1;$k++)
				{
					$totalentropy_variabel_other = 0;
					settype($totalentropy_variabel_other, 'float');

					$variabel_entropy[$no_urut][0] = $nilai_variabel[$j][0];
					$variabel_entropy[$no_urut][1] = $nilai_variabel[$j][$k+1];

					//Looping Berdasarkan Jumlah Jenis Dari HBA1C
					for($i=0; $i<count($nilai_variabel[12])-1; $i++)
					{
						$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						$result = $conn->prepare('
							SELECT COUNT(id_hsl_k) AS Total_Jns_Var FROM tbl_hsl_kmean WHERE
							'.$nilai_variabel[$j][0].' = :nilai 
							AND '.$variabel_none[2].' = :vl
							');
						$kunci = array(
										':nilai' => $nilai_variabel[$j][$k+1],
										':vl' => $variabel_none[3]
								);
						$result->execute($kunci);
						$hitung_data = $result->rowCount();

						if($hitung_data>0)
						{
							$row = $result->fetch(PDO::FETCH_OBJ);
							$total_jns_var = $row->Total_Jns_Var;
						}
						else
						{
							$total_jns_var = 0;
						}


						$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						$result = $conn->prepare('
							SELECT COUNT(id_hsl_k) AS Total FROM tbl_hsl_kmean  WHERE 
							'.$nilai_variabel[12][0].' = :nilai AND 
							'.$nilai_variabel[$j][0].' = :nilai2 AND
							'.$variabel_none[2].' = :vl
							');
						$kunci = array(
										':nilai' => $nilai_variabel[12][$i+1],
										':nilai2' => $nilai_variabel[$j][$k+1],
										':vl' => $variabel_none[3]
								);
						$result->execute($kunci);

						$hitung_data = $result->rowCount();

						if($hitung_data>0)
						{
							$row = $result->fetch(PDO::FETCH_OBJ);
							$jumlah = $row->Total;
						}
						else
						{
							$jumlah = 0;
						}


						$entropy=(($jumlah/$total_jns_var)*-1)*(LOG(($jumlah/$total_jns_var),2));

						if(is_nan($entropy))
						{
							$entropy=0;
						}

						$totalentropy_variabel_other+=floatval($entropy);

						$variabel_entropy[$no_urut][2] = $total_jns_var;
						$variabel_entropy[$no_urut][$i+6] = $jumlah;
					}
					$variabel_entropy[$no_urut][3] = $totalentropy_variabel_other;
					$variabel_entropy[$no_urut][4] = (floatval($total_jns_var)/floatval($TotalData))*floatval($totalentropy_variabel_other);
					$no_urut++;
				}
			}
		}
		// echo json_encode($variabel_entropy);

		//Menghitung Total Gain Variabel Lain
		$variabel_gain[]= array();
		$cek_max_gain=array();
		//-Looping Nama Field
		for($i=0;$i<count($nilai_variabel);$i++)
		{
			if($nilai_variabel[$i][0] != $nilai_variabel[12][0] || 
				$nilai_variabel[$i][0] != 0)
			{
				$variabel_gain[$i][1] = 0;
				//Looping Pencarian Entropy Tiap NamaField
				for($j=0;$j<count($variabel_entropy);$j++)
				{
					if(strval($nilai_variabel[$i][0]) == strval($variabel_entropy[$j][0]))
					{
						$variabel_gain[$i][0] = $variabel_entropy[$j][0];
						$variabel_gain[$i][1] += $variabel_entropy[$j][4];
					}
				}
				$variabel_gain[$i][2] = floatval($TotalEntropy_Root) - floatval($variabel_gain[$i][1]);
				array_push($cek_max_gain,$variabel_gain[$i][2]);
			}
		}
		// echo json_encode($cek_max_gain);

		//Memasukkan Nilai Gain Pada Array $variabel_entropy
		for($i=0;$i<count($nilai_variabel);$i++)
		{
			//Looping Pencarian Entropy Tiap NamaField
			for($j=0;$j<count($variabel_entropy);$j++)
			{
				if(strval($nilai_variabel[$i][0]) == strval($variabel_entropy[$j][0]))
				{
					$variabel_entropy[$j][5] = $variabel_gain[$i][2];
				}
			}
		}
		// echo json_encode($variabel_entropy);

		//Mencari Nilai Giant Tertinggi
		$gaint_akhir[]=array();
		$no_gaint=0;
		for($j=0;$j<count($variabel_entropy);$j++)
			{
				if(strval($variabel_entropy[$j][5]) == strval(max($cek_max_gain)))
				{
					for($k=0;$k<count($variabel_entropy[$j]);$k++)
					{
						$gaint_akhir[$no_gaint][$k] = $variabel_entropy[$j][$k];
					}
					$no_gaint++;
				}
			}
			// echo json_encode($gaint_akhir);

			//Mengambil Nilai ID Terakhir
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$result = $conn->prepare('SELECT id_hc45
										FROM tbl_hsl_c45
										ORDER BY id_hc45 DESC LIMIT 1
										');
			$result->execute();
			$count = $result->rowCount();
			
			if($count>0)
			{
				$row = $result->fetch(PDO::FETCH_OBJ);
				$no=intval($row->id_hc45)+1;	
			}
			else
			{
				$no=1;
			}

		//Input Hasil C4.5s
		for($j=0;$j<count($gaint_akhir);$j++)
			{
				$prediksi=0;
				$angka=0;
				for($k=0;$k<count($gaint_akhir[$j])-6;$k++)
				{
					$angka++;
					if(intval($gaint_akhir[$j][$k+6])>0)
					{
						$prediksi+=$angka;
					}
				}
				if($prediksi == 1)
				{
					$hsl_prediksi=">7";
				}
				else if($prediksi == 2)
				{
					$hsl_prediksi=">8";
				}
				else if($prediksi == 3)
				{
					$hsl_prediksi="Norm";
				}
				else
				{
					$hsl_prediksi="None";
				}

				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$pdo = $conn->prepare('INSERT INTO tbl_hsl_c45 (
											id_hc45,
											no_hc45,
											nmvariabel_hc45,
											value_hc45,
											entropy_hc45,
											gain_hc45,
											perkiraan_hc45
											) 
											VALUES 
											(
											:id, :no, :nm, :vl, :entropy, :gain, :prdks
										)');
				$insertdata = array(
										':id' => $no, 
										':no' => $j, 
										':nm' => $gaint_akhir[$j][0], 
										':vl' => $gaint_akhir[$j][1], 
										':entropy' => $gaint_akhir[$j][3],
										':gain' => $gaint_akhir[$j][5],
										':prdks' => $hsl_prediksi
									);
				$pdo->execute($insertdata);
			}

		//Update Nilai None
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('UPDATE tbl_hsl_c45
												set
												perkiraan_hc45 = :prk
												WHERE id_hc45 = :id AND
												no_hc45 = :noc45
								');
			$updatedata = array(
									':prk' => $no,
									':id' => $variabel_none[0], 
									':noc45' => $variabel_none[1]
								);
			$pdo->execute($updatedata);

// -------------------------------------------------
		unset($variabel_none);
		}
		// Cek Kata None Masih Ada Atau Tidak
			$kata_none = "None";
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$result = $conn->prepare('
									SELECT COUNT(id_hc45) AS TOTAL_NONE FROM tbl_hsl_c45 
									WHERE perkiraan_hc45 = :nm
									');
			$cek = array(':nm' => $kata_none);
			$result->execute($cek);

			$row= $result->fetch(PDO::FETCH_OBJ);
			$count_end_looping = $row->TOTAL_NONE;
			// $count_end_looping = $result->rowCount();
			// echo json_encode($count_end_looping);
			$tes++;
			// unset($variabel_done);
			
}while($tes < 20);
// }while(intval($count_end_looping) != 0);


	function searchDistinct($i, $variabel, $field)
	{
		include '../../koneksi/koneksi.php';
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo = $conn->prepare('SELECT DISTINCT '.$field.' 
			FROM tbl_hsl_kmean ORDER BY '.$field.' ASC');

		$pdo->execute();
		$GLOBALS['nilai_variabel'][$i][0] = $variabel;
		$a=0; 
		while($row= $pdo->fetch(PDO::FETCH_OBJ))
		{
			$a++;
			$GLOBALS['nilai_variabel'][$i][$a]= $row->$field;
		}
	}

?>