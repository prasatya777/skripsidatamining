<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	//Hitung Total Data Pada Tabel Hasil K-Means
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$result = $conn->prepare('SELECT COUNT(id_hsl_k) AS TotalD FROM tbl_hsl_kmean');
	$result->execute();
	$row = $result->fetch(PDO::FETCH_OBJ);
	$GLOBALS['TotalData'] = $row->TotalD;

	//Mengambil Nama Field Database
	$GLOBALS['namafield']=array();

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$result = $conn->prepare('SELECT * FROM tbl_hsl_kmean LIMIT 0');
	$result->execute();
	$count_column = $result->columnCount();

	for ($i = 1; $i < $count_column ; $i++) 
	{
	    $col = $result->getColumnMeta($i);
	    array_push($GLOBALS['namafield'], $col['name']);
	}

	//No Leaf Tree - 1
	$GLOBALS['no_akar']=1;

	//Proses Distinct
	$GLOBALS['nilai_variabel'][]= array();
	for($i=0; $i < ($count_column-1);$i++)
	{
		searchDistinct($i, $namafield[$i], $namafield[$i]);
	}

	//Menyimpan Value Dari HBA1C yang menjadi hasil perkiraan
	$GLOBALS['nilai_variabel_hba1c']=array();
	for($i=0; $i < count($nilai_variabel[12]); $i++)
	{
		array_push($GLOBALS['nilai_variabel_hba1c'], $nilai_variabel[12][$i]);
	}

	//Menghitung Total Entropy Root
	$syntax_sql = 'SELECT COUNT(id_hsl_k) AS Total FROM tbl_hsl_kmean  
					WHERE '.$nilai_variabel[12][0].' = :nilai';

	$GLOBALS['TotalEntropy_Root'] = 0;
	settype($TotalEntropy_Root, 'float');
	for($i=0; $i < count($nilai_variabel[12])-1; $i++)
		{
			$GLOBALS['kunci'] = array(':nilai' => $nilai_variabel[12][$i+1]);
			entropyRoot($syntax_sql);
		}

	//Menghitung Total Entropy Awal
	$GLOBALS['variabel_entropy'][]= array();
	$syntax_sql='';
	$no_urut=0;
	proseshitungEntropy($no_urut, $syntax_sql);

	//Menghitung Total Gain Variabel Lain
	totalGain();

	//Memasukkan Nilai Gain Pada Array $variabel_entropy
	gaintoEntropy();

	//Mencari Nilai Giant Tertinggi
	prosesgianTertinggi();

	//Mengambil Nilai ID Terakhir
	nilaiidTerakhir();

	//Input Hasil C4.5
	inputhasilC45();

	$GLOBALS['nm_variabel_root'] = $gaint_akhir[0][0];

// ----------------------------------------------------------------------------
//No Leaf Tree - 2
$GLOBALS['no_akar']=2;
$GLOBALS['search_no_akar']=1;
$GLOBALS['variabel_none'][0]=0;
do{

	//Cek Apakah Masih Terdapat NONE atau Tidak Pada perhitungan sebelumnya
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$result = $conn->prepare('SELECT id_hc45, no_hc45, nmvariabel_hc45, 
									value_hc45, perkiraan_hc45, noleaftree_hc45
									FROM tbl_hsl_c45
									WHERE noleaftree_hc45 = :id 
									AND perkiraan_hc45 = :prdks
									');
		$cek = array(':id' => $GLOBALS['search_no_akar'], ':prdks' => 'None');
		$result->execute($cek);
		$cek_none = $result->rowCount();
		
		if($cek_none>0)
		{
			unset($GLOBALS['variabel_none']);
			$row = $result->fetch(PDO::FETCH_OBJ);
			$GLOBALS['variabel_none'] = array(
									$row->id_hc45, $row->no_hc45, $row->nmvariabel_hc45
									, $row->value_hc45, $row->perkiraan_hc45, 
									$row->noleaftree_hc45
			);		
		}

		if($GLOBALS['variabel_none'][0] > 0)
		{
			unset($GLOBALS['nilai_variabel']);unset($GLOBALS['variabel_entropy']);
			unset($GLOBALS['variabel_gain']);unset($GLOBALS['cek_max_gain']);
			unset($GLOBALS['gaint_akhir']);unset($GLOBALS['TotalEntropy_Root']);

			//Proses Distinct
			$GLOBALS['nilai_variabel'][]= array();
			for($i=0; $i < ($count_column-1);$i++)
			{
				$syntax_sql = 'SELECT DISTINCT '.$namafield[$i].' 
								FROM tbl_hsl_kmean WHERE '.$variabel_none[2].' = :vl
								ORDER BY '.$namafield[$i].' ASC';
				$GLOBALS['cek'] = array(':vl' => $variabel_none[3]);
				prosesleafDistinct($i, $syntax_sql);
			}

			//Menghitung Total Entropy Root
			$syntax_sql = 'SELECT COUNT(id_hsl_k) AS Total FROM tbl_hsl_kmean  
							WHERE '.$nilai_variabel[12][0].' = :nilai
							AND '.$variabel_none[2].' = :nilai2';

			$GLOBALS['TotalEntropy_Root'] = 0;
			settype($TotalEntropy_Root, 'float');
			for($i=0; $i < count($nilai_variabel[12])-1; $i++)
			{
				$GLOBALS['kunci'] = array(':nilai' => $nilai_variabel[12][$i+1],
								':nilai2' => $variabel_none[3]);
				entropyRoot($syntax_sql);
			}

			//Menentukan Variabel Yang Tidak Dizinkan Diproses
			notallowVariabel();
// -------------------------------------------------
			//Menghitung Total Entropy
			$GLOBALS['variabel_entropy'][]= array();
			$syntax_sql='AND '.$variabel_none[2].' = '.$variabel_none[3].'';
			$no_urut=0;
			proseshitungEntropy($no_urut, $syntax_sql);

			//Menghitung Total Gain Variabel Lain
			totalGain();

			//Memasukkan Nilai Gain Pada Array $variabel_entropy
			gaintoEntropy();

			//Mencari Nilai Giant Tertinggi
			prosesgianTertinggi();

			//Mengambil Nilai ID Terakhir
			nilaiidTerakhir();

			//Input Hasil C4.5
			inputhasilC45();

			//Update Nilai None
				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$pdo = $conn->prepare('UPDATE tbl_hsl_c45
													set
													perkiraan_hc45 = :prk
													WHERE 
													noleaftree_hc45 = :noakar AND
													id_hc45 = :id AND
													no_hc45 = :noc45
									');
				$updatedata = array(
										':prk' => $GLOBALS['no'],
										':noakar' => $variabel_none[5],
										':id' => $variabel_none[0], 
										':noc45' => $variabel_none[1]
									);
				$pdo->execute($updatedata);

// -------------------------------------------------
			unset($GLOBALS['variabel_none']);
		}
		// Cek Kata None Masih Ada Atau Tidak
			$kata_none = "None";
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$result = $conn->prepare('
									SELECT COUNT(id_hc45) AS TOTAL_NONE FROM tbl_hsl_c45 
									WHERE perkiraan_hc45 = :nm AND noleaftree_hc45 = :id 
									');
			$cek = array(':nm' => $kata_none, ':id'  => $GLOBALS['search_no_akar']);
			$result->execute($cek);

			$row= $result->fetch(PDO::FETCH_OBJ);
			$count_end_looping = $row->TOTAL_NONE;

}while(intval($count_end_looping) != 0);


// -----------------------------Testing Function
function entropyRoot($syntax_sql)
{
	include '../../koneksi/koneksi.php';
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$result = $conn->prepare(''.$syntax_sql.'');
	$result->execute($GLOBALS['kunci']);
	$hitung_data = $result->rowCount();

	if($hitung_data>0)
	{
		$row = $result->fetch(PDO::FETCH_OBJ);
		$jumlah = $row->Total;
	}
	else
	{
		$jumlah = 0;
	}
				
	$entropy=(($jumlah/$GLOBALS['TotalData'])*-1)*(LOG(($jumlah/$GLOBALS['TotalData']),2));

	if(is_nan($entropy))
		{
			$entropy=0;
		}

	$GLOBALS['TotalEntropy_Root']+=floatval($entropy);
}

function prosesleafDistinct($i, $syntax_sql)
{
	//Proses Distinct
	include '../../koneksi/koneksi.php';
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare(''.$syntax_sql.'');
	$pdo->execute($GLOBALS['cek']);
		if($i != 12)
		{
			$GLOBALS['nilai_variabel'][$i][0] = $GLOBALS['namafield'][$i];
			$a=0; 
			$field = $GLOBALS['namafield'][$i];
			while($row= $pdo->fetch(PDO::FETCH_OBJ))
			{
				$a++;
				$GLOBALS['nilai_variabel'][$i][$a]= $row->$field;
			}	
		}
		else if($i == 12)
		{
			for($a=0;$a<count($GLOBALS['nilai_variabel_hba1c']);$a++)
			{
				$GLOBALS['nilai_variabel'][12][$a] = 
													$GLOBALS['nilai_variabel_hba1c'][$a];
			}
		}
}

function notallowVariabel()
{
	//Menetukan Variabel Yang Tidak Boleh DiProses
	$not_variabel_allow = array($GLOBALS['nm_variabel_root'], 
								$GLOBALS['variabel_none'][2]);

	//Merubah Varibel Nama Field Yang Sudah Menjadi Root Dan Tidak Terdapat None Di Set Nilai 0
	for($i=0;$i<count($GLOBALS['nilai_variabel']);$i++)
	{
		for($j=0;$j<count($not_variabel_allow);$j++)
		{
			if($GLOBALS['nilai_variabel'][$i][0] == $not_variabel_allow[$j])
			{
				$GLOBALS['nilai_variabel'][$i][0] = 0;
			}
		}
	}
	unset($not_variabel_allow);
}

function proseshitungEntropy($no_urut, $syntax_sql)
{
	include '../../koneksi/koneksi.php';
//Menghitung Total Entropy
	//-Looping Nama Field
	for($j=0;$j<count($GLOBALS['nilai_variabel']);$j++)
	{
		//Kondisi HBA1C Tidak Boleh Diproses
		if($GLOBALS['nilai_variabel'][$j][0] != $GLOBALS['nilai_variabel'][12][0] || 
			$GLOBALS['nilai_variabel'][$j][0] != 0)
		{
			//Looping Berdasakran Jumlah Jenis Data Pada Tiap Field
			for($k=0;$k<count($GLOBALS['nilai_variabel'][$j])-1;$k++)
			{
				$totalentropy_variabel_other = 0;
				settype($totalentropy_variabel_other, 'float');

				$GLOBALS['variabel_entropy'][$no_urut][0] = 
														$GLOBALS['nilai_variabel'][$j][0];
				$GLOBALS['variabel_entropy'][$no_urut][1] = 
													$GLOBALS['nilai_variabel'][$j][$k+1];

				//Looping Berdasarkan Jumlah Jenis Dari HBA1C
				for($i=0; $i<count($GLOBALS['nilai_variabel'][12])-1; $i++)
				{
					$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$result = $conn->prepare('
						SELECT COUNT(id_hsl_k) AS Total_Jns_Var FROM tbl_hsl_kmean WHERE
						'.$GLOBALS['nilai_variabel'][$j][0].' = :nilai '.$syntax_sql.'
						');
					$kunci = array(':nilai' => $GLOBALS['nilai_variabel'][$j][$k+1]);
					$result->execute($kunci);
					$hitung_data = $result->rowCount();

					if($hitung_data>0)
					{
						$row = $result->fetch(PDO::FETCH_OBJ);
						$total_jns_var = $row->Total_Jns_Var;
					}
					else
					{
						$total_jns_var = 0;
					}

					$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$result = $conn->prepare('
						SELECT COUNT(id_hsl_k) AS Total FROM tbl_hsl_kmean  WHERE 
						'.$GLOBALS['nilai_variabel'][12][0].' = :nilai AND 
						'.$GLOBALS['nilai_variabel'][$j][0].' = :nilai2
						'.$syntax_sql.'
						');
					$kunci = array(
									':nilai' => $GLOBALS['nilai_variabel'][12][$i+1],
									':nilai2' => $GLOBALS['nilai_variabel'][$j][$k+1]
							);
					$result->execute($kunci);

					$hitung_data = $result->rowCount();
					if($hitung_data>0)
					{
						$row = $result->fetch(PDO::FETCH_OBJ);
						$jumlah = $row->Total;
					}
					else
					{
						$jumlah = 0;
					}

					$entropy=(($jumlah/$total_jns_var)*-1)*(LOG(($jumlah/$total_jns_var),2));

					if(is_nan($entropy))
					{
						$entropy=0;
					}

					$totalentropy_variabel_other+=floatval($entropy);

					$GLOBALS['variabel_entropy'][$no_urut][2] = $total_jns_var;
					$GLOBALS['variabel_entropy'][$no_urut][$i+6] = $jumlah;
				}
				$GLOBALS['variabel_entropy'][$no_urut][3] = $totalentropy_variabel_other;
				$GLOBALS['variabel_entropy'][$no_urut][4] = (floatval($total_jns_var)/floatval($GLOBALS['TotalData']))*floatval($totalentropy_variabel_other);
				$no_urut++;
			}
		}
	}
}

function totalGain()
{
	//Menghitung Total Gain Variabel Lain
	$GLOBALS['variabel_gain'][]= array();
	$GLOBALS['cek_max_gain']=array();
	//-Looping Nama Field
	for($i=0;$i<count($GLOBALS['nilai_variabel']);$i++)
	{
		if($GLOBALS['nilai_variabel'][$i][0] != $GLOBALS['nilai_variabel'][12][0] || 
			$GLOBALS['nilai_variabel'][$i][0] != 0)
		{
			$GLOBALS['variabel_gain'][$i][1] = 0;
			//Looping Pencarian Entropy Tiap NamaField
			for($j=0;$j<count($GLOBALS['variabel_entropy']);$j++)
			{
				if(strval($GLOBALS['nilai_variabel'][$i][0]) == 
											strval($GLOBALS['variabel_entropy'][$j][0]))
				{
					$GLOBALS['variabel_gain'][$i][0] = $GLOBALS['variabel_entropy'][$j][0];
					$GLOBALS['variabel_gain'][$i][1] += 
													$GLOBALS['variabel_entropy'][$j][4];
				}
			}
			$GLOBALS['variabel_gain'][$i][2] = floatval($GLOBALS['TotalEntropy_Root']) - 
									floatval($GLOBALS['variabel_gain'][$i][1]);
			array_push($GLOBALS['cek_max_gain'],$GLOBALS['variabel_gain'][$i][2]);
		}
	}
}

function gaintoEntropy()
{
	//Memasukkan Nilai Gain Pada Array $variabel_entropy
	for($i=0;$i<count($GLOBALS['nilai_variabel']);$i++)
	{
		//Looping Pencarian Entropy Tiap NamaField
		for($j=0;$j<count($GLOBALS['variabel_entropy']);$j++)
		{
			if(strval($GLOBALS['nilai_variabel'][$i][0]) == 
											strval($GLOBALS['variabel_entropy'][$j][0]))
			{
				$GLOBALS['variabel_entropy'][$j][5] = $GLOBALS['variabel_gain'][$i][2];
			}
		}
	}
}

function prosesgianTertinggi()
{
	//Mencari Nilai Giant Tertinggi
	$GLOBALS['gaint_akhir'][]=array();
	$no_gaint=0;
	for($j=0;$j<count($GLOBALS['variabel_entropy']);$j++)
		{
			if(strval($GLOBALS['variabel_entropy'][$j][5]) == 
													strval(max($GLOBALS['cek_max_gain'])))
			{
				for($k=0;$k<count($GLOBALS['variabel_entropy'][$j]);$k++)
				{
					$GLOBALS['gaint_akhir'][$no_gaint][$k] 
													= $GLOBALS['variabel_entropy'][$j][$k];
				}
				$no_gaint++;
			}
		}
}

function nilaiidTerakhir()
{
	include '../../koneksi/koneksi.php';
	//Mengambil Nilai ID Terakhir
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$result = $conn->prepare('SELECT id_hc45
							FROM tbl_hsl_c45
							ORDER BY id_hc45 DESC LIMIT 1
							');
	$result->execute();
	$count = $result->rowCount();
			
	if($count>0)
	{
		$row = $result->fetch(PDO::FETCH_OBJ);
		$GLOBALS['no']=intval($row->id_hc45)+1;	
	}
	else
	{
		$GLOBALS['no']=1;
	}
}

function inputhasilC45()
{
	include '../../koneksi/koneksi.php';
	//Input Hasil C4.5s
	for($j=0;$j<count($GLOBALS['gaint_akhir']);$j++)
		{
			$prediksi=0;
			$angka=0;
			$cektunggal=0;
			for($k=0;$k<count($GLOBALS['gaint_akhir'][$j])-6;$k++)
			{
				$angka++;
				if(intval($GLOBALS['gaint_akhir'][$j][$k+6])>0)
				{
					$prediksi+=$angka;
					$cektunggal+=1;
				}
			}
			if($prediksi == 1 && $cektunggal == 1)
			{
				$hsl_prediksi=">7";
			}
			else if($prediksi == 2 && $cektunggal == 1)
			{
				$hsl_prediksi=">8";
			}
			else if($prediksi == 3 && $cektunggal == 1)
			{
				$hsl_prediksi="Norm";
			}
			else
			{
				$hsl_prediksi="None";
			}

			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('INSERT INTO tbl_hsl_c45 (
									id_hc45,
									no_hc45,
									nmvariabel_hc45,
									value_hc45,
									entropy_hc45,
									gain_hc45,
									perkiraan_hc45,
									noleaftree_hc45
									) 
									VALUES 
									(
									:id, :no, :nm, :vl, :entropy, :gain, :prdks, :noakar
								)');
			$insertdata = array(
									':id' => $GLOBALS['no'], 
									':no' => $j, 
									':nm' => $GLOBALS['gaint_akhir'][$j][0], 
									':vl' => $GLOBALS['gaint_akhir'][$j][1], 
									':entropy' => $GLOBALS['gaint_akhir'][$j][3],
									':gain' => $GLOBALS['gaint_akhir'][$j][5],
									':prdks' => $hsl_prediksi,
									':noakar' => $GLOBALS['no_akar']
								);
			$pdo->execute($insertdata);
		}
}
// -----------------------------

	function searchDistinct($i, $variabel, $field)
	{
		include '../../koneksi/koneksi.php';
		$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo = $conn->prepare('SELECT DISTINCT '.$field.' 
			FROM tbl_hsl_kmean ORDER BY '.$field.' ASC');

		$pdo->execute();
		$GLOBALS['nilai_variabel'][$i][0] = $variabel;
		$a=0; 
		while($row= $pdo->fetch(PDO::FETCH_OBJ))
		{
			$a++;
			$GLOBALS['nilai_variabel'][$i][$a]= $row->$field;
		}
	}

?>