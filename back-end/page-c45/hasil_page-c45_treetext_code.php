<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';

	//Bagian Root
	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT * FROM tbl_hsl_c45 WHERE noleaftree_hc45 = :noleaf');
	$cek = array(':noleaf' => '1');
	$pdo->execute($cek);

	$no=0;
	$tree="";
	while($row= $pdo->fetch(PDO::FETCH_OBJ))
	{	
		$nm = str_replace("_hsl_k","",$row->nmvariabel_hc45);
		if(
			strval($row->perkiraan_hc45)!="Norm" &&
			strval($row->perkiraan_hc45)!=">8" &&
			strval($row->perkiraan_hc45)!=">7" &&
			strval($row->perkiraan_hc45)!="None"
		  )
		{
			$hsl = "?";
		}
		else
		{
			$hsl = $row->perkiraan_hc45;
		}

		$tree.=''.$nm.' = '.$row->value_hc45.' --> 
				['.$hsl.'] </br>';
		//Bagian Akar 1 ------------------------------------------------------------->
		if(
			strval($row->perkiraan_hc45)!="Norm" &&
			strval($row->perkiraan_hc45)!=">8" &&
			strval($row->perkiraan_hc45)!=">7" &&
			strval($row->perkiraan_hc45)!="None"
		  )
		{
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo1 = $conn->prepare('SELECT * FROM tbl_hsl_c45 
									WHERE noleaftree_hc45 = :noleaf
									AND id_hc45 = :nilaiakar1');
			$cek1 = array(':noleaf' => '2', ':nilaiakar1' => $row->perkiraan_hc45);
			$pdo1->execute($cek1);
			while($row1= $pdo1->fetch(PDO::FETCH_OBJ))
			{
				$nm = str_replace("_hsl_k","",$row1->nmvariabel_hc45);
				if(
					strval($row1->perkiraan_hc45)!="Norm" &&
					strval($row1->perkiraan_hc45)!=">8" &&
					strval($row1->perkiraan_hc45)!=">7" &&
					strval($row1->perkiraan_hc45)!="None"
				  )
				{
					$hsl = "?";
				}
				else
				{
					$hsl = $row1->perkiraan_hc45;
				}
				$tree.='|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						'.$nm.' = '.$row1->value_hc45.' --> 
						['.$hsl.'] </br>';
				//Bagian Akar 2 ----------------------------------------------------------->
				if(
					strval($row1->perkiraan_hc45)!="Norm" &&
					strval($row1->perkiraan_hc45)!=">8" &&
					strval($row1->perkiraan_hc45)!=">7" &&
					strval($row1->perkiraan_hc45)!="None"
				  )
				{
					$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$pdo2 = $conn->prepare('SELECT * FROM tbl_hsl_c45 
											WHERE noleaftree_hc45 = :noleaf
											AND id_hc45 = :nilaiakar1');
					$cek2 = array(':noleaf' => '3', ':nilaiakar1' => $row1->perkiraan_hc45);
					$pdo2->execute($cek2);
					while($row2= $pdo2->fetch(PDO::FETCH_OBJ))
					{
						$nm = str_replace("_hsl_k","",$row2->nmvariabel_hc45);
						if(
							strval($row2->perkiraan_hc45)!="Norm" &&
							strval($row2->perkiraan_hc45)!=">8" &&
							strval($row2->perkiraan_hc45)!=">7" &&
							strval($row2->perkiraan_hc45)!="None"
						  )
						{
							$hsl = "?";
						}
						else
						{
							$hsl = $row2->perkiraan_hc45;
						}

						$tree.='|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								'.$nm.' = '.$row2->value_hc45.' --> 
								['.$hsl.'] </br>';
						//Bagian Akar 3 --------------------------------------------------->
						if(
							strval($row2->perkiraan_hc45)!="Norm" &&
							strval($row2->perkiraan_hc45)!=">8" &&
							strval($row2->perkiraan_hc45)!=">7" &&
							strval($row2->perkiraan_hc45)!="None"
						  )
						{
							$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
							$pdo3 = $conn->prepare('SELECT * FROM tbl_hsl_c45 
													WHERE noleaftree_hc45 = :noleaf
													AND id_hc45 = :nilaiakar1');
							$cek3 = array(':noleaf' => '4', ':nilaiakar1' => $row2->perkiraan_hc45);
							$pdo3->execute($cek3);
							while($row3= $pdo3->fetch(PDO::FETCH_OBJ))
							{
								$nm = str_replace("_hsl_k","",$row3->nmvariabel_hc45);
								if(
									strval($row3->perkiraan_hc45)!="Norm" &&
									strval($row3->perkiraan_hc45)!=">8" &&
									strval($row3->perkiraan_hc45)!=">7" &&
									strval($row3->perkiraan_hc45)!="None"
								  )
								{
									$hsl = "?";
								}
								else
								{
									$hsl = $row3->perkiraan_hc45;
								}
								$tree.='|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										'.$nm.' = '.$row3->value_hc45.' --> 
										['.$hsl.'] </br>';
							}	
						}
					}	
				}
			}	
		}
	}

	echo($tree);
?>