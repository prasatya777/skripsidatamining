<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';

	$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo = $conn->prepare('SELECT * FROM tbl_hsl_c45');
	$pdo->execute();

	$no=0;
	while($row= $pdo->fetch(PDO::FETCH_OBJ))
	{	
		$no++;
		$nm = str_replace("_hsl_k","",$row->nmvariabel_hc45);
		$data[] = array(
			'no'	 					=> $no,
			'id'	 					=> $row->id_hc45,
			'noleave'	 				=> $row->noleaftree_hc45,
			'nmvariabel' 				=> $nm,
			'value' 					=> $row->value_hc45,
			'entropy'					=> $row->entropy_hc45,
			'gain'						=> $row->gain_hc45,
			'perkiraan'					=> $row->perkiraan_hc45
		);
	}

	echo json_encode($data);
?>