<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';

	extract($_POST);

	$nm = $_POST['txt_nama'];
	$em	= $_POST['txt_email'];
	$psw	= $_POST['txt_password'];

	try {
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('INSERT INTO tbl_admin_login ( 
									email_al, nama_al, password_al
									) 
									VALUES 
									(
									:mail, :nam, :pwd
									)');
			$insertdata = array(
								':mail' => $em, ':nam' => $nm, ':pwd' => $psw
								);
			$pdo->execute($insertdata);

		} catch (PDOexception $e) {
		die();
		}
?>