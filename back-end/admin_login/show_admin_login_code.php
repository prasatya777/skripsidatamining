<?php
  include '../../koneksi/koneksi.php';
  if (session_status() == PHP_SESSION_NONE) 
  {
    session_start();
    ob_start();
  }

  include '../../page-admin/authentication/authenc_code.php';

  $conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo = $conn->prepare('SELECT * FROM tbl_admin_login ORDER BY email_al ASC');
  $pdo->execute();

  $no = 0;
  while($row= $pdo->fetch(PDO::FETCH_OBJ))
  {
    $no++;
    $data[] = array(
                    'no'     => $no,
                    'email'  => $row->email_al,
                    'nama'   => $row->nama_al,
              );
  }
  
  echo json_encode($data);
?>

