<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';
	
	extract($_POST);

	$nm 	= $_POST['txt_nama'];
	$em		= $_POST['txt_email'];
	$psw	= $_POST['txt_password'];
	$bfr_em = $_POST['bfr_email'];
	try {
			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('UPDATE tbl_admin_login
											set
											email_al =:mail,
											nama_al =:nam,
											password_al =:pwd
											WHERE email_al =:bfrmail');
			$updatedata = array(
								':mail' => $em,
								':nam' => $nm, 
								':pwd' => $psw,
								':bfrmail' => $bfr_em
								);
			$pdo->execute($updatedata);

		} catch (PDOexception $e) {
		die();
		}
?>