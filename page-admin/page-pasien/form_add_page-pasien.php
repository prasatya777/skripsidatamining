<?php
  include '../../page-admin/authentication/authenc_code.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

</head>

<body>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="txt_nama" class="control-label"></label>
                  <div class="form-label-group">
                    <input type="text" name="txt_nama" id="txt_nama" class="form-control" placeholder="Masukkan Nama Pasien" autofocus="autofocus">
                    <label for="txt_nama">Masukkan Nama Pasien</label>
                  </div>
              </div>
              <div class="col-md-6">
                <label for="txt_gender" class="control-label">Gender</label>
                  <div class="form-label-group">
                    <div class="">
                      <select name="txt_gender" autocomplete="off" required="required" id="txt_gender" class="form-control control-label" placeholder="Pilih Gender" style="width: 100%"> 
                      </select>
                    </div>
                  </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="txt_age" class="control-label">Age</label>
                  <div class="form-label-group">
                    <div class="">
                      <select name="txt_age" autocomplete="off" required="required" id="txt_age" class="form-control" placeholder="Pilih Rentang Usia" style="width: 100%">    
                      </select>
                    </div>
                  </div>
              </div>
              <div class="col-md-6">
                <label for="txt_admission_type_id" class="control-label">Admission Type</label>
                  <div class="form-label-group">
                    <div class="">
                      <select name="txt_admission_type_id" autocomplete="off" required="required" id="txt_admission_type_id" class="form-control" placeholder="Pilih Admission Type" style="width: 100%">         
                      </select>
                    </div>
                  </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="txt_discharge_disposition_id" class="control-label">Discharge Disposition</label>
                  <div class="form-label-group">
                    <div class="">
                      <select name="txt_discharge_disposition_id" autocomplete="off" required="required" id="txt_discharge_disposition_id" class="form-control" placeholder="Pilih Discharge Disposition" style="width: 100%">      
                      </select>
                    </div>
                  </div>
              </div>
              <div class="col-md-6">
                <label for="txt_admission_source_id" class="control-label">Admission Source</label>
                  <div class="form-label-group">
                    <div class="">
                      <select name="txt_admission_source_id" autocomplete="off" required="required" id="txt_admission_source_id" class="form-control" placeholder="Pilih Admission Source" style="width: 100%">          
                      </select>
                    </div>
                  </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="txt_time_in_hospital" class="control-label"></label>
                  <div class="form-label-group">
                    <input type="text" name="txt_time_in_hospital" id="txt_time_in_hospital" class="form-control" placeholder="Masukkan Lama Di Rumah Sakit" autofocus="autofocus">
                    <label for="txt_time_in_hospital">Masukkan Lama Di Rumah Sakit</label>
                  </div>
              </div>
              <div class="col-md-6">
                <label for="txt_num_lab_procedures" class="control-label"></label>
                  <div class="form-label-group">
                    <input type="text" name="txt_num_lab_procedures" id="txt_num_lab_procedures" class="form-control" placeholder="Masukkan Jumlah Prosedur Lab" autofocus="autofocus">
                    <label for="txt_num_lab_procedures">Masukkan Jumlah Prosedur Lab</label>
                  </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="txt_num_procedures" class="control-label"></label>
                  <div class="form-label-group">
                    <input type="text" name="txt_num_procedures" id="txt_num_procedures" class="form-control" placeholder="Masukkan Jumlah Prosedur" autofocus="autofocus">
                    <label for="txt_num_procedures">Masukkan Jumlah Prosedur</label>
                  </div>
              </div>
              <div class="col-md-6">
                <label for="txt_diag_1" class="control-label">Diagnosis 1</label>
                  <div class="form-label-group">
                    <div class="">
                      <select name="txt_diag_1" autocomplete="off" required="required" id="txt_diag_1" class="form-control" placeholder="Pilih Diagnosis 1" style="width: 100%">       
                      </select>
                    </div>
                  </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="txt_diag_2" class="control-label">Diagnosis 2</label>
                  <div class="form-label-group">
                    <div class="">
                      <select name="txt_diag_2" autocomplete="off" required="required" id="txt_diag_2" class="form-control" placeholder="Pilih Diagnosis 2" style="width: 100%">          
                      </select>
                    </div>
                  </div>
              </div>
              <div class="col-md-6">
                <label for="txt_diag_3" class="control-label">Diagnosis 3</label>
                  <div class="form-label-group">
                    <div class="">
                      <select name="txt_diag_3" autocomplete="off" required="required" id="txt_diag_3" class="form-control" placeholder="Pilih Diagnosis 3" style="width: 100%">         
                      </select>
                    </div>
                  </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="txt_number_diagnoses" class="control-label"></label>
                  <div class="form-label-group">
                    <input type="text" name="txt_number_diagnoses" id="txt_number_diagnoses" class="form-control" placeholder="Masukkan Jumlah Diagnosis" autofocus="autofocus">
                    <label for="txt_number_diagnoses">Masukkan Jumlah Diagnosis</label>
                  </div>
              </div>
              <div class="col-md-6">
                <label for="txt_change" class="control-label">Change</label>
                  <div class="form-label-group">
                    <div class="">
                      <select name="txt_change" autocomplete="off" required="required" id="txt_change" class="form-control" placeholder="Pilih Perubahan Resep" style="width: 100%">      
                      </select>
                    </div>
                  </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="txt_diabetesMed" class="control-label">DiabetesMed</label>
                  <div class="form-label-group">
                    <div class="">
                      <select name="txt_diabetesMed" autocomplete="off" required="required" id="txt_diabetesMed" class="form-control" placeholder="Pilih Penggunaan Obat Sebelumnya" style="width: 100%">         
                      </select>
                    </div>
                  </div>
              </div>
              <div class="col-md-6">
                <label for="txt_readmitted" class="control-label">Readmitted</label>
                      <div class="form-label-group">
                        <div class="">
                          <select name="txt_readmitted" autocomplete="off" required="required" id="txt_readmitted" class="form-control" placeholder="Pilih Kategori Lama Pertemuan" style="width: 100%">           
                          </select>
                        </div>
                      </div>
              </div>
            </div>
          </div>
</body>

</html>
