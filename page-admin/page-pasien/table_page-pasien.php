<?php
  include '../../page-admin/authentication/authenc_code.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Garuda-Incoming Internal</title>

</head>

<body>
<!-- Bagian Tabel Data Pasien Baru -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Tabel Data Pasien Baru</div>
          <div class="card-body">

            <button type="button" class="btn btn-primary" data-toggle="modal" 
            onclick="modalInsert_Click()" style="margin-top: 5px; margin-bottom: 20px;">
            <i class='fa fa-plus-circle fa-lg fa-fw' aria-hidden='true'></i> Tambah Data Pasien
            </button>

            <div class='table-responsive'>
              <table class='table table-striped table-bordered' id='dataTable-NewPasien' width='170%' cellspacing='0'>
                <thead>
                  <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Gender</th>
                      <th width="20%">Age</th>
                      <th>Admission Type ID</th>
                      <th>Discharge Disposition ID</th>
                      <th>Admission Source ID</th>
                      <th>Time in Hospital</th>
                      <th>Num Lab Procedures</th>
                      <th>Num Procedures</th>
                      <th>Diag 1</th>
                      <th>Diag 2</th>
                      <th>Diag 3</th>
                      <th>Number Diagnoses</th>
                      <th>Change</th>
                      <th>Diabetes Med</th>
                      <th>Readmitted</th>
                      <th>Edit</th>
                      <th>Delete</th>
                  </tr>
                </thead>
              </table>
            </div>

            <button type="button" class="btn btn-primary" data-toggle="modal" onclick="deleteAllDataPasienClick()" style="margin-top: 15px; margin-bottom: 5px;">
            <i class='fa fa-trash fa-lg fa-fw' aria-hidden='true'></i>Delete Semua Data Pasien
            </button>

            <button type="button" class="btn btn-primary" data-toggle="modal" onclick="modalprediksi()" style="margin-top: 15px; margin-bottom: 5px;">
            <i class='fa fa-calculator fa-lg fa-fw' aria-hidden='true'></i>Start Prediksi
            </button>

          </div>

          <div class="card-footer small text-muted">Prediksi HbA1c-Andika Prasatya[201531220]&copy;2019</div>
        </div>

<!-- Bagian Import -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Import File</div>
          <div class="card-body">

            <form method="POST" id="ImportExcel">

              <div class="form-group">
                <div class="form-row">
                  <div class="col-md-6">
                    <div class="form-label-group">
                      <input type="file" name="ImportExcelFile" autocomplete="off" id="ImportExcelFile" class="form-control" placeholder="Masukkan File Excel">
                      <label for="ImportExcelFile">Masukkan File Excel</label>
                    </div>
                  </div>
                </div>
              </div>

            </form>

                <button type="button" class="btn btn-primary" data-toggle="modal" onclick="importClick()" >
                  <i class='fa fa-file-import fa-lg fa-fw' aria-hidden='true'></i>Import Excel
                </button>

          </div>
          <div class="card-footer small text-muted">Prediksi HbA1c-Andika Prasatya[201531220]&copy;2019</div>
        </div>

<!-- Bagian Tabel Data Hasil K-Means -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Tabel Hasil K-Means Data Pasien</div>
          <div class="card-body">

            <div class='table-responsive'>
              <table class='table table-striped table-bordered' id='dataTable-HasilPasien' width='170%' cellspacing='0'>
                <thead>
                  <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Gender</th>
                      <th width="20%">Age</th>
                      <th>Admission Type ID</th>
                      <th>Discharge Disposition ID</th>
                      <th>Admission Source ID</th>
                      <th>Time in Hospital</th>
                      <th>Num Lab Procedures</th>
                      <th>Num Procedures</th>
                      <th>Diag 1</th>
                      <th>Diag 2</th>
                      <th>Diag 3</th>
                      <th>Number Diagnoses</th>
                      <th>Change</th>
                      <th>Diabetes Med</th>
                      <th>Readmitted</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Prediksi HbA1c-Andika Prasatya[201531220]&copy;2019</div>
        </div>

<!-- Bagian Tabel Prediksi Data Pasien -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Tabel Hasil Prediksi Data Pasien</div>
          <div class="card-body">

            <div class='table-responsive'>
              <table class='table table-striped table-bordered' id='dataTable-HasilPrediksiPasien' width='100%' cellspacing='0'>
                <thead>
                  <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Gender</th>
                      <th>Age</th>
                      <th>Prediksi</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Prediksi HbA1c-Andika Prasatya[201531220]&copy;2019</div>
        </div>

  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
