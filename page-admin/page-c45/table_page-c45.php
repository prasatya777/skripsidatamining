<?php
  include '../../page-admin/authentication/authenc_code.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Garuda-Incoming Internal</title>

</head>

<body>
<!-- Bagian Tabel Data Hasil K-Means -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Tabel Hasil K-Means</div>
          <div class="card-body">

            <div class='table-responsive'>
              <table class='table table-striped table-bordered' id='dataTable-HasilKmean' width='170%' cellspacing='0'>
                <thead>
                  <tr>
                      <th>No</th>
                      <th>Gender</th>
                      <th width="20%">Age</th>
                      <th>Admission Type ID</th>
                      <th>Discharge Disposition ID</th>
                      <th>Admission Source ID</th>
                      <th>Time in Hospital</th>
                      <th>Num Lab Procedures</th>
                      <th>Num Procedures</th>
                      <th>Diag 1</th>
                      <th>Diag 2</th>
                      <th>Diag 3</th>
                      <th>Number Diagnoses</th>
                      <th>A1Cresult</th>
                      <th>Change</th>
                      <th>Diabetes Med</th>
                      <th>Readmitted</th>
                  </tr>
                </thead>
              </table>
            </div>

            <button type="button" class="btn btn-primary" data-toggle="modal" onclick="c45Click()" style="margin-top: 25px; margin-bottom: 5px;">
            <i class='fa fa-calculator fa-lg fa-fw' aria-hidden='true'></i>Proses Metode C4.5
            </button>

<!--             <button type="button" class="btn btn-primary" data-toggle="modal" onclick="deleteClick()" style="margin-top: 25px; margin-bottom: 5px;">
            <i class='fa fa-trash fa-lg fa-fw' aria-hidden='true'></i>Delete Isi Tabel Hasil C4.5
            </button> -->

          </div>
          <div class="card-footer small text-muted">Prediksi HbA1c-Andika Prasatya[201531220]&copy;2019</div>
        </div>

<!-- Bagian Tabel Data Hasil C45 -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Tabel Proses Hasil C4.5</div>
          <div class="card-body">

            <div class='table-responsive'>
              <table class='table table-striped table-bordered' id='dataTable-HasilC45' width='100%' cellspacing='0'>
                <thead>
                  <tr>
                      <th>No</th>
                      <th>ID-C.45</th>
                      <th>Urutan Tree</th>
                      <th>Nama Variabel</th>
                      <th>Nilai Variabel</th>
                      <th>Nilai Entropy</th>
                      <th>Nilai Gain</th>
                      <th>Hasil Prediksi</th>
                  </tr>
                </thead>
              </table>
            </div>

          </div>
          <div class="card-footer small text-muted">Prediksi HbA1c-Andika Prasatya[201531220]&copy;2019</div>
        </div>

        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Hasil Pohon C4.5 Dalam Bentuk Deskripsi</div>
          <div class="card-body">

            <div id='pohon-c45' style="max-height: 400px; overflow-y: scroll;">

            </div>

          </div>
          <div class="card-footer small text-muted">Prediksi HbA1c-Andika Prasatya[201531220]&copy;2019</div>
        </div>


  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
