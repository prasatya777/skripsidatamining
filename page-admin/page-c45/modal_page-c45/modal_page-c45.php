<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

</head>

<body>

<!-- Modal Proses C4.5-->
<div class="modal fade" id="modalImport" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Information</h5>
        <button id="close-import" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div id="modal-body-c45" class="modal-body"></div>

      <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <button id='btn-import-excel' name='btn-import-excel' type="button" class="btn btn-primary" data-dismiss="modal" onclick="c45Loading()">Proses</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Proses C4.5 Succes -->
<div class="modal fade" id="modalImportSucces" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Information</h5>
        <button id="close-import-succes" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body-c45-succes">
      </div>
      <div class="modal-footer">
        <button id='ok-c45' type="button" class="btn btn-primary" data-dismiss="modal" >OK</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Loading -->
<div class="modal fade" id="modalLoading" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button id="close-loading" type="button" class="close" data-dismiss="modal" aria-label="Close" style="display:none;">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div  class="modal-body">
        <div class="d-flex align-items-center">
          <strong id="modal-body-loading">Loading Progress Metode C4.5......</strong>
          <div class="spinner-border ml-auto" role="status" aria-hidden="true"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal Delete-->
<!-- <div class="modal fade" id="modalDeleteTruncate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Information</h5>
        <button id="close-deletetruncate" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div id="modal-body-deletetruncate" class="modal-body"></div>

      <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <button id='btn-deletetruncate' name='btn-deletetruncate' type="button" class="btn btn-primary" data-dismiss="modal" onclick="jv_modal_delete()">Hapus</button>
      </div>
    </div>
  </div>
</div> -->

<!-- Modal Delete Succes -->
<!-- <div class="modal fade" id="modalDeleteSucces" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Information</h5>
        <button id="close-delete-succes" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body-delete-succes">
      </div>
      <div class="modal-footer">
        <button id='ok-delete' type="button" class="btn btn-primary" data-dismiss="modal" >OK</button>
      </div>
    </div>
  </div>
</div> -->

  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>