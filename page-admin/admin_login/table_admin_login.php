<?php
  include '../../page-admin/authentication/authenc_code.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>AdminLogin&copy;PredikHbA1c-201531220</title>

</head>

<body>
<!-- Bagian Tabel User -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Tabel User</div>
          <div class="card-body">

            <button type="button" class="btn btn-primary" data-toggle="modal" 
            onclick="modalInsert_Click()" style="margin-top: 5px; margin-bottom: 20px;">
            <i class='fa fa-plus-circle fa-lg fa-fw' aria-hidden='true'></i> Tambah Data User
            </button>

            <div class='table-responsive'>
              <table class='table table-striped table-bordered' id='dataTable-User' width='100%' cellspacing='0'>
                <thead>
                  <tr>
                      <th width="10%">No</th>
                      <th>Email</th>
                      <th>Nama</th>
                      <th width="10%">Edit</th>
                      <th width="10%">Delete</th>
                  </tr>
                </thead>
              </table>
            </div>

            <button type="button" class="btn btn-primary" data-toggle="modal" onclick="deleteAllDataUserClick()" style="margin-top: 15px; margin-bottom: 5px;">
            <i class='fa fa-trash fa-lg fa-fw' aria-hidden='true'></i>Delete Semua User
            </button>

          </div>
          <div class="card-footer small text-muted">Prediksi HbA1c-Andika Prasatya[201531220]&copy;2019</div>
        </div>

  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
