<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

</head>

<body>
<!-- Modal Add-->
  <div class="modal fade" id="modalinsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Tambah Data Pasien</h5>
          <button id="close-add" type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <?php include 'form_add_page-pasien.php'; ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <button id='submit-add_pasien' name='submit-add_pasien' type="button" class="btn btn-primary" onclick="add_data_pasien()">Tambah</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Insert Succes-->
  <div class="modal fade" id="modalInsertSucces" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Insert Information</h5>
          <button id="close-insert-succes" type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Insert Data Berhasil
        </div>
        <div class="modal-footer">
          <button id='ok-page-pasien' type="button" class="btn btn-primary" data-dismiss="modal" onclick="jv_modal_succes()">OK</button>
        </div>
      </div>
    </div>
  </div>

<!-- Modal Edit-->
  <div class="modal fade" id="modaledit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Edit Data Pasien</h5>
          <button id="close-edit" type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <?php include 'form_edit_page-pasien.php'; ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <button id='submit-edit_pasien' name='submit-edit_pasien' type="button" class="btn btn-primary" onclick="edit_data_pasien()">Update</button>
        </div>
      </div>
    </div>
  </div>

<!-- Modal Edit Succes-->
<div class="modal fade" id="modaleditsucces" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Information</h5>
        <button id="close-edit-succes" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Update Data Pasien Berhasil
      </div>
      <div class="modal-footer">
        <button id='edit-succes-page-pasien' type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Delete Data Pasien-->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete Information</h5>
        <button id="close-delete" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div id="modal-body-delete" class="modal-body"></div>

      <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <button id='delete-admin-login' name='delete-admin-login' type="button" class="btn btn-primary" data-dismiss="modal" onclick="jv_delete_pasien()">Hapus</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Loading -->
<div class="modal fade" id="modalLoading" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button id="close-loading" type="button" class="close" data-dismiss="modal" aria-label="Close" style="display:none;">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div  class="modal-body">
        <div class="d-flex align-items-center">
          <strong id="modal-body-loading">Loading Progress Prediksi......</strong>
          <div class="spinner-border ml-auto" role="status" aria-hidden="true"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal Delete-->
<div class="modal fade" id="modalDeleteTruncate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete Information</h5>
        <button id="close-deletetruncate" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div id="modal-body-deletetruncate" class="modal-body"></div>

      <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <button id='btn-deletetruncate' name='btn-deletetruncate' type="button" class="btn btn-primary" data-dismiss="modal" onclick="jv_modal_delete()">Hapus</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Delete Succes -->
<div class="modal fade" id="modalDeleteSucces" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Information</h5>
        <button id="close-delete-succes" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body-delete-succes">
      </div>
      <div class="modal-footer">
        <button id='ok-delete' type="button" class="btn btn-primary" data-dismiss="modal" >OK</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Prediksi-->
<div class="modal fade" id="modalprediksi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Information</h5>
        <button id="close-prediksi" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div id="modal-body-prediksi" class="modal-body"></div>

      <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
          <button id='btn-prediksi' name='btn-deletetruncate' type="button" class="btn btn-primary" data-dismiss="modal" onclick="prediksiClick()">Ya</button>
      </div>
    </div>
  </div>
</div>

  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>