<?php
  include '../../page-admin/authentication/authenc_code.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

<!--   <title>Dashboard&copy;PredikHbA1c-201531220</title> -->

</head>

<body>
<!-- Bagian Bar Chart -->
  <div class="row">
    <div class="col-lg-6">
      <div class="card mb-3">
        <div class="card-header">
          <i class="fas fa-chart-bar"></i>
          Bar Chart Data Mentah
        </div>
          <div class="card-body">
            <canvas id="myBarChart-datamentah" width="100%" height="100"></canvas>
          </div>
            <div class="card-footer small text-muted">Prediksi HbA1c-Andika Prasatya[201531220]&copy;2019</div>
      </div>
    </div>
      <div class="col-lg-6">
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-chart-bar"></i>
            Bar Chart Prediksi Pasien
          </div>
            <div class="card-body">
              <canvas id="myBarChart-dataprediksipasien" width="100%" height="100"></canvas>
            </div>
              <div class="card-footer small text-muted">Prediksi HbA1c-Andika Prasatya[201531220]&copy;2019</div>
        </div>
      </div>
  </div>

<!-- Bagian Tabel Akurasi -->
    <div class="row">
      <div class="col-lg-6">
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Tabel Akurasi</div>
          <div class="card-body">
            <div class='table-responsive'>
              <table class='table table-striped table-bordered' id='dataTable-Akurasi' width='100%' cellspacing='0'>
                <thead>
                  <tr>
                      <th>Prediksi Benar</th>
                      <th>Prediksi Salah</th>
                      <th>Total Prediksi</th>
                      <th>Akurasi</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>

          <div class="card-footer small text-muted">Prediksi HbA1c-Andika Prasatya[201531220]&copy;2019</div>
        </div>
      </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
