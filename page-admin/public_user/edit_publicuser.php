<?php
  include '../../page-admin/authentication/authenc_code.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

</head>

<body>

      <form id="form_admin_login" method="POST" enctype="multipart/form-data" >
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="email" name="txt_email_login" id="txt_email_login" class="form-control" placeholder="Email Address" required="required">
                  <label for="txt_email_login">Email Address</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_nama_login" id="txt_nama_login" class="form-control" placeholder="Nama Lengkap" required="required">
                  <label for="txt_nama_login">Nama Lengkap</label>
                </div>
              </div>
            </div>
          </div>
          
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="password" name="txt_password_login" id="txt_password_login" class="form-control" placeholder="Password" required="required">
                  <label for="txt_password_login">Password</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="password" name="txt_conf_password_login" id="txt_conf_password_login" class="form-control" placeholder="Confirm Password" required="required">
                  <label for="txt_conf_password_login">Confirm Password</label>
                </div>
              </div>
            </div>
          </div>
      </form>
        
</body>

</html>
