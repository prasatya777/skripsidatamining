var iddelete;
var bfr_email_unv;

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function modalInsert_Click()
{
	$('#modalinsert').modal('show');
	$('#txt_email').focus();
}

function addAdminLogin(data){
	var txt_nama = $('#txt_nama').val();
	var txt_email = $('#txt_email').val();
	var txt_password = $('#txt_password').val();
	var txt_conf_password = $('#txt_conf_password').val();
	var hslcekdouble = data;

	if(txt_email =="")
	{
		swal("Information", "Email Tidak Boleh Kosong", "warning");
		$('#txt_email').focus();
	}
	else if (!validateEmail(txt_email))
	{
		swal("Information", "Format Email Tidak Sesuai", "warning");
		$('#txt_email').val(null);
		$('#txt_email').focus();
	}
	else if(txt_nama == "")
	{
		swal("Information", "Nama Lengkap Tidak Boleh Kosong", "warning");
		$('#txt_nama').focus();
	} 
	else if(txt_password == "")
	{
		swal("Information", "Password Tidak Boleh Kosong", "warning");
		$('#txt_password').focus();
	}
	else if(txt_conf_password == "")
	{
		swal("Information", "Confirm Password Tidak Boleh Kosong", "warning");
		$('#txt_conf_password').focus();
	}
	else if(txt_password != txt_conf_password)
	{
		swal("Information", "Password Tidak Sesuai", "warning");
		$('#txt_password').val(null);
		$('#txt_conf_password').val(null);
		$('#txt_password').focus();
	}
	else if(hslcekdouble == "1")
	{
		swal("Information", "Email Sudah Terdaftar. Silahkan Gunakan Email Yang Berbeda ", "warning");
		$('#txt_email').val(null);
		$('#txt_email').focus();
	}
	else
	{
		$.ajax({
			url:"../../back-end/admin_login/add_admin_login_code.php",
			type:'POST',
			data: { 
				txt_email : txt_email,
				txt_nama : txt_nama,
				txt_password : txt_password
			},

			success:function(data,status){
				showAdminLogin();
				$('#modalinsert').modal('hide');
	            $('#close-add').trigger('click');
	            resetAdminLogin();
	            $('#modalInsertSucces').modal('show');
			}

		});
	}

	hasilcekdouble = "";
}

function cekdoubleemail(pilihan)
{
	if(pilihan=="tambah")
	{
		var id = $('#txt_email').val();
		$.post("../../back-end/admin_login/cek_double_email_admin_login_code.php", {id:id}, 
			function(data,status)
				{
					addAdminLogin(data);
				}
		);
	}
	if(pilihan=="edit")
	{
		var id = $('#txt_email_update').val();
		if(id==bfr_email_unv)
		{
			var data = "0";
			editAdminLogin(data);
		}
		else
		{
			$.post("../../back-end/admin_login/cek_double_email_admin_login_code.php", {id:id}, 
				function(data,status)
					{
						editAdminLogin(data);
					}
			);
		}
	}
}

function showAdminLogin()
{
    $.ajax({
        url:"../../back-end/admin_login/show_admin_login_code.php",
        dataType: 'json',
        contentType:false,
        processData:false,
        success:function(data_json)
        {
          var table = $('#dataTable-User').DataTable();
          table.clear();
          table.destroy();
          $('#dataTable-User').DataTable({
                "scrollY": "250",
                "scrollX": true,
                data:data_json,
                    columns:[
                              { data: 'no' },
                              { data: 'email' },
                              { data: 'nama' },
                              { data: 'email',
                                render: function ( data, type, row ) 
                                {
                                  return '<div class="editmain button_admin_edit"> <a onclick="tampileditAdminLogin(\''+data+'\')"> <i class="fa fa-edit fa-lg fa-fw" aria-hidden="true"></i></a> </div>';
                                }
                              },
                              { data: 'email',
                                render: function ( data, type, row ) 
                                {
                                  return '<div class="deletemain button_admin_delete"> <a onclick="popupdeleteAdminLogin(\''+data+'\')"> <i class="fa fa-trash fa-lg fa-fw" aria-hidden="true"></i></a> </div>';
                                }
                              } 
                    ]
                });
        }
    });
}

function resetAdminLogin()
{
	$('#txt_nama').val(null);
	$('#txt_email').val(null);
	$('#txt_password').val(null);
	$('#txt_conf_password').val(null);
	$('#txt_email').focus();
}

function popupdeleteAdminLogin(id)
{
	$("#modalDelete").modal('show');
	iddelete = id;
	var isipesan = "Apakah Anda Yakin Menghapus User Dengan Email '"+id+"' ?";

	$("#modal-body-delete").html(isipesan);

}

function jv_modal_succes()
{
	setTimeout(function(){ $('#modalinsert').modal('show'); }, 500);
}

function jv_modal_delete()
{
	var id = iddelete;
			$.ajax({
			url:"../../back-end/admin_login/delete_admin_login_code.php",
			type:"POST",
			data:{ id:id },
			success:function(data,status){
				var table = $('#dataTable-User').DataTable();
          		table.clear();
          		table.destroy();
		   		showAdminLogin();
		   		var hasil = "Hapus Data User Berhasil !";
                $('#modal-body-delete-succes').html(hasil);
		   		$('#modalDeleteSucces').modal('show');
			}
		});
	iddelete = "";
}

// Bagian Edit-----------------------------------------------------------------------
function submit_form_edit()
{
	swal({
	  title: "Edit Information",
	  text: "Apakah Anda Yakin Update Data User Dengan Email : '"+bfr_email_unv+"' ?",
	  icon: "warning",
	  buttons: true,
	  dangerMode: true,
	})
	.then((willDelete) => {
	  if (willDelete) 
	  {
	  	var pilihan = "edit";
	  	cekdoubleemail(pilihan);
	  } 
	});

}


function tampileditAdminLogin(id)
{
	$.post("../../back-end/admin_login/view_update_admin_login_code.php", {id:id}, 
		function(data,status)
			{
				var view_update_al = JSON.parse(data);
				$('#txt_email_update').val(view_update_al.email_al);
				$('#txt_nama_update').val(view_update_al.nama_al);
	});

	$('#modaledit').modal('show');
	bfr_email_unv = id;
}

function editAdminLogin(data)
{
	var txt_email = $('#txt_email_update').val();
	var txt_nama = $('#txt_nama_update').val();
	var txt_password = $('#txt_password_update').val();
	var txt_conf_password = $('#txt_conf_password_update').val();
	var hslcekdouble = data;

	if(txt_email =="")
	{
		swal("Information", "Email Tidak Boleh Kosong", "warning");
		$('#txt_email_update').focus();
	}
	else if (!validateEmail(txt_email))
	{
		swal("Information", "Format Email Tidak Sesuai", "warning");
		$('#txt_email_update').val(null);
		$('#txt_email_update').focus();
	}
	else if(txt_nama == "")
	{
		swal("Information", "Nama Lengkap Tidak Boleh Kosong", "warning");
		$('#txt_nama_update').focus();
	} 
	else if(txt_password == "")
	{
		swal("Information", "Password Tidak Boleh Kosong", "warning");
		$('#txt_password_update').focus();
	}
	else if(txt_conf_password == "")
	{
		swal("Information", "Confirm Password Tidak Boleh Kosong", "warning");
		$('#txt_conf_password_update').focus();
	}
	else if(txt_password != txt_conf_password)
	{
		swal("Information", "Password Tidak Sesuai", "warning");
		$('#txt_password_update').val(null);
		$('#txt_conf_password_update').val(null);
		$('#txt_password_update').focus();
	}
	else if(hslcekdouble == "1")
	{
		swal("Information", "Email Sudah Terdaftar. Silahkan Gunakan Email Yang Berbeda ", "warning");
		$('#txt_email_update').val(null);
		$('#txt_email_update').focus();
	}
	else
	{
		$.ajax({
			url:"../../back-end/admin_login/update_admin_login_code.php",
			type:'POST',
			data: { 
				txt_email : txt_email,
				txt_nama : txt_nama,
				txt_password : txt_password,
				bfr_email : bfr_email_unv
			},
			success:function(data,status){
				showAdminLogin();
				$('#modaledit').modal('hide');
	            $('#close-edit').trigger('click');
	            resetupdate_AdminLogin();
	            $('#modalEditSucces').modal('show');
			}
		});
		bfr_email_unv = "";
	}
}


function resetupdate_AdminLogin()
{
	$('#txt_email_update').val(null);
	$('#txt_password_update').val(null);
	$('#txt_conf_password_update').val(null);
	$('#txt_email_update').focus();
}

function deleteAllDataUserClick()
{
    $('#modalDeleteTruncate').modal('show');
    var isipesan = "Apakah Anda Yakin Menghapus Semua Data User ?";
    $("#modal-body-deletetruncate").html(isipesan);
}

function jv_modal_all_delete()
{ 
  $.ajax({
          url:"../../back-end/admin_login/delete_all_admin_login_code.php",
          success:function(data)
          {
            if(data == "")
              {
                var table = $('#dataTable-User').DataTable();
                table.clear();
                table.destroy();

                $('#modalDeleteSucces').modal('show');
                var hasil = "Hapus Semua Data User Berhasil !";
                $('#modal-body-delete-succes').html(hasil);

              }
              else
              {
                $('#modalDeleteSucces').modal('show');

                var hasil = "Hapus Semua Data Pasien Gagal !";
                $('#modal-body-delete-success').html(hasil);
              }
          }
  });
}