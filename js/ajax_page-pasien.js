var iddelete;
var idupdate;
var namaupdate;

function importClick()
{
  var file = $('#ImportExcelFile').val();
  if(file != "")
  {
    var app_extension = file.slice((file.lastIndexOf(".") - 1 >>> 0) + 2);

    if(app_extension != "xlsx")
    {
      swal("Error", "Jenis Ekstensi Yang Dapat Digunakan Adalah Microsoft Excel (.xlsx) !", "error");
    }

    else
    {
      $('#modalImport').modal('show');
      var isipesan = "Apakah Anda Yakin Import File "+file+" ?";

      $("#modal-body-import").html(isipesan);
    }

  }
}

function jv_modal_import_excel()
{
  $('#ImportExcel').submit();
  $('#modalLoading').modal({
    keyboard: false,
    backdrop: 'static'
  });
    
  $('#modalLoading').modal('show');
}

function modalInsert_Click()
{
  // var tes= "Male"
  // var $option = $('<option>Male</option>').val(tes);
  // $('#txt_gender').append($option).trigger('change');

  $('#modalinsert').modal('show');
  // $('#txt_gender').focus();
}

function add_data_pasien()
{
  var txt_nama = $('#txt_nama').val();
  var txt_gender = $('#txt_gender').val();
  var txt_age = $('#txt_age').val();
  var txt_admission_type_id = $('#txt_admission_type_id').val();
  var txt_discharge_disposition_id = $('#txt_discharge_disposition_id').val();
  var txt_admission_source_id = $('#txt_admission_source_id').val();
  var txt_time_in_hospital = $('#txt_time_in_hospital').val();
  var txt_num_lab_procedures = $('#txt_num_lab_procedures').val();
  var txt_num_procedures = $('#txt_num_procedures').val();
  var txt_diag_1 = $('#txt_diag_1').val();
  var txt_diag_2 = $('#txt_diag_2').val();
  var txt_diag_3 = $('#txt_diag_3').val();
  var txt_number_diagnoses = $('#txt_number_diagnoses').val();
  var txt_change = $('#txt_change').val();
  var txt_diabetesMed = $('#txt_diabetesMed').val();
  var txt_readmitted = $('#txt_readmitted').val();

  if(txt_nama == "")
  {
    swal("Information", "Nama Pasien Tidak Boleh Kosong", "warning");
    $('#txt_nama').focus();
  }
  else if(txt_gender == null)
  {
    swal("Information", "Gender Tidak Boleh Kosong", "warning");
    $('#txt_gender').focus();
  }
  else if(txt_age == null)
  {
    swal("Information", "Age Tidak Boleh Kosong", "warning");
    $('#txt_age').focus();
  } 
  else if(txt_admission_type_id == null)
  {
    swal("Information", "Admission Type Tidak Boleh Kosong", "warning");
    $('#txt_admission_type_id').focus();
  }
  else if(txt_discharge_disposition_id == null)
  {
    swal("Information", "Discharge Disposition Tidak Boleh Kosong", "warning");
    $('#txt_discharge_disposition_id').focus();
  } 
  else if(txt_admission_source_id == null)
  {
    swal("Information", "Admission Source Tidak Boleh Kosong", "warning");
    $('#txt_admission_source_id').focus();
  }
  else if(txt_time_in_hospital == "")
  {
    swal("Information", "Time In Hospital Tidak Boleh Kosong", "warning");
    $('#txt_time_in_hospital').focus();
  }
  else if(txt_num_lab_procedures == "")
  {
    swal("Information", "Num Lab Procedures Tidak Boleh Kosong", "warning");
    $('#txt_num_lab_procedures').focus();
  }
  else if(txt_num_procedures == "")
  {
    swal("Information", "Num Procedures Tidak Boleh Kosong", "warning");
    $('#txt_num_procedures').focus();
  }
  else if(txt_diag_1 == null)
  {
    swal("Information", "Diagnosis 1 Tidak Boleh Kosong", "warning");
    $('#txt_diag_1').focus();
  }
  else if(txt_diag_2 == null)
  {
    swal("Information", "Diagnosis 2 Tidak Boleh Kosong", "warning");
    $('#txt_diag_2').focus();
  }
  else if(txt_diag_3 == null)
  {
    swal("Information", "Diagnosis 3 Tidak Boleh Kosong", "warning");
    $('#txt_diag_3').focus();
  }
  else if(txt_number_diagnoses == "")
  {
    swal("Information", "Number Diagnoses Tidak Boleh Kosong", "warning");
    $('#txt_number_diagnoses').focus();
  }
  else if(txt_change == null)
  {
    swal("Information", "Change Tidak Boleh Kosong", "warning");
    $('#txt_change').focus();
  }
  else if(txt_diabetesMed == null)
  {
    swal("Information", "DiabetesMed Tidak Boleh Kosong", "warning");
    $('#txt_diabetesMed').focus();
  }
  else if(txt_readmitted == null)
  {
    swal("Information", "Readmitted Tidak Boleh Kosong", "warning");
    $('#txt_readmitted').focus();
  }
  else
  {
    $.ajax({
      url:"../../back-end/page-pasien/add_page-pasien_code.php",
      type:'POST',
      data: { 
        txt_nama : txt_nama,
        txt_gender : txt_gender,
        txt_age : txt_age,
        txt_admission_type_id : txt_admission_type_id,
        txt_discharge_disposition_id : txt_discharge_disposition_id,
        txt_admission_source_id : txt_admission_source_id,
        txt_time_in_hospital : txt_time_in_hospital,
        txt_num_lab_procedures : txt_num_lab_procedures,
        txt_num_procedures : txt_num_procedures,
        txt_diag_1 : txt_diag_1,
        txt_diag_2 : txt_diag_2,
        txt_diag_3 : txt_diag_3,
        txt_number_diagnoses : txt_number_diagnoses,
        txt_change : txt_change,
        txt_diabetesMed : txt_diabetesMed,
        txt_readmitted : txt_readmitted
      },
      success:function(data,status){
        showtabelNewPasien();
        $('#modalinsert').modal('hide');
        $('#close-add').trigger('click');
        resetPagePasien();
        $('#modalInsertSucces').modal('show');
      }

    });
  }               
}

function jv_modal_succes()
{
  setTimeout(function(){ $('#modalinsert').modal('show'); }, 500);
}

function tampileditPagePasien(id)
{
  $.post("../../back-end/page-pasien/view_page-pasien_code.php", {id:id}, 
    function(data)
      {
        var view_pp = JSON.parse(data);
        var $option = "";

        idupdate = id;
        namaupdate = view_pp.nama_dp;

        $('#txt_nama-edit').val(view_pp.nama_dp);

        $('#txt_gender-edit').val(view_pp.gender_dp);
        $('#txt_gender-edit').select2({
          dropdownParent: $('#txt_gender-edit').parent(),
          placeholder: "---Pilih Gender---",
          allowClear: true
        }).trigger('change');

        $('#txt_age-edit').val(view_pp.age_dp);
        $('#txt_age-edit').select2({
          dropdownParent: $('#txt_age-edit').parent(),
          placeholder: "---Pilih Rentang Usia---",
          allowClear: true
        }).trigger('change');

        $('#txt_admission_type_id-edit').val(view_pp.admission_type_id_dp);
        $('#txt_admission_type_id-edit').select2({
          dropdownParent: $('#txt_admission_type_id-edit').parent(),
          placeholder: "---Pilih Admission Type---",
          allowClear: true
        }).trigger('change');

        $('#txt_discharge_disposition_id-edit').val(view_pp.discharge_disposition_id_dp);
        $('#txt_discharge_disposition_id-edit').select2({
          dropdownParent: $('#txt_discharge_disposition_id-edit').parent(),
          placeholder: "---Pilih Discharge Disposition---",
          allowClear: true
        }).trigger('change');

        $('#txt_admission_source_id-edit').val(view_pp.admission_source_id_dp);
        $('#txt_admission_source_id-edit').select2({
          dropdownParent: $('#txt_admission_source_id-edit').parent(),
          placeholder: "---Pilih Admission Source---",
          allowClear: true
        }).trigger('change');


        $('#txt_time_in_hospital-edit').val(view_pp.time_in_hospital_dp);
        $('#txt_num_lab_procedures-edit').val(view_pp.num_lab_procedures_dp);
        $('#txt_num_procedures-edit').val(view_pp.num_procedures_dp);


        $('#txt_diag_1-edit').val(view_pp.diag_1_dp);
        $('#txt_diag_1-edit').select2({
          dropdownParent: $('#txt_diag_1-edit').parent(),
          placeholder: "---Pilih Diagnosis 1---",
          allowClear: true
        }).trigger('change');


        $('#txt_diag_2-edit').val(view_pp.diag_2_dp);
        $('#txt_diag_2-edit').select2({
          dropdownParent: $('#txt_diag_2-edit').parent(),
          placeholder: "---Pilih Diagnosis 2---",
          allowClear: true
        }).trigger('change');

        $('#txt_diag_3-edit').val(view_pp.diag_3_dp);
        $('#txt_diag_3-edit').select2({
          dropdownParent: $('#txt_diag_3-edit').parent(),
          placeholder: "---Pilih Diagnosis 3---",
          allowClear: true
        }).trigger('change');


        $('#txt_number_diagnoses-edit').val(view_pp.number_diagnoses_dp);


        $('#txt_change-edit').val(view_pp.change_dp);
        $('#txt_change-edit').select2({
          dropdownParent: $('#txt_change-edit').parent(),
          placeholder: "---Pilih Perubahan Resep---",
          allowClear: true
        }).trigger('change');

        $('#txt_diabetesMed-edit').val(view_pp.diabetesMed_dp);
        $('#txt_diabetesMed-edit').select2({
          dropdownParent: $('#txt_diabetesMed-edit').parent(),
          placeholder: "---Pilih Penggunaan Obat Sebelumnya---",
          allowClear: true
        }).trigger('change');

        $('#txt_readmitted-edit').val(view_pp.readmitted_dp);
        $('#txt_readmitted-edit').select2({
          dropdownParent: $('#txt_readmitted-edit').parent(),
          placeholder: "---Pilih Kategori Lama Pertemuan---",
          allowClear: true
        }).trigger('change');
        
      });
  $('#modaledit').modal('show');
}

function edit_data_pasien()
{
  var txt_nama = $('#txt_nama-edit').val();
  var txt_gender = $('#txt_gender-edit').val();
  var txt_age = $('#txt_age-edit').val();
  var txt_admission_type_id = $('#txt_admission_type_id-edit').val();
  var txt_discharge_disposition_id = $('#txt_discharge_disposition_id-edit').val();
  var txt_admission_source_id = $('#txt_admission_source_id-edit').val();
  var txt_time_in_hospital = $('#txt_time_in_hospital-edit').val();
  var txt_num_lab_procedures = $('#txt_num_lab_procedures-edit').val();
  var txt_num_procedures = $('#txt_num_procedures-edit').val();
  var txt_diag_1 = $('#txt_diag_1-edit').val();
  var txt_diag_2 = $('#txt_diag_2-edit').val();
  var txt_diag_3 = $('#txt_diag_3-edit').val();
  var txt_number_diagnoses = $('#txt_number_diagnoses-edit').val();
  var txt_change = $('#txt_change-edit').val();
  var txt_diabetesMed = $('#txt_diabetesMed-edit').val();
  var txt_readmitted = $('#txt_readmitted-edit').val();

  if(txt_nama == "")
  {
    swal("Information", "Nama Pasien Tidak Boleh Kosong", "warning");
    $('#txt_nama-edit').focus();
  }
  else if(txt_gender == null)
  {
    swal("Information", "Gender Tidak Boleh Kosong", "warning");
    $('#txt_gender-edit').focus();
  }
  else if(txt_age == null)
  {
    swal("Information", "Age Tidak Boleh Kosong", "warning");
    $('#txt_age-edit').focus();
  } 
  else if(txt_admission_type_id == null)
  {
    swal("Information", "Admission Type Tidak Boleh Kosong", "warning");
    $('#txt_admission_type_id-edit').focus();
  }
  else if(txt_discharge_disposition_id == null)
  {
    swal("Information", "Discharge Disposition Tidak Boleh Kosong", "warning");
    $('#txt_discharge_disposition_id-edit').focus();
  } 
  else if(txt_admission_source_id == null)
  {
    swal("Information", "Admission Source Tidak Boleh Kosong", "warning");
    $('#txt_admission_source_id-edit').focus();
  }
  else if(txt_time_in_hospital == "")
  {
    swal("Information", "Time In Hospital Tidak Boleh Kosong", "warning");
    $('#txt_time_in_hospital-edit').focus();
  }
  else if(txt_num_lab_procedures == "")
  {
    swal("Information", "Num Lab Procedures Tidak Boleh Kosong", "warning");
    $('#txt_num_lab_procedures-edit').focus();
  }
  else if(txt_num_procedures == "")
  {
    swal("Information", "Num Procedures Tidak Boleh Kosong", "warning");
    $('#txt_num_procedures-edit').focus();
  }
  else if(txt_diag_1 == null)
  {
    swal("Information", "Diagnosis 1 Tidak Boleh Kosong", "warning");
    $('#txt_diag_1-edit').focus();
  }
  else if(txt_diag_2 == null)
  {
    swal("Information", "Diagnosis 2 Tidak Boleh Kosong", "warning");
    $('#txt_diag_2-edit').focus();
  }
  else if(txt_diag_3 == null)
  {
    swal("Information", "Diagnosis 3 Tidak Boleh Kosong", "warning");
    $('#txt_diag_3-edit').focus();
  }
  else if(txt_number_diagnoses == "")
  {
    swal("Information", "Number Diagnoses Tidak Boleh Kosong", "warning");
    $('#txt_number_diagnoses-edit').focus();
  }
  else if(txt_change == null)
  {
    swal("Information", "Change Tidak Boleh Kosong", "warning");
    $('#txt_change-edit').focus();
  }
  else if(txt_diabetesMed == null)
  {
    swal("Information", "DiabetesMed Tidak Boleh Kosong", "warning");
    $('#txt_diabetesMed-edit').focus();
  }
  else if(txt_readmitted == null)
  {
    swal("Information", "Readmitted Tidak Boleh Kosong", "warning");
    $('#txt_readmitted-edit').focus();
  }
  else
  {
    swal({
      title: "Edit Information",
      text: "Apakah Anda Yakin Update Data Pasien Dengan Nama : '"+namaupdate+"' Dan No ID '"+idupdate+"' ?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) 
      {
        $.ajax({
          url:"../../back-end/page-pasien/update_page-pasien_code.php",
          type:'POST',
          data: { 
                  id : idupdate,
                  txt_nama : txt_nama,
                  txt_gender : txt_gender,
                  txt_age : txt_age,
                  txt_admission_type_id : txt_admission_type_id,
                  txt_discharge_disposition_id : txt_discharge_disposition_id,
                  txt_admission_source_id : txt_admission_source_id,
                  txt_time_in_hospital : txt_time_in_hospital,
                  txt_num_lab_procedures : txt_num_lab_procedures,
                  txt_num_procedures : txt_num_procedures,
                  txt_diag_1 : txt_diag_1,
                  txt_diag_2 : txt_diag_2,
                  txt_diag_3 : txt_diag_3,
                  txt_number_diagnoses : txt_number_diagnoses,
                  txt_change : txt_change,
                  txt_diabetesMed : txt_diabetesMed,
                  txt_readmitted : txt_readmitted
          },
          success:function(data,status){
            showtabelNewPasien();
            $('#modaledit').modal('hide');
            $('#close-edit').trigger('click');
            resetPagePasien();
            $('#modaleditsucces').modal('show');
          }
        });
      } 
    });
  }
}

function showtabelNewPasien()
{
    $.ajax({
        url:"../../back-end/page-pasien/data_pasien_page-pasien_code.php",
        dataType: 'json',
        contentType:false,
        processData:false,
        success:function(data_json)
        {
          var table = $('#dataTable-NewPasien').DataTable();
          table.clear();
          table.destroy();
          $('#dataTable-NewPasien').DataTable({
                "scrollY": "250",
                "scrollX": true,
                data:data_json,
                    columns:[
                              { data: 'no' },
                              { data: 'nama' },
                              { data: 'gender' },
                              { data: 'age' },
                              { data: 'admission_type_id' },
                              { data: 'discharge_disposition_id' },
                              { data: 'admission_source_id' },
                              { data: 'time_in_hospital' },
                              { data: 'num_lab_procedures' },
                              { data: 'num_procedures' },
                              { data: 'diag_1' },
                              { data: 'diag_2' },
                              { data: 'diag_3' },
                              { data: 'number_diagnoses' },
                              { data: 'change' },
                              { data: 'diabetesMed' },
                              { data: 'readmitted' },
                              { data: 'id',
                                render: function ( data, type, row ) 
                                {
                                  return '<div class="editmain button_admin_edit"> <a onclick="tampileditPagePasien('+data+')"> <i class="fa fa-edit fa-lg fa-fw" aria-hidden="true"></i></a> </div>';
                                }
                              },
                              { data: 'info',
                                render: function ( data, type, row ) 
                                {
                                  return '<div class="deletemain button_admin_delete"> <a onclick="popupdeletePagePasien(\''+data+'\')"> <i class="fa fa-trash fa-lg fa-fw" aria-hidden="true"></i></a> </div>';
                                }
                              } 
                    ]
                });
        }
    });
}

function resetPagePasien()
{
  $('#txt_gender').val(null).trigger('change');
  $('#txt_age').val(null).trigger('change');
  $('#txt_admission_type_id').val(null).trigger('change');
  $('#txt_discharge_disposition_id').val(null).trigger('change');
  $('#txt_admission_source_id').val(null).trigger('change');
  $('#txt_diag_1').val(null).trigger('change');
  $('#txt_diag_2').val(null).trigger('change');
  $('#txt_diag_3').val(null).trigger('change');
  $('#txt_change').val(null).trigger('change');
  $('#txt_diabetesMed').val(null).trigger('change');
  $('#txt_readmitted').val(null).trigger('change');

  $('#txt_nama').val(null);
  $('#txt_time_in_hospital').val(null);
  $('#txt_num_lab_procedures').val(null);
  $('#txt_num_procedures').val(null);
  $('#txt_number_diagnoses').val(null);

  $('#txt_gender-edit').val(null).trigger('change');
  $('#txt_age-edit').val(null).trigger('change');
  $('#txt_admission_type_id-edit').val(null).trigger('change');
  $('#txt_discharge_disposition_id-edit').val(null).trigger('change');
  $('#txt_admission_source_id-edit').val(null).trigger('change');
  $('#txt_diag_1-edit').val(null).trigger('change');
  $('#txt_diag_2-edit').val(null).trigger('change');
  $('#txt_diag_3-edit').val(null).trigger('change');
  $('#txt_change-edit').val(null).trigger('change');
  $('#txt_diabetesMed-edit').val(null).trigger('change');
  $('#txt_readmitted-edit').val(null).trigger('change');

  $('#txt_nama-edit').val(null);
  $('#txt_time_in_hospital-edit').val(null);
  $('#txt_num_lab_procedures-edit').val(null);
  $('#txt_num_procedures-edit').val(null);
  $('#txt_number_diagnoses-edit').val(null);
}

function popupdeletePagePasien(id)
{
  var nilai = id;
  var pisah = nilai.split('-');
  iddelete = pisah[0];
  $("#modalDelete").modal('show');
  var isipesan = "Apakah Anda Yakin Menghapus Data Pasien '"+pisah[1]+"' Dengan ID '"+pisah[0]+"' ?";
  $("#modal-body-delete").html(isipesan);
}

function jv_delete_pasien()
{
  var id = iddelete;
    $.ajax({
    url:"../../back-end/page-pasien/delete_page-pasien_code.php",
    type:"POST",
    data:{ id:id },
    success:function(data,status)
    {
      var table = $('#dataTable-NewPasien').DataTable();
      table.clear();
      table.destroy();
      showtabelNewPasien();
      var isipesan = "Delete Data Pasien Sukses";
      $("#modal-body-delete-succes").html(isipesan);
      $('#modalDeleteSucces').modal('show');
    }
  });
  iddelete = "";
}

function modalprediksi()
{
  var isipesan = "Apakah Anda Yakin Memulai Proses Prediksi?";
  $("#modal-body-prediksi").html(isipesan);
  $('#modalprediksi').modal('show');
}

function prediksiClick()
{
  $.ajax({
          url:"../../back-end/page-pasien/ceknilaibefore_page-pasien_code.php",
          success:function(data)
          {
            if(data>0)
            {
              swal("Error", "Proses Prediksi Gagal. Pastikan Data Pasien Tidak Kosong, Proses K-Means Sudah Dilakukan, Dan Proses C4.5 Sudah Dilakukan ", "error");
            }
            else
            {
              $('#modalLoading').modal({
                keyboard: false,
                backdrop: 'static'
              });
              var isi = "Loading Progress Persiapan Data......";
              $('#modal-body-loading').html(isi);
              $('#modalLoading').modal('show');
              setTimeout(function()
              {
                start_clean();
                prosesKMean();
              },1000);
            }
          }
  });
}

// ------------------------------------------------------------------------------

function deleteAllDataPasienClick()
{
    $('#modalDeleteTruncate').modal('show');
    var isipesan = "Apakah Anda Yakin Menghapus Semua Data Pasein ?";
    $("#modal-body-deletetruncate").html(isipesan);
}

function jv_modal_delete()
{ 
  $.ajax({
          url:"../../back-end/page-pasien/delete_page-kmean_code.php",
          success:function(data)
          {
            if(data == "")
              {
                var table = $('#dataTable-NewPasien').DataTable();
                table.clear();
                table.destroy();

                var table = $('#dataTable-HasilPasien').DataTable();
                table.clear();
                table.destroy();

                var table = $('#dataTable-HasilPrediksiPasien').DataTable();
                table.clear();
                table.destroy();

                $('#modalDeleteSucces').modal('show');

                var hasil = "Hapus Data Pasien Berhasil !";
                $('#modal-body-delete-succes').html(hasil);

              }
              else
              {
                $('#modalDeleteSucces').modal('show');

                var hasil = "Hapus Data Pasien Gagal !";
                $('#modal-body-delete-success').html(hasil);
              }
          }
  });
}

function start_clean()
{ 
  $.ajax({
          url:"../../back-end/page-pasien/delete_start_page-pasien_code.php",
          success:function(data)
          {
            var table = $('#dataTable-HasilPasien').DataTable();
            table.clear();
            table.destroy();

            var table = $('#dataTable-HasilPrediksiPasien').DataTable();
            table.clear();
            table.destroy();
          }
  });
}

function prosesKMean()
{
  var isi = "Loading Progress Metode K-Means......";
  $('#modal-body-loading').html(isi);
    $.ajax({
        url:"../../back-end/page-pasien/kmean_code.php",
        success:function(data_json)
        {
          prosesPrediksi();
        }
    });
}

function prosesPrediksi()
{
  var isi = "Loading Progress Prediksi Data Pasien......";
  $('#modal-body-loading').html(isi);
    $.ajax({
        url:"../../back-end/page-pasien/prediksi_c45_page-pasien_code.php",
        success:function(data_json)
        {
          showtabelhasilKMean();
          showtabelPrediksi();
          setTimeout(function()
          { 
            $('#modalLoading').modal('hide');
            $('#close-loading').trigger('click');
          }, 1000);
          setTimeout(function()
          {
            $('#modalprediksiSucces').modal('show');
            var isipesan = "Proses Prediksi Berhasil";
            $("#modal-body-prediksi-succes").html(isipesan);
          },1000);
        }
    });
}


function showtabelhasilKMean()
{
    $.ajax({
        url:"../../back-end/page-pasien/hasil_page-kmean_code.php",
        dataType: 'json',
        contentType:false,
        processData:false,
        success:function(data_json)
        {
          var table = $('#dataTable-HasilPasien').DataTable();
          table.clear();
          table.destroy();
        	$('#dataTable-HasilPasien').DataTable({
                "scrollY": "250",
                "scrollX": true,
                data:data_json,
                    columns:[
                              { data: 'id' },
                              { data: 'nama'},
                              { data: 'gender' },
                              { data: 'age' },
                              { data: 'admission_type_id' },
                              { data: 'discharge_disposition_id' },
                              { data: 'admission_source_id' },
                              { data: 'time_in_hospital' },
                              { data: 'num_lab_procedures' },
                              { data: 'num_procedures' },
                              { data: 'diag_1' },
                              { data: 'diag_2' },
                              { data: 'diag_3' },
                              { data: 'number_diagnoses' },
                              { data: 'change' },
                              { data: 'diabetesMed' },
                              { data: 'readmitted' }
                    ]
                });
            }
        });
}

function showtabelPrediksi()
{
    $.ajax({
        url:"../../back-end/page-pasien/hasil_prediksi_code.php",
        dataType: 'json',
        contentType:false,
        processData:false,
        success:function(data_json)
        {
          var table = $('#dataTable-HasilPrediksiPasien').DataTable();
          table.clear();
          table.destroy();
          $('#dataTable-HasilPrediksiPasien').DataTable({
                "scrollY": "250",
                "scrollX": true,
                data:data_json,
                columns:[
                        { data: 'id' },
                        { data: 'nama' },
                        { data: 'gender' },
                        { data: 'age' },
                        { data: 'prediksi' }
                    ]
                });
            }
        });
}

// ----------------------Export Hasil K-Means
function exportClick()
{
  window.open('../../back-end/page-pasien/export_download_page-kmean_code.php', '_blank');
}
