function c45Click()
{
	$('#modalImport').modal('show');
	var isipesan = "Apakah Anda Yakin Memulai Proses Perhitungan Metode C4.5?";
	$("#modal-body-c45").html(isipesan);

}

function c45Loading()
{
  $.ajax({
          url:"../../back-end/page-c45/ceknilaibefore_page-c45_code.php",
          success:function(data)
          {
            if(data>0)
            {
              swal("Error", "Proses Klasifikasi C4.5 Gagal. Pastikan Proses K-Means Sudah Dilakukan !", "error");
            }
            else
            {
              jv_modal_delete();

              $('#modalLoading').modal({
                keyboard: false,
                backdrop: 'static'
              });
                
              $('#modalLoading').modal('show');

              setTimeout(function()
              {
                prosesC45();
              },1000);
            }
          }
  });
}

// function deleteClick()
// {
//     $('#modalDeleteTruncate').modal('show');
//     var isipesan = "Apakah Anda Yakin Menghapus Isi Tabel Hasil C4.5?";
//     $("#modal-body-deletetruncate").html(isipesan);
// }

function jv_modal_delete()
{ 
  $.ajax({
          url:"../../back-end/page-c45/delete_page-c45_code.php",
          success:function(data)
          {
            // if(data == "")
              // {
                var table = $('#dataTable-HasilC45').DataTable();
                table.clear();
                table.destroy();

                $("#pohon-c45").html(null);

                // $('#modalDeleteSucces').modal('show');

                // var hasil = "Hapus Data Tabel Data Hasil C4.5 Berhasil !";
                // $('#modal-body-delete-succes').html(hasil);

              // }
              // else
              // {
                // $('#modalDeleteSucces').modal('show');

                // var hasil = "Hapus Data Tabel Data Hasil C4.5 Gagal !";
                // $('#modal-body-delete-success').html(hasil);

              // }
          }
  });
}

function prosesC45()
{
  console.time('waktu-yang-dibutuhkan-proses-kmean');
    $.ajax({
        url:"../../back-end/page-c45/c45_code.php",
        success:function(data_json)
        {
        	showtabelC45();
          console.timeEnd('waktu-yang-dibutuhkan-proses-kmean');
        }
    });
}

function showtabelC45()
{
    $.ajax({
        url:"../../back-end/page-c45/hasil_page-c45_code.php",
        dataType: 'json',
        contentType:false,
        processData:false,
        success:function(data_json)
        {
          var table = $('#dataTable-HasilC45').DataTable();
          table.destroy();
        	$('#dataTable-HasilC45').DataTable({
                "scrollY": "500",
                "scrollX": true,
                data:data_json,
                columns:[
                    	  { data: 'no' },
                        { data: 'id' },
                        { data: 'noleave' },
                        { data: 'nmvariabel' },
                        { data: 'value' },
                        { data: 'entropy' },
                        { data: 'gain' },
                        { data: 'perkiraan' }
                    ]
                });
          loadpohonC45();
          $('#modalLoading').modal('hide');
          $('#close-loading').trigger('click');
          setTimeout(function()
          {
            $('#modalImportSucces').modal('show');
            var isipesan = "Proses Perhitungan Metode C4.5 Berhasil";
            $("#modal-body-c45-succes").html(isipesan);
          },300);
        }    
    });
}

function loadtabelKMean()
{
    $.ajax({
        url:"../../back-end/page-kmean/hasil_page-kmean_code.php",
        dataType: 'json',
        contentType:false,
        processData:false,
        success:function(data_json)
        {
          $('#dataTable-HasilKmean').DataTable({
                "scrollY": "550",
                "scrollX": true,
                data:data_json,
                    columns:[
                              { data: 'id' },
                              { data: 'gender' },
                              { data: 'age' },
                              { data: 'admission_type_id' },
                              { data: 'discharge_disposition_id' },
                              { data: 'admission_source_id' },
                              { data: 'time_in_hospital' },
                              { data: 'num_lab_procedures' },
                              { data: 'num_procedures' },
                              { data: 'diag_1' },
                              { data: 'diag_2' },
                              { data: 'diag_3' },
                              { data: 'number_diagnoses' },
                              { data: 'A1Cresult' },
                              { data: 'change' },
                              { data: 'diabetesMed' },
                              { data: 'readmitted' }
                    ]
                });
          loadtabelC45();        
          }
        });
}

function loadtabelC45()
{
    $.ajax({
        url:"../../back-end/page-c45/hasil_page-c45_code.php",
        dataType: 'json',
        contentType:false,
        processData:false,
        success:function(data_json)
        {
          var table = $('#dataTable-HasilC45').DataTable();
          table.destroy();
          $('#dataTable-HasilC45').DataTable({
                "scrollY": "500",
                "scrollX": true,
                data:data_json,
                columns:[
                        { data: 'no' },
                        { data: 'id' },
                        { data: 'noleave' },
                        { data: 'nmvariabel' },
                        { data: 'value' },
                        { data: 'entropy' },
                        { data: 'gain' },
                        { data: 'perkiraan' }
                    ]
                });
        loadpohonC45();
        }    
    });
}

function loadpohonC45()
{
    $.ajax({
        url:"../../back-end/page-c45/hasil_page-c45_treetext_code.php",
        success:function(data)
        {
          $("#pohon-c45").html(data);
        }    
    });
}

