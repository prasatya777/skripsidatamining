function validateEmailLogin(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function shownamaLogin()
{
	$.post("../../back-end/public_user/show_nama.php", {}, 
		function(data)
			{
				$("#WelcomeUser").html(data);
			});
}

// Update Login

function getusernameLogin()
{
	$.post("../../back-end/public_user/show_username.php", {}, 
		function(data)
			{
				tampiledituserLogin(data);
			});

}

function tampiledituserLogin(id)
{
	$.post("../../back-end/public_user/view_update_publicuser_code.php", {id:id}, 
		function(data)
			{
				var view_update_al = JSON.parse(data);
				$('#txt_email_login').val(view_update_al.email_al);
				$('#txt_nama_login').val(view_update_al.nama_al);
			});
	$('#modaledituserNavbar').modal('show');
}

function submit_editLogin()
{
	swal({
	  title: "Edit Information",
	  text: "Apakah Anda Yakin Update Data User ?",
	  icon: "warning",
	  buttons: true,
	  dangerMode: true,
	})
	.then((willDelete) => {
	  if (willDelete) 
	  {
	  	edituserLogin();
	  } 
	});
}

function edituserLogin()
{
	var txt_nama = $('#txt_nama_login').val();
	var txt_email = $('#txt_email_login').val();
	var txt_password = $('#txt_password_login').val();
	var txt_conf_password = $('#txt_conf_password_login').val();

	if(txt_email =="")
	{
		swal("Information", "Email Tidak Boleh Kosong", "warning");
		$('#txt_email_login').focus();
	}
	else if (!validateEmailLogin(txt_email))
	{
		swal("Information", "Format Email Tidak Sesuai", "warning");
		$('#txt_email_login').val(null);
		$('#txt_email_login').focus();
	}
	else if(txt_nama == "")
	{
		swal("Information", "Nama Lengkap Tidak Boleh Kosong", "warning");
		$('#txt_nama_login').focus();
	}
	else if(txt_password == "")
	{
		swal("Information", "Password Tidak Boleh Kosong", "warning");
		$('#txt_password_login').focus();
	}
	else if(txt_conf_password == "")
	{
		swal("Information", "Confirm Password Tidak Boleh Kosong", "warning");
		$('#txt_conf_password_login').focus();
	}
	else if(txt_password != txt_conf_password)
	{
		swal("Information", "Password Tidak Sesuai", "warning");
		$('#txt_password_login').val(null);
		$('#txt_conf_password_login').val(null);
		$('#txt_password_login').focus();
	}
	else
	{
		$.ajax({
			url:"../../back-end/public_user/update_publicuser_code.php",
			type:'POST',
			data: { 
				txt_nama : txt_nama,
				txt_email : txt_email,
				txt_password : txt_password
			},

			success:function(data){
				$('#modaledituserNavbar').modal('hide');
	            $('#close-edituserNavbar').trigger('click');
	            resetupdateuserLogin();
	            $('#modalEditSuccesLogin').modal('show');
	            shownamaLogin();
			}

		});
	}
}

function resetupdateuserLogin()
{
			$('#txt_nama_login').val(null);
			$('#txt_email_login').val(null);
			$('#txt_password_login').val(null);
			$('#txt_conf_password_login').val(null);

			$('#txt_email_login').focus();
}

// Logout
function submitLogout()
{
	$.post("../../back-end/public_user/logout_publicuser.php", {}, 
		function(data)
		{
			window.location.href='../../login/login.php';
		});
}

$(document).ready(function() {
    shownamaLogin();
});