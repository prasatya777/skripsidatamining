function tampildatamentahPageUtama()
{
  $.ajax({
      url:"../../back-end/page-utama/view_data_mentah_page-utama_code.php",
      dataType: 'json',
      contentType:false,
      processData:false,
      success:function(data)
      {
        var variabel = data;
        var nama_label = variabel.nilai.map(function(e) {
            return e.labelname;
        });

        var nilai_label = variabel.nilai.map(function(e) {
            return e.labelnilai;
        });

        // Bar Chart
        var ctx = document.getElementById("myBarChart-datamentah");
        var myLineChart_datamentah = new Chart(ctx, {
          type: 'bar',
          data: {
            labels: nama_label,
            datasets: [{
              label: "Jumlah Pasien",
              backgroundColor: "rgba(2,117,216,1)",
              borderColor: "rgba(2,117,216,1)",
              data: nilai_label,
            }],
          },
          options: {
            scales: {
              xAxes: [{
                gridLines: {
                  display: false
                },
                ticks: {
                  maxTicksLimit: 6
                }
              }],
              yAxes: [{
                ticks: {
                  min: 0,
                  maxTicksLimit: 5
                },
                gridLines: {
                  display: true
                }
              }],
            },
            legend: {
              display: false
            }
          }
        });
        setInterval(function(){
          updatedatamentahPageUtama(myLineChart_datamentah);
        },1000);
      }    
  });
}

function tampildatapasienPageUtama()
{
  $.ajax({
      url:"../../back-end/page-utama/view_data_pasien_page-utama_code.php",
      dataType: 'json',
      contentType:false,
      processData:false,
      success:function(data_json)
      {
        var variabel = data_json;
        var nama_label = variabel.nilai.map(function(e) {
            return e.labelname;
        });

        var nilai_label = variabel.nilai.map(function(e) {
            return e.labelnilai;
        });

        // Bar Chart
        var ctx = document.getElementById("myBarChart-dataprediksipasien");
        var myLineChart_datapasien = new Chart(ctx, {
          type: 'bar',
          data: {
            labels: nama_label,
            datasets: [{
              label: "Jumlah Pasien",
              backgroundColor: "rgba(2,117,216,1)",
              borderColor: "rgba(2,117,216,1)",
              data: nilai_label,
            }],
          },
          options: {
            scales: {
              xAxes: [{
                gridLines: {
                  display: false
                },
                ticks: {
                  maxTicksLimit: 6
                }
              }],
              yAxes: [{
                ticks: {
                  min: 0,
                  maxTicksLimit: 5
                },
                gridLines: {
                  display: true
                }
              }],
            },
            legend: {
              display: false
            }
          }
        });
        setInterval(function(){
          updatedatapasienPageUtama(myLineChart_datapasien);
        },1000);
      }    
  });
}

function updatedatamentahPageUtama(myLineChart_datamentah)
{
  $.ajax({
      url:"../../back-end/page-utama/view_data_mentah_page-utama_code.php",
      dataType: 'json',
      contentType:false,
      processData:false,
      success:function(data_json)
      {
        var variabel = data_json;
        var nama_label = variabel.nilai.map(function(e) {
            return e.labelname;
        });

        var nilai_label = variabel.nilai.map(function(e) {
            return e.labelnilai;
        });
        myLineChart_datamentah.data.labels = nama_label;
        myLineChart_datamentah.data.datasets[0].data = nilai_label;
        myLineChart_datamentah.update();
      }    
  });
}

function updatedatapasienPageUtama(myLineChart_datapasien)
{
  $.ajax({
      url:"../../back-end/page-utama/view_data_pasien_page-utama_code.php",
      dataType: 'json',
      contentType:false,
      processData:false,
      success:function(data_json)
      {
        var variabel = data_json;
        var nama_label = variabel.nilai.map(function(e) {
            return e.labelname;
        });

        var nilai_label = variabel.nilai.map(function(e) {
            return e.labelnilai;
        });

        myLineChart_datapasien.data.labels = nama_label;
        myLineChart_datapasien.data.datasets[0].data = nilai_label;
        myLineChart_datapasien.update();
      }    
  });
}

function showtabelAkurasi()
{
  setInterval(function(){
    $.ajax({
        url:"../../back-end/page-utama/hitung_akurasi_page-utama_code.php",
        dataType: 'json',
        contentType:false,
        processData:false,
        success:function(data_json)
        {
          var table = $('#dataTable-Akurasi').DataTable();
          table.destroy();
          $('#dataTable-Akurasi').DataTable({
                "scrollY": "80",
                "scrollX": true,
                data:data_json,
                    columns:[
                              { data: 'nilai_benar' },
                              { data: 'nilai_salah' },
                              { data: 'nilai_total' },
                              { data: 'nilai_akurasi' }
                            ]
                });
        }
      });
  },1000);
}
