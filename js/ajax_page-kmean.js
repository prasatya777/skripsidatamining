function importClick()
{
	var file = $('#ImportExcelFile').val();
	if(file != "")
	{
		var app_extension = file.slice((file.lastIndexOf(".") - 1 >>> 0) + 2);

		if(app_extension != "xlsx")
		{
			swal("Error", "Jenis Ekstensi Yang Dapat Digunakan Adalah Microsoft Excel (.xlsx) !", "error");
		}

		else
		{
			$('#modalImport').modal('show');
			var isipesan = "Apakah Anda Yakin Import File "+file+" Dan Memulai Proses Perhitungan Metode K-Means ?";

			$("#modal-body-import").html(isipesan);
		}

	}
}

function jv_modal_import_excel()
{
	$('#ImportExcel').submit();
	$('#modalLoading').modal({
		keyboard: false,
		backdrop: 'static'
	});
		
	$('#modalLoading').modal('show');
}

function deleteClick()
{
    $('#modalDeleteTruncate').modal('show');
    var isipesan = "Apakah Anda Yakin Menghapus Semua Isi Tabel (Tabel Data Mentah, Tabel Data Centroid, Tabel Data Hasil K-Means) ?";
    $("#modal-body-deletetruncate").html(isipesan);
}

function jv_modal_delete()
{ 
  $.ajax({
          url:"../../back-end/page-kmean/delete_page-kmean_code.php",
          success:function(data)
          {
            if(data == "")
              {
                var table = $('#dataTable').DataTable();
                table.clear();
                table.destroy();

                var table = $('#dataTable-Range').DataTable();
                table.clear();
                table.destroy();

                var table = $('#dataTable-HasilKmean').DataTable();
                table.clear();
                table.destroy();

                $('#modalDeleteSucces').modal('show');

                var hasil = "Hapus Data Tabel Data Mentah, Tabel Data Centroid, Tabel Data Hasil K-Means  Berhasil !";
                $('#modal-body-delete-succes').html(hasil);

              }
              else
              {
                $('#modalDeleteSucces').modal('show');

                var hasil = "Hapus Data Tabel Data Mentah, Tabel Data Centroid, Tabel Data Hasil K-Means Gagal !";
                $('#modal-body-delete-success').html(hasil);

              }
          }
  });
}

// Proses K-Mean--------------------------
function prosesKMean_tih()
{
  // alert("prosesKMean_tih");
    $.ajax({
        url:"../../back-end/page-kmean/kmean_tih_code.php",
        success:function(data_json)
        {
          setTimeout(function()
              {
                prosesKMean_nlp();
              },1000);
          // alert("ok-tih");
        }
    });
}

function prosesKMean_nlp()
{
  // alert("prosesKMean_nlp");
    $.ajax({
        url:"../../back-end/page-kmean/kmean_nlp_code.php",
        success:function(data_json)
        {
          setTimeout(function()
              {
                // alert("ok-nlp");
                prosesKMean_np();
              },1000);
        }
    });
}

function prosesKMean_np()
{
  // alert("prosesKMean_np");
    $.ajax({
        url:"../../back-end/page-kmean/kmean_np_code.php",
        success:function(data_json)
        {
          setTimeout(function()
              {
                // alert("ok-np");
                prosesKMean_nd();
              },1000);
        }
    });
}

function prosesKMean_nd()
{
  // alert("prosesKMean_nd");
    $.ajax({
        url:"../../back-end/page-kmean/kmean_nd_code.php",
        success:function(data_json)
        {
          // alert("ok-nd");
          setTimeout(function()
              {
        	       prosesKMean_final();
              },1000);
        }
    });
}

function prosesKMean_final()
{
    $.ajax({
        url:"../../back-end/page-kmean/kmean_final_code.php",
        success:function(data_json)
        {
          // alert("ok-final");
          setTimeout(function()
              {
                showtabelRangeKmean();
              },1000);
        }
    });
}
// --------------------------------------

function showtabelMentah()
{
    $.ajax({
        url:"../../back-end/page-kmean/data_mentah_page-kmean_code.php",
        dataType: 'json',
        contentType:false,
        processData:false,
        success:function(data_json)
        {
          var table = $('#dataTable').DataTable();
          table.destroy();
          $('#dataTable').DataTable({
                "scrollY": "550",
                "scrollX": true,
                data:data_json,
                    columns:[
                              { data: 'id' },
                              { data: 'gender' },
                              { data: 'age' },
                              { data: 'admission_type_id' },
                              { data: 'discharge_disposition_id' },
                              { data: 'admission_source_id' },
                              { data: 'time_in_hospital' },
                              { data: 'num_lab_procedures' },
                              { data: 'num_procedures' },
                              { data: 'diag_1' },
                              { data: 'diag_2' },
                              { data: 'diag_3' },
                              { data: 'number_diagnoses' },
                              { data: 'A1Cresult' },
                              { data: 'change' },
                              { data: 'diabetesMed' },
                              { data: 'readmitted' }
                    ]
                });

            var isi = "Loading Progress Centroid K-Means......";
            $('#modal-body-loading').html(isi);
            prosesKMean_tih();
          }
        });
}

function showtabelRangeKmean()
{
    $.ajax({
        url:"../../back-end/page-kmean/range_page-kmean_code.php",
        dataType: 'json',
        contentType:false,
        processData:false,
        success:function(data_json)
        {
          var table = $('#dataTable-Range').DataTable();
          table.destroy();
        	$('#dataTable-Range').DataTable({
                "scrollY": "500",
                "scrollX": true,
                data:data_json,
                columns:[
                    	  { data: 'no_range' },
                        { data: 'var_range' },
                        { data: 'cluster_range' },
                        { data: 'min_range' },
                        { data: 'max_range' }
                    ]
                });

            var isi = "Loading Progress Hasil K-Meas......";
            $('#modal-body-loading').html(isi);
            // alert("ok-range");
        		showtabelhasilKMean();
            }
        });
}

function showtabelhasilKMean()
{
    $.ajax({
        url:"../../back-end/page-kmean/hasil_page-kmean_code.php",
        dataType: 'json',
        contentType:false,
        processData:false,
        success:function(data_json)
        {
          var table = $('#dataTable-HasilKmean').DataTable();
          table.destroy();
        	$('#dataTable-HasilKmean').DataTable({
                "scrollY": "550",
                "scrollX": true,
                data:data_json,
                    columns:[
                              { data: 'id' },
                              { data: 'gender' },
                              { data: 'age' },
                              { data: 'admission_type_id' },
                              { data: 'discharge_disposition_id' },
                              { data: 'admission_source_id' },
                              { data: 'time_in_hospital' },
                              { data: 'num_lab_procedures' },
                              { data: 'num_procedures' },
                              { data: 'diag_1' },
                              { data: 'diag_2' },
                              { data: 'diag_3' },
                              { data: 'number_diagnoses' },
                              { data: 'A1Cresult' },
                              { data: 'change' },
                              { data: 'diabetesMed' },
                              { data: 'readmitted' }
                    ]
                });

                $('#modalLoading').modal('hide');
                $('#close-loading').trigger('click');
                $('#modalImportSucces').modal('show');

                var hasil = "Import Data Dan Proses Metode K-Means Berhasil";
                $('#modal-body-import-succes').html(hasil);
                $('#ImportExcelFile').val("");
            }
        });
}

// Proses Tabel Ketika Awal Load
function loadtabelMentah()
{
  // alert("Load Running");
    $.ajax({
        url:"../../back-end/page-kmean/data_mentah_page-kmean_code.php",
        dataType: 'json',
        contentType:false,
        processData:false,
        success:function(data_json)
        {
          // alert("Tabel Mentah : " +data_json);
          $('#dataTable').DataTable({
                "scrollY": "550",
                "scrollX": true,
                data:data_json,
                    columns:[
                              { data: 'id' },
                              { data: 'gender' },
                              { data: 'age' },
                              { data: 'admission_type_id' },
                              { data: 'discharge_disposition_id' },
                              { data: 'admission_source_id' },
                              { data: 'time_in_hospital' },
                              { data: 'num_lab_procedures' },
                              { data: 'num_procedures' },
                              { data: 'diag_1' },
                              { data: 'diag_2' },
                              { data: 'diag_3' },
                              { data: 'number_diagnoses' },
                              { data: 'A1Cresult' },
                              { data: 'change' },
                              { data: 'diabetesMed' },
                              { data: 'readmitted' }
                    ]
                });
            loadtabelRangeKmean();
            }
        });
}

function loadtabelRangeKmean()
{
    $.ajax({
        url:"../../back-end/page-kmean/range_page-kmean_code.php",
        dataType: 'json',
        contentType:false,
        processData:false,
        success:function(data_json)
        {
          $('#dataTable-Range').DataTable({
                "scrollY": "500",
                "scrollX": true,
                data:data_json,
                columns:[
                        { data: 'no_range' },
                        { data: 'var_range' },
                        { data: 'cluster_range' },
                        { data: 'min_range' },
                        { data: 'max_range' }
                    ]
                });
          loadtabelKMean();
        }
      });
}

function loadtabelKMean()
{
    $.ajax({
        url:"../../back-end/page-kmean/hasil_page-kmean_code.php",
        dataType: 'json',
        contentType:false,
        processData:false,
        success:function(data_json)
        {
          // alert("Tabel K-Mean : " +data_json);
          $('#dataTable-HasilKmean').DataTable({
                "scrollY": "550",
                "scrollX": true,
                data:data_json,
                    columns:[
                              { data: 'id' },
                              { data: 'gender' },
                              { data: 'age' },
                              { data: 'admission_type_id' },
                              { data: 'discharge_disposition_id' },
                              { data: 'admission_source_id' },
                              { data: 'time_in_hospital' },
                              { data: 'num_lab_procedures' },
                              { data: 'num_procedures' },
                              { data: 'diag_1' },
                              { data: 'diag_2' },
                              { data: 'diag_3' },
                              { data: 'number_diagnoses' },
                              { data: 'A1Cresult' },
                              { data: 'change' },
                              { data: 'diabetesMed' },
                              { data: 'readmitted' }
                    ]
                });
            }
        });
}

// ----------------------Export Hasil K-Means
function exportClick()
{
  window.open('../../back-end/page-kmean/export_download_page-kmean_code.php', '_blank');
}

