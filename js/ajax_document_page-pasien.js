$(document).ready(function () {
// Import
  $('#ImportExcel').on('submit', function(event)
   {
      event.preventDefault();
        $.ajax({
                url:"../../back-end/page-pasien/import_page-pasien_code.php",
                method:"POST",
                data:new FormData(this),
                contentType:false,
                processData:false,
                success:function(data_json)
                {
                  showtabelNewPasien();
                  setTimeout(function()
                  { 
                      $('#modalLoading').modal('hide');
                      $('#close-loading').trigger('click');
                      $('#modalImportSucces').modal('show');

                      var hasil = "Import Data Pasien Berhasil";
                      $('#modal-body-import-succes').html(hasil);
                      $('#ImportExcelFile').val("");
                  }, 3000);
              }
        });
    }
  );

//Select Add--------------------------------------------------------------------------
  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-gender_code.php",
            dataType: 'json'
        }).then(function (response) {
                $('#txt_gender').select2({
                    dropdownParent: $('#txt_gender').parent(),
                    placeholder: "---Pilih Gender---",
                    allowClear: true,
                    data: response
                });
            });

  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-age_code.php",
            dataType: 'json'
        }).then(function (response) {
                $("#txt_age").select2({
                    dropdownParent: $('#txt_age').parent(),
                    placeholder: "---Pilih Rentang Usia---",
                    allowClear: true,
                    data: response
                });
            });

  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-admission_type_id_code.php",
            dataType: 'json'
        }).then(function (response) {
                $("#txt_admission_type_id").select2({
                    dropdownParent: $('#txt_admission_type_id').parent(),
                    placeholder: "---Pilih Admission Type---",
                    allowClear: true,
                    data: response
                }); 
            });

  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-discharge_disposition_id_code.php",
            dataType: 'json'
        }).then(function (response) {
                $("#txt_discharge_disposition_id").select2({
                    dropdownParent: $('#txt_discharge_disposition_id').parent(),
                    placeholder: "---Pilih Discharge Disposition---",
                    allowClear: true,
                    data: response
                });
            });

  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-admission_source_id_code.php",
            dataType: 'json'
        }).then(function (response) {
                $("#txt_admission_source_id").select2({
                    dropdownParent: $('#txt_admission_source_id').parent(),
                    placeholder: "---Pilih Admission Source---",
                    allowClear: true,
                    data: response
                });
            });

  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-diag_1_code.php",
            dataType: 'json'
        }).then(function (response) {
                $("#txt_diag_1").select2({
                    dropdownParent: $('#txt_diag_1').parent(),
                    placeholder: "---Pilih Diagnosis 1---",
                    allowClear: true,
                    data: response
                });
            });

  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-diag_2_code.php",
            dataType: 'json'
        }).then(function (response) {
                $("#txt_diag_2").select2({
                    dropdownParent: $('#txt_diag_2').parent(),
                    placeholder: "---Pilih Diagnosis 2---",
                    allowClear: true,
                    data: response
                });
            });

  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-diag_3_code.php",
            dataType: 'json'
        }).then(function (response) {
                $("#txt_diag_3").select2({
                    dropdownParent: $('#txt_diag_3').parent(),
                    placeholder: "---Pilih Diagnosis 3---",
                    allowClear: true,
                    data: response
                });
            });

  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-change_code.php",
            dataType: 'json'
        }).then(function (response) {
                $("#txt_change").select2({
                    dropdownParent: $('#txt_change').parent(),
                    placeholder: "---Pilih Perubahan Resep---",
                    allowClear: true,
                    data: response
                });
            });

  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-diabetesMed_code.php",
            dataType: 'json'
        }).then(function (response) {
                $("#txt_diabetesMed").select2({
                    dropdownParent: $('#txt_diabetesMed').parent(),
                    placeholder: "---Pilih Penggunaan Obat Sebelumnya---",
                    allowClear: true,
                    data: response
                });
            });

  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-readmitted_code.php",
            dataType: 'json'
        }).then(function (response) {
                $("#txt_readmitted").select2({
                    dropdownParent: $('#txt_readmitted').parent(),
                    placeholder: "---Pilih Kategori Lama Pertemuan---",
                    allowClear: true,
                    data: response
                });
            });

//Select Edit--------------------------------------------------------------------------
  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-gender_code.php",
            dataType: 'json'
        }).then(function (response) {
                $('#txt_gender-edit').select2({
                    dropdownParent: $('#txt_gender-edit').parent(),
                    placeholder: "---Pilih Gender---",
                    allowClear: true,
                    data: response
                });
            });

  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-age_code.php",
            dataType: 'json'
        }).then(function (response) {
                $("#txt_age-edit").select2({
                    dropdownParent: $('#txt_age-edit').parent(),
                    placeholder: "---Pilih Rentang Usia---",
                    allowClear: true,
                    data: response
                });
            });

  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-admission_type_id_code.php",
            dataType: 'json'
        }).then(function (response) {
                $("#txt_admission_type_id-edit").select2({
                    dropdownParent: $('#txt_admission_type_id-edit').parent(),
                    placeholder: "---Pilih Admission Type---",
                    allowClear: true,
                    data: response
                }); 
            });

  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-discharge_disposition_id_code.php",
            dataType: 'json'
        }).then(function (response) {
                $("#txt_discharge_disposition_id-edit").select2({
                    dropdownParent: $('#txt_discharge_disposition_id-edit').parent(),
                    placeholder: "---Pilih Discharge Disposition---",
                    allowClear: true,
                    data: response
                });
            });

  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-admission_source_id_code.php",
            dataType: 'json'
        }).then(function (response) {
                $("#txt_admission_source_id-edit").select2({
                    dropdownParent: $('#txt_admission_source_id-edit').parent(),
                    placeholder: "---Pilih Admission Source---",
                    allowClear: true,
                    data: response
                });
            });

  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-diag_1_code.php",
            dataType: 'json'
        }).then(function (response) {
                $("#txt_diag_1-edit").select2({
                    dropdownParent: $('#txt_diag_1-edit').parent(),
                    placeholder: "---Pilih Diagnosis 1---",
                    allowClear: true,
                    data: response
                });
            });

  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-diag_2_code.php",
            dataType: 'json'
        }).then(function (response) {
                $("#txt_diag_2-edit").select2({
                    dropdownParent: $('#txt_diag_2-edit').parent(),
                    placeholder: "---Pilih Diagnosis 2---",
                    allowClear: true,
                    data: response
                });
            });

  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-diag_3_code.php",
            dataType: 'json'
        }).then(function (response) {
                $("#txt_diag_3-edit").select2({
                    dropdownParent: $('#txt_diag_3-edit').parent(),
                    placeholder: "---Pilih Diagnosis 3---",
                    allowClear: true,
                    data: response
                });
            });

  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-change_code.php",
            dataType: 'json'
        }).then(function (response) {
                $("#txt_change-edit").select2({
                    dropdownParent: $('#txt_change-edit').parent(),
                    placeholder: "---Pilih Perubahan Resep---",
                    allowClear: true,
                    data: response
                });
            });

  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-diabetesMed_code.php",
            dataType: 'json'
        }).then(function (response) {
                $("#txt_diabetesMed-edit").select2({
                    dropdownParent: $('#txt_diabetesMed-edit').parent(),
                    placeholder: "---Pilih Penggunaan Obat Sebelumnya---",
                    allowClear: true,
                    data: response
                });
            });

  $.ajax({
            url: "../../back-end/page-pasien/list-menu/list-readmitted_code.php",
            dataType: 'json'
        }).then(function (response) {
                $("#txt_readmitted-edit").select2({
                    dropdownParent: $('#txt_readmitted-edit').parent(),
                    placeholder: "---Pilih Kategori Lama Pertemuan---",
                    allowClear: true,
                    data: response
                });
            });
    
    setTimeout(function()
    { 
        resetPagePasien();
    }, 500);

    showtabelNewPasien();
    showtabelhasilKMean();
    showtabelPrediksi();
});